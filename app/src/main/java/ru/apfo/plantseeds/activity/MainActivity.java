package ru.apfo.plantseeds.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.List;

import ru.apfo.plantseeds.FragmentType;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.fragments.main.FavouriteMainFragment;
import ru.apfo.plantseeds.fragments.main.HomeCategoryFragment;
import ru.apfo.plantseeds.fragments.main.SearchMainFragment;
import ru.apfo.plantseeds.fragments.nav.AboutMainFragment;
import ru.apfo.plantseeds.fragments.nav.CompaniesMainFragment;
import ru.apfo.plantseeds.fragments.nav.FlowersMainFragment;
import ru.apfo.plantseeds.fragments.nav.VegetablesMainFragment;
import ru.apfo.plantseeds.util.PreferencesAdapter;

import static ru.apfo.plantseeds.MyPreferences.APP_PREFERENCES_FIRST_START;

public class MainActivity extends AppCompatActivity implements ServiceConnection,
        NavigationView.OnNavigationItemSelectedListener, HomeCategoryFragment.ActivityCallback {
    private FragmentManager mFragmentManager;

    private HomeCategoryFragment homeFragment;
    private SearchMainFragment searchMainFragment;
    private FavouriteMainFragment favouriteMainFragment;
    private FlowersMainFragment flowersMainFragment;
    private VegetablesMainFragment vegetablesMainFragment;
    private CompaniesMainFragment companiesMainFragment;
    private AboutMainFragment aboutMainFragment;

    private Fragment mFragment;

    private NavigationView navigationView;

    private boolean bindStatus;

    private List<HomeCategory> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialise();
    }

    private void initialise(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMainDefault);
        setSupportActionBar(toolbar);

        new PreferencesAdapter().clearAllSuggestions(PreferenceManager.getDefaultSharedPreferences(this));

        Intent intent = new Intent(this, Service.class);
        bindStatus = bindService(intent, this, Context.BIND_AUTO_CREATE);

        final SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        final boolean isFirstStart = getPrefs.getBoolean(APP_PREFERENCES_FIRST_START, true);

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if (isFirstStart) {
                    startActivity(new Intent(MainActivity.this, CustomIntro.class));

                    SharedPreferences.Editor e = getPrefs.edit();
                    e.putBoolean(APP_PREFERENCES_FIRST_START, false);
                    e.apply();
                }
            }
        });
        t.start();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        BottomNavigationBar bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottomNavigationCategory);
        BottomBarItem bottomBarItemHome = new BottomBarItem(R.drawable.icon_flowers, R.string.bottom_bar_home);
        BottomBarItem bottomBarItemSearch = new BottomBarItem(R.drawable.icon_search, R.string.bottom_bar_search);
        BottomBarItem bottomBarItemFavourites = new BottomBarItem(R.drawable.icon_favourite_bottom, R.string.bottom_bar_favourites);
        bottomNavigationBar.addTab(bottomBarItemHome)
                .addTab(bottomBarItemSearch)
                .addTab(bottomBarItemFavourites);
        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                bottomBarChoose(position);
            }
        });
        bottomNavigationBar.setOnReselectListener(new BottomNavigationBar.OnReselectListener() {
            @Override
            public void onReselect(int position) {
                bottomBarChoose(position);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            MaterialDialog.Builder materialDialog = new MaterialDialog.Builder(this);
            materialDialog.titleGravity(GravityEnum.CENTER);
            materialDialog.title("Вы действительно хотите выйти?");
            materialDialog.positiveText("Да");
            materialDialog.negativeText("Отмена");
            materialDialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    finish();
                }
            });
            materialDialog.show();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_left_flowers:
                changeFragment(FragmentType.FLOWERS);
                break;
            case R.id.nav_left_vegetables:
                changeFragment(FragmentType.VEGETABLES);
                break;
            case R.id.nav_left_manufacturers:
                changeFragment(FragmentType.COMPANIES);
                break;
            case R.id.nav_left_about:
                changeFragment(FragmentType.ABOUT);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void disableAllNavigationItems() {
        int size = navigationView.getMenu().size();
        for (int i = 0; i < size; i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent != null) {
            bottomBarChoose(intent.getIntExtra(MyIntentKeys.APP_INTENT_BOTTOM_BAR_POSITION, 0));
        }
    }

    private void bottomBarChoose(int position){
        disableAllNavigationItems();
        switch (position){
            case 0:
                changeFragment(FragmentType.MAIN);
                break;
            case 1:
                changeFragment(FragmentType.SEARCH);
                break;
            case 2:
                changeFragment(FragmentType.FAVOURITE);
                break;
            default:
                break;
        }
    }

    private void changeFragment(FragmentType type){
        switch (type){
            case MAIN:
                setTitle("Главная");
                hideShowFragment(mFragment, homeFragment);
                showDefaultToolbar();
                mFragment = homeFragment;
                break;
            case SEARCH:
                setTitle("Поиск");
                if (searchMainFragment == null) searchMainFragment = SearchMainFragment.newInstance();
                hideShowFragment(mFragment, searchMainFragment);
                showToolbarWithSearch();
                mFragment = searchMainFragment;
                break;
            case FAVOURITE:
                setTitle("Избранные");
                if (favouriteMainFragment == null) favouriteMainFragment = FavouriteMainFragment.newInstance();
                hideShowFragment(mFragment, favouriteMainFragment);
                showDefaultToolbar();
                mFragment = favouriteMainFragment;
                break;
            case FLOWERS:
                setTitle("Цветы");
                showDefaultToolbar();
                if (flowersMainFragment == null){
                    if (categories == null){
                        showSnackbarMessage("∆анные для цветов отсутствуют");
                        return;
                    }
                    flowersMainFragment = FlowersMainFragment.newInstance(categories.get(1));
                }
                hideShowFragment(mFragment, flowersMainFragment);
                mFragment = flowersMainFragment;
                break;
            case VEGETABLES:
                setTitle("Овощи");
                showDefaultToolbar();
                if (vegetablesMainFragment == null){
                    if (categories == null){
                        showSnackbarMessage("∆анные для овощей отсутствуют");
                        return;
                    }
                    vegetablesMainFragment = VegetablesMainFragment.newInstance(categories.get(0));
                }
                hideShowFragment(mFragment, vegetablesMainFragment);
                mFragment = vegetablesMainFragment;
                break;
            case COMPANIES:
                setTitle("Производители");
                if (companiesMainFragment == null) companiesMainFragment = CompaniesMainFragment.newInstance();
                hideShowFragment(mFragment, companiesMainFragment);
                showDefaultToolbar();
                mFragment = companiesMainFragment;
                break;
            case ABOUT:
                setTitle("О программе");
                if (aboutMainFragment == null) aboutMainFragment = AboutMainFragment.newInstance();
                hideShowFragment(mFragment, aboutMainFragment);
                showDefaultToolbar();
                mFragment = aboutMainFragment;
                break;
            default:
                break;
        }
    }

    private void createHomeFragment(){
        mFragmentManager = getSupportFragmentManager();

        homeFragment = HomeCategoryFragment.newInstance();
        homeFragment.setActiviy(this);
        mFragmentManager.beginTransaction().add(R.id.main_frame_layout, homeFragment).commit();
        mFragment = homeFragment;
        setTitle("Главная");
    }

    private void hideShowFragment(Fragment hide, Fragment show) {
        if (!show.isAdded()) mFragmentManager.beginTransaction().add(R.id.main_frame_layout, show).commit();
        mFragmentManager.beginTransaction().hide(hide).show(show).commit();
    }

    private void showDefaultToolbar(){
        MaterialSearchBar searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        searchBar.setVisibility(View.GONE);
        Toolbar toolbarDefault = (Toolbar) findViewById(R.id.toolbarMainDefault);
        toolbarDefault.setVisibility(View.VISIBLE);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarMain);
        appBarLayout.setElevation(8);
    }

    private void showToolbarWithSearch(){
        Toolbar toolbarDefault = (Toolbar) findViewById(R.id.toolbarMainDefault);
        toolbarDefault.setVisibility(View.GONE);
        MaterialSearchBar searchBar = (MaterialSearchBar) findViewById(R.id.searchBar);
        searchBar.setVisibility(View.VISIBLE);
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appBarMain);
        appBarLayout.setElevation(0);
    }

    private void showSnackbarMessage(String msg){
        Snackbar.make(navigationView, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setCategories(List<HomeCategory> categories) {
        this.categories = categories;
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        createHomeFragment();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bindStatus){
            unbindService(this);
            bindStatus = false;
        }
    }
}