package ru.apfo.plantseeds.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueMultiply;
import ru.apfo.plantseeds.data.api.model.filter.base.ValueMultiply;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueMultiplyResult;

public class MultiplyFilterActivity extends AppCompatActivity {
    private Context context;

    private int spec_id;

    private FilterValueMultiply filter;
    private ListView listView;
    private BaseAdapter adapter;

    private boolean[] checks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multiply_filter);
        getSupportActionBar().hide();

        context = this;

        Intent intent = getIntent();
        filter = (FilterValueMultiply) intent.getSerializableExtra(MyIntentKeys.APP_INTENT_MULTIPLY_FILTER_VALUES);
        spec_id = intent.getIntExtra(MyIntentKeys.APP_INTENT_MULTIPLY_FILTER_VALUES_ID, 0);
        if (filter.getFilterValueMultiplyResult() == null){
            filter.setFilterValueMultiplyResult(
                    new FilterValueMultiplyResult(
                            filter.getSpec_id(),
                            filter.getType(),
                            filter.getCode()
                    ));
        }
        checks = new boolean[filter.getValues().size()];

        ((ImageView) findViewById(R.id.imageViewFilterBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        listView = (ListView) findViewById(R.id.listViewMultiplyFilter);
        adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return filter.getValues().size();
            }

            @Override
            public ValueMultiply getItem(int i) {
                return filter.getValues().get(i);
            }

            @Override
            public long getItemId(int i) {
                return filter.getValues().get(i).hashCode();
            }

            @Override
            public View getView(final int i, View convertView, ViewGroup viewGroup) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                final MyViewHolder vh;
                View row = convertView;
                if (row == null) {
                    row = inflater.inflate(R.layout.filter_type_multiply_select_item, viewGroup, false);

                    vh = new MyViewHolder();
                    vh.textView = (TextView) row.findViewById(R.id.textViewFilterTypeMultiplySelectItem);
                    vh.checkBox = (CheckBox) row.findViewById(R.id.checkboxFilterTypeMultiplySelectItem);

                    row.setTag(vh);
                } else{
                    vh = (MyViewHolder) row.getTag();
                }

                vh.textView.setText(filter.getValues().get(i).getCode());
                vh.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        int getPosition = (Integer) compoundButton.getTag();
                        checks[getPosition] = compoundButton.isChecked();
                    }
                });
                vh.checkBox.setTag(i);
                vh.checkBox.setChecked(checks[i]);

                List<String> result = filter.getFilterValueMultiplyResult().getValues();
                for (int j = 0; j < result.size(); j++) {
                    if (vh.textView.getText().toString().equals(result.get(j))) {
                        vh.checkBox.setChecked(true);
                    }
                }

                return row;
            }

            class MyViewHolder{
                TextView textView;
                CheckBox checkBox;
            }
        };
        listView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        Intent sendIntent = new Intent();

        List<String> results = new ArrayList<>();
        for (int i = 0; i < checks.length; i++){
            if (checks[i]){
                results.add(filter.getValues().get(i).getCode());
            }
        }

        filter.getFilterValueMultiplyResult().setValues(results);
        sendIntent.putExtra(MyIntentKeys.APP_INTENT_MULTIPLY_FILTER_VALUES_SEND, filter);
        sendIntent.putExtra(MyIntentKeys.APP_INTENT_MULTIPLY_FILTER_VALUES_ID, spec_id);
        setResult(Activity.RESULT_OK, sendIntent);
        finish();
    }
}