package ru.apfo.plantseeds.activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.fragments.main.HomeSubSubcategoryFragment;

public class HomeSubSubcategoryActivity extends AppCompatActivity implements HomeSubSubcategoryFragment.ActivityCallback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_sub_subcategory);

        initialise();
    }

    private void initialise(){
        HomeCategory homeSubcategory = (HomeCategory) getIntent().getSerializableExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBCATEGORY);
        DetailCompany detailCompany = (DetailCompany) getIntent().getSerializableExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FragmentManager fragmentManager = getSupportFragmentManager();

        HomeSubSubcategoryFragment homeSubSubcategoryFragment = HomeSubSubcategoryFragment.newInstance(homeSubcategory, true, detailCompany);
        homeSubSubcategoryFragment.setActivity(this);
        fragmentManager.beginTransaction().add(R.id.frameLayoutHomeSubSubcategoryActivity, homeSubSubcategoryFragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}