package ru.apfo.plantseeds.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.github.paolorotolo.appintro.AppIntro2;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.fragments.intro.SampleSlideFragment;

public class CustomIntro extends AppIntro2 {
    @Override
    public void init(@Nullable Bundle savedInstanceState) {
        showSkipButton(false);
        addSlide(SampleSlideFragment.newInstance(R.layout.intro_1));
        addSlide(SampleSlideFragment.newInstance(R.layout.intro_2));
        addSlide(SampleSlideFragment.newInstance(R.layout.intro_3));
    }

    @Override
    public void onDonePressed() {
        finish();
    }

    @Override
    public void onNextPressed() {

    }

    @Override
    public void onSlideChanged() {

    }
}