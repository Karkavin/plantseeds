package ru.apfo.plantseeds.activity;

import android.app.DialogFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.squareup.picasso.Picasso;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.Company;
import ru.apfo.plantseeds.data.api.model.CompanyReview;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.api.model.Product;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.fragments.dialog.CompanyReviewDialogFragment;
import ru.apfo.plantseeds.presenter.HomeCompanyPresenter;
import ru.apfo.plantseeds.view.HomeCompanyView;

public class CompanyActivity extends AppCompatActivity implements ServiceConnection,
        HomeCompanyView, CompanyReviewDialogFragment.CompanyReviewDialogFragmentListener{
    public static final int ACTIVITY_FOR_RESULT_CODE = 101;

    private HomeCompanyPresenter presenter;

    private boolean bindStatus;

    private ScrollView scrollViewContent;
    private ProgressBar progressBarLoadingContent;
    private ProgressBar progressBarLoadingReviews;

    private LinearLayout linearLayoutReviewContainer;

    private Company baseCompanyInfo;
    private Product baseProductInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);

        initialise();
    }

    private void initialise(){
        Fresco.initialize(this);

        baseCompanyInfo = (Company) getIntent().getSerializableExtra(MyIntentKeys.APP_INTENT_HOME_COMPANY_BASE_INFO);
        baseProductInfo = (Product) getIntent().getSerializableExtra(MyIntentKeys.APP_INTENT_HOME_COMPANY_PRODUCT);

        setTitle(baseProductInfo.getTitle());
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BottomNavigationBar bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottomNavigationCompany);
        BottomBarItem bottomBarItemHome = new BottomBarItem(R.drawable.icon_flowers, R.string.bottom_bar_home);
        BottomBarItem bottomBarItemSearch = new BottomBarItem(R.drawable.icon_search, R.string.bottom_bar_search);
        BottomBarItem bottomBarItemFavourites = new BottomBarItem(R.drawable.icon_favourite_bottom, R.string.bottom_bar_favourites);
        bottomNavigationBar.addTab(bottomBarItemHome)
                .addTab(bottomBarItemSearch)
                .addTab(bottomBarItemFavourites);
        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                bottomBarChoose(position);
            }
        });
        bottomNavigationBar.setOnReselectListener(new BottomNavigationBar.OnReselectListener() {
            @Override
            public void onReselect(int position) {
                bottomBarChoose(position);
            }
        });

        presenter = new HomeCompanyPresenter(this);

        Intent intent = new Intent(this, Service.class);
        bindStatus = bindService(intent, this, Context.BIND_AUTO_CREATE);

        scrollViewContent = (ScrollView) findViewById(R.id.scrollViewCompanyContent);
        progressBarLoadingContent = (ProgressBar) findViewById(R.id.progressBarCompanyLoadingContent);
        progressBarLoadingReviews = (ProgressBar) findViewById(R.id.progressBarHomeCompanyLoadingReview);

        linearLayoutReviewContainer = (LinearLayout) findViewById(R.id.linearLayoutHomeCompanyReviewContainer);
    }

    private void bottomBarChoose(int position){
        Intent intent = new Intent(CompanyActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(MyIntentKeys.APP_INTENT_BOTTOM_BAR_POSITION, position);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bindStatus){
            unbindService(this);
            bindStatus = false;
        }
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) return;
        else if (data.getBooleanExtra(MyIntentKeys.APP_INTENT_FULL_REVIEW_IS_REVIEW_ADDED, false)) {
            removeAllReview();
            presenter.getReviews((int) baseCompanyInfo.getId(), (int) baseProductInfo.getId());
        }
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(this)));
        bindStatus = true;
        presenter.getContent((int) baseCompanyInfo.getId());
        presenter.getReviews((int) baseCompanyInfo.getId(), (int) baseProductInfo.getId());
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void startLoadingContent() {
        scrollViewContent.setVisibility(View.GONE);
        progressBarLoadingContent.setVisibility(View.VISIBLE);
        progressBarLoadingContent.setVisibility(View.VISIBLE);

    }

    @Override
    public void stopLoadingContent() {
        scrollViewContent.setVisibility(View.VISIBLE);
        progressBarLoadingContent.setVisibility(View.GONE);
        progressBarLoadingContent.setVisibility(View.GONE);
    }

    @Override
    public void startLoadingReviews() {
        progressBarLoadingReviews.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoadingReviews() {
        progressBarLoadingReviews.setVisibility(View.GONE);
    }

    @Override
    public void addContent(DetailCompany detailCompany) {
        //TODO: перемешанные данные: тестовые данные и данные с сервера
        ((TextView) findViewById(R.id.textViewHomeCompanyName)).setText(detailCompany.getTitle());
        ((TextView) findViewById(R.id.textViewHomeCompanyCompanyNameInCard)).setText(detailCompany.getTitle());
        Picasso.with(this)
                .load(baseProductInfo.getImage())
                .into(((ImageView) findViewById(R.id.imageViewHomeCompany)));
        ((TextView) findViewById(R.id.textViewHomeCompanyProductName)).setText(baseProductInfo.getTitle());
        ((TextView) findViewById(R.id.textViewHomeCompanyWeight)).setText("22г");
        ((TextView) findViewById(R.id.textViewHomeCompanyCount)).setText("1250 семян");
        ((MaterialRatingBar) findViewById(R.id.ratingBarHomeCompanyRating)).setRating(detailCompany.getRating());
        ((TextView) findViewById(R.id.textViewHomeCompanyCountry)).setText(detailCompany.getCountry());
        ((TextView) findViewById(R.id.textViewHomeCompanyRating)).setText("" + detailCompany.getRating());
        ((TextView) findViewById(R.id.textViewHomeCompanyDescription)).setText(detailCompany.getDescr());

        MaterialRatingBar materialRatingBar = (MaterialRatingBar) findViewById(R.id.ratingBarHomeCompanyMyRating);
        materialRatingBar.setOnRatingChangeListener(new MaterialRatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChanged(MaterialRatingBar ratingBar, float rating) {
                CompanyReviewDialogFragment dialogFragment = CompanyReviewDialogFragment.newInstance((int) rating);
                dialogFragment.setListener(CompanyActivity.this);
                dialogFragment.show(getFragmentManager(), CompanyReviewDialogFragment.TAG_REVIEW);
            }
        });
    }

    @Override
    public void addReviews(List<CompanyReview> companyReviews) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textViewReviewCount = (TextView) findViewById(R.id.textViewHomeCompanyReviewCount);
        textViewReviewCount.setText("Отзывы (" + (companyReviews.size()+1) + ")");

        for (int i = 0; i < companyReviews.size(); i++) {
            View review = layoutInflater.inflate(R.layout.card_home_company_review, null);

            TextView textViewRating = (TextView) review.findViewById(R.id.textViewCardHomeCompanyReviewRating);
            MaterialRatingBar materialRatingBar = (MaterialRatingBar) review.findViewById(R.id.ratingBarCardHomeCompanyReviewRating);
            TextView textViewDate = (TextView) review.findViewById(R.id.textViewCardHomeCompanyReviewDate);
            TextView textViewReview = (TextView) review.findViewById(R.id.textViewCardHomeCompanyReviewContent);
            TextView textViewAuthor = (TextView) review.findViewById(R.id.textViewCardHomeCompanyReviewAuthor);
            LinearLayout linearLayoutImages = (LinearLayout) review.findViewById(R.id.linearLayoutCompanyReviewImages);

            textViewRating.setText(companyReviews.get(i).getRating() + "");
            materialRatingBar.setRating(companyReviews.get(i).getRating());

            if (!companyReviews.get(i).getDate().replaceAll(" ", "").equals("")){
                textViewDate.setVisibility(View.VISIBLE);
                textViewDate.setText(companyReviews.get(i).getDate());
            } else textViewDate.setVisibility(View.GONE);

            if (!companyReviews.get(i).getText().replaceAll(" ", "").equals("")){
                textViewReview.setVisibility(View.VISIBLE);
                textViewReview.setText(companyReviews.get(i).getText());
            } else textViewReview.setVisibility(View.GONE);

            if (!companyReviews.get(i).getAuthor().replaceAll(" ", "").equals("")){
                textViewAuthor.setVisibility(View.VISIBLE);
                textViewAuthor.setText(companyReviews.get(i).getAuthor());
            } else textViewAuthor.setVisibility(View.GONE);

            if (companyReviews.get(i).getImages() != null && companyReviews.get(i).getImages().size() > 0) {
                linearLayoutImages.setVisibility(View.VISIBLE);
                String[] imagePaths = new String[companyReviews.get(i).getImages().size()];
                for (int im = 0; im < companyReviews.get(i).getImages().size(); im++){
                    imagePaths[im] = companyReviews.get(i).getImages().get(im).getUrl();
                }
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                layoutParams.setMargins(10,0, 0, 0);
                for (int im = 0; im < imagePaths.length; im++) {
                    ImageView imageView = new ImageView(this);
                    imageView.setLayoutParams(layoutParams);
                    imageView.setAdjustViewBounds(true);
                    Picasso.with(this)
                            .load(imagePaths[im])
                            .into(imageView);
                    showImage(imageView, imagePaths, im);
                    linearLayoutImages.addView(imageView);
                }
            } else linearLayoutImages.setVisibility(View.GONE);

            linearLayoutReviewContainer.addView(review);
        }

        //TODO: Тестовые данные для просмотра
        View review = layoutInflater.inflate(R.layout.card_home_company_review, null);

        TextView textViewRating = (TextView) review.findViewById(R.id.textViewCardHomeCompanyReviewRating);
        MaterialRatingBar materialRatingBar = (MaterialRatingBar) review.findViewById(R.id.ratingBarCardHomeCompanyReviewRating);
        TextView textViewDate = (TextView) review.findViewById(R.id.textViewCardHomeCompanyReviewDate);
        TextView textViewReview = (TextView) review.findViewById(R.id.textViewCardHomeCompanyReviewContent);
        TextView textViewAuthor = (TextView) review.findViewById(R.id.textViewCardHomeCompanyReviewAuthor);
        LinearLayout linearLayoutImages = (LinearLayout) review.findViewById(R.id.linearLayoutCompanyReviewImages);

        textViewRating.setText(4.5f + "");
        materialRatingBar.setRating(4.5f);
        textViewDate.setText("25 февраля 2018г");
        textViewReview.setText("Взошли прекрасно, но немного не порадовал цвет. За это снизил оценку.");
        textViewAuthor.setVisibility(View.VISIBLE);
        textViewAuthor.setText("Петров Николай Иванович");
        linearLayoutImages.setVisibility(View.VISIBLE);
        String[] imagePaths = new String[]{"https://www.ogorod.ru/images/cache/660x400/crop/images%7Ccms-image-000042331.jpg",
                "https://www.ogorod.ru/images/cache/660x400/crop/images%7Ccms-image-000042331.jpg",
                "https://www.ogorod.ru/images/cache/660x400/crop/images%7Ccms-image-000042331.jpg",
                "https://www.ogorod.ru/images/cache/660x400/crop/images%7Ccms-image-000042331.jpg"};
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(10,0, 0, 0);
        for (int im = 0; im < imagePaths.length; im++) {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(layoutParams);
            imageView.setAdjustViewBounds(true);
            Picasso.with(this)
                    .load(imagePaths[im])
                    .into(imageView);
            showImage(imageView, imagePaths, im);
            linearLayoutImages.addView(imageView);
        }
        linearLayoutReviewContainer.addView(review);
    }

    private void showImage(ImageView imageView, String[] images, int startPosition){
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View overlayViewGallery = layoutInflater.inflate(R.layout.overlay_view_gallery, null);

                ImageViewer imageViewer = new ImageViewer.Builder(CompanyActivity.this, images)
                        .setOverlayView(overlayViewGallery)
                        .setStartPosition(startPosition)
                        .build();

                ImageView imageViewExit = (ImageView) overlayViewGallery.findViewById(R.id.imageViewOverlayGallery);
                imageViewExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imageViewer.onDismiss();
                    }
                });
                imageViewer.show();
            }
        });
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(progressBarLoadingReviews, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void removeAllReview() {
        if(linearLayoutReviewContainer.getChildCount() > 0) linearLayoutReviewContainer.removeAllViews();
    }

    @Override
    public void fullReviewAdded() {

    }

    @Override
    public void onDialogCompanyFullReview(DialogFragment dialog, int rating) {
        Intent intentFullReview = new Intent(this, FullReviewActivity.class);
        intentFullReview.putExtra(MyIntentKeys.APP_INTENT_FULL_REVIEW_RATING, rating);
        intentFullReview.putExtra(MyIntentKeys.APP_INTENT_FULL_REVIEW_COMPANY, baseCompanyInfo);
        intentFullReview.putExtra(MyIntentKeys.APP_INTENT_FULL_REVIEW_PRODUCT, baseProductInfo);
        startActivityForResult(intentFullReview, ACTIVITY_FOR_RESULT_CODE);
    }

    @Override
    public void onDialogCompanyShortReview(DialogFragment dialog, int rating) {
        presenter.addShortReview((int) baseCompanyInfo.getId(), (int) baseProductInfo.getId(), rating);
    }
}