package ru.apfo.plantseeds.activity;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.MyPermissions;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.Company;
import ru.apfo.plantseeds.data.api.model.CompanyReview;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.api.model.Product;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.presenter.HomeCompanyPresenter;
import ru.apfo.plantseeds.util.FileWork;
import ru.apfo.plantseeds.util.GenericFileProvider;
import ru.apfo.plantseeds.util.ImageUtils;
import ru.apfo.plantseeds.view.HomeCompanyView;

public class FullReviewActivity extends AppCompatActivity implements ServiceConnection, HomeCompanyView{

    private HomeCompanyPresenter presenter;
    private boolean bindStatus;

    private RelativeLayout relativeLayoutLoading;
    private ProgressBar progressBarLoading;

    private static final int RESULT_LOAD_IMG = 101;
    private static final int RESULT_LOAD_PHOTO = 102;
    private Uri imageToUploadUri;

    private Company company;
    private Product product;

    private ArrayList<String> imagesBase64;

    private ArrayList<HashMap<Integer, String>> imagePaths;
    private LinearLayout linearLayoutImages;
    private ImageView clickedImageView;
    private Bitmap clickedBitmap;

    private Button buttonAttachPhoto;
    private MaterialRatingBar ratingBar;
    private MaterialEditText editTextFullReview;
    private MaterialEditText editTextFullReviewAuthor;

    private MaterialDialog.Builder materialDialogShowLoad;
    private MaterialDialog dialogShowLoad;

    private MaterialDialog.Builder materialDialogPhoto;
    private MaterialDialog dialogPhoto;

    private MaterialDialog.Builder materialDialogShowPhoto;
    private MaterialDialog dialogShowPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_review);

        initialise();
    }

    private void initialise(){
        Intent intentExtra = getIntent();
        int rating = intentExtra.getIntExtra(MyIntentKeys.APP_INTENT_FULL_REVIEW_RATING, 0);
        company = (Company) intentExtra.getSerializableExtra(MyIntentKeys.APP_INTENT_FULL_REVIEW_COMPANY);
        product = (Product) intentExtra.getSerializableExtra(MyIntentKeys.APP_INTENT_FULL_REVIEW_PRODUCT);

        setTitle("Отзыв");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BottomNavigationBar bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottomNavigationFullReview);
        BottomBarItem bottomBarItemHome = new BottomBarItem(R.drawable.icon_flowers, R.string.bottom_bar_home);
        BottomBarItem bottomBarItemSearch = new BottomBarItem(R.drawable.icon_search, R.string.bottom_bar_search);
        BottomBarItem bottomBarItemFavourites = new BottomBarItem(R.drawable.icon_favourite_bottom, R.string.bottom_bar_favourites);
        bottomNavigationBar.addTab(bottomBarItemHome)
                .addTab(bottomBarItemSearch)
                .addTab(bottomBarItemFavourites);
        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                bottomBarChoose(position);
            }
        });
        bottomNavigationBar.setOnReselectListener(new BottomNavigationBar.OnReselectListener() {
            @Override
            public void onReselect(int position) {
                bottomBarChoose(position);
            }
        });

        relativeLayoutLoading = (RelativeLayout) findViewById(R.id.relativeLayoutFullReviewLoading);
        progressBarLoading = (ProgressBar) findViewById(R.id.progressBarFullReviewLoading);
        relativeLayoutLoading.setVisibility(View.GONE);
        progressBarLoading.setVisibility(View.GONE);

        imagesBase64 = new ArrayList<>();
        imagePaths = new ArrayList<>();
        linearLayoutImages = (LinearLayout) findViewById(R.id.linearLayoutCompanyFullReviewImages);
        linearLayoutImages.setVisibility(View.GONE);
        editTextFullReview = (MaterialEditText) findViewById(R.id.editTextFullReview);
        editTextFullReviewAuthor = (MaterialEditText) findViewById(R.id.editTextFullReviewAuthor);
        editTextFullReview.setAutoValidate(true);
        ratingBar = (MaterialRatingBar) findViewById(R.id.ratingBarFullReviewMyRating);
        ratingBar.setRating(rating);

        presenter = new HomeCompanyPresenter(this);

        ((TextView) findViewById(R.id.textViewFullReviewCompany)).setText("Напишите свой отзыв об использовании продукта производителя " + company.getTitle());
        ((Button) findViewById(R.id.buttonFullReviewRatingFull)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int contentSize = editTextFullReview.getText().toString().length();
                if (contentSize >= 5 && contentSize <= 240) {
                    Log.d(getClass().getSimpleName(), "New review:");
                    Log.d(getClass().getSimpleName(), "FulReview: " + editTextFullReview.getText().toString());
                    Log.d(getClass().getSimpleName(), "Rating: " + ratingBar.getRating());
                    Log.d(getClass().getSimpleName(), "Image (base64): " + imagesBase64.size());
                    presenter.addFullReview(company.getId(), product.getId(), editTextFullReviewAuthor.getText().toString()
                            , "", editTextFullReview.getText().toString(),
                            (int) ratingBar.getRating(), "", imagesBase64);
                } else {
                    showSnackbarMessage("Отзыв должен содержать от 5 до 240 символов");
                }
            }
        });

        materialDialogShowPhoto = new MaterialDialog.Builder(this);
        final LayoutInflater inflateShowPhoto = getLayoutInflater();
        View convertViewShowPhoto = inflateShowPhoto.inflate(R.layout.gallery_overlay, null);
        final ImageView imageViewPhotoModal = (ImageView) convertViewShowPhoto.findViewById(R.id.imageViewModal);
        materialDialogShowPhoto.customView(convertViewShowPhoto, false);
        dialogShowPhoto = materialDialogShowPhoto.build();

        materialDialogPhoto = new MaterialDialog.Builder(this);
        final LayoutInflater inflaterPhoto = getLayoutInflater();
        View convertViewPhoto = inflaterPhoto.inflate(R.layout.list_view_modal, null);
        final ListView listViewPhoto = (ListView) convertViewPhoto.findViewById(R.id.listViewModal);
        materialDialogPhoto.customView(convertViewPhoto, false);
        materialDialogPhoto.title("Действия с фото");
        materialDialogPhoto.titleGravity(GravityEnum.CENTER);
        dialogPhoto = materialDialogPhoto.build();

        String[] photo_actions = new String[2];
        photo_actions[0] = "Просмотреть";
        photo_actions[1] = "Удалить";

        ArrayAdapter<String> adapterPhotos = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, photo_actions) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setTextColor(Color.BLACK);

                return view;
            }
        };

        listViewPhoto.setAdapter(adapterPhotos);
        listViewPhoto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        System.out.println("Show image");
                        dialogPhoto.dismiss();
                        String imageUri = "";
                        for (int i = 0; i < imagePaths.size(); i++){
                            if (imagePaths.get(i).containsKey(clickedImageView.getId())){
                                imageUri = imagePaths.get(i).get(clickedImageView.getId());
                                break;
                            }
                        }
                        imageViewPhotoModal.setImageBitmap(clickedBitmap);
                        dialogShowPhoto.show();
                        System.out.println("Image uri: " + imageUri);
                        break;
                    case 1:
                        System.out.println("Delete image");
                        dialogPhoto.dismiss();
                        int index = linearLayoutImages.indexOfChild(clickedImageView);
                        imagesBase64.remove(index);
                        linearLayoutImages.removeView(clickedImageView);
                        System.out.println("Count images before delete: " + imagePaths.size());
                        int indexDelete = -1;
                        for (int i = 0; i < imagePaths.size(); i++){
                            if (imagePaths.get(i).containsKey(clickedImageView.getId())){
                                indexDelete = i;
                                break;
                            }
                        }
                        if (indexDelete != -1){
                            imagePaths.remove(indexDelete);
                            System.out.println("Image has been deleted!!! Position: " + indexDelete + "; ID = " + clickedImageView.getId());
                        }
                        if (imagePaths.size() == 0){
                            linearLayoutImages.setVisibility(View.GONE);
                        }
                        System.out.println("Count images before delete: " + imagePaths.size());
                        break;
                }
            }
        });

        materialDialogShowLoad = new MaterialDialog.Builder(this);
        LayoutInflater inflaterLoading = getLayoutInflater();
        View convertViewLoading = inflaterLoading.inflate(R.layout.list_view_modal, null);
        ListView listViewLoading = (ListView) convertViewLoading.findViewById(R.id.listViewModal);
        materialDialogShowLoad.customView(convertViewLoading, false);
        materialDialogShowLoad.titleGravity(GravityEnum.CENTER);
        dialogShowLoad = materialDialogShowLoad.build();

        String[] loading_actions = new String[2];
        loading_actions[0] = "Сделать фото";
        loading_actions[1] = "Загрузить из галереи";

        ArrayAdapter<String> adapterLoading = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, loading_actions) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                TextView textView = (TextView) view.findViewById(android.R.id.text1);
                textView.setTextColor(Color.BLACK);

                return view;
            }
        };

        listViewLoading.setAdapter(adapterLoading);
        listViewLoading.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        if (!MyPermissions.checkCameraPermission(FullReviewActivity.this)) {
                            Log.d("Camera", "EditProfileActivity -> permissions doesn't granted");
                            ActivityCompat.requestPermissions(
                                    FullReviewActivity.this,
                                    new String[]{Manifest.permission.CAMERA,
                                            Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    1);
                        } else {
                            dialogShowLoad.dismiss();
                            Intent photoCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            File f = new File(Environment.getExternalStorageDirectory(), "PlantSeeds_" + timeStamp + ".jpg");
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                Uri photoURI = GenericFileProvider.getUriForFile(getApplicationContext(),
                                        "ru.apfo.plantseeds.util.GenericFileProvider",
                                        f);
                                photoCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                imageToUploadUri = photoURI;
                                System.out.println("PATH FUCK N: " + photoURI.getPath());
                            } else{
                                photoCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                                imageToUploadUri = Uri.fromFile(f);
                                System.out.println("PATH FUCK Not N: " + Uri.fromFile(f));
                            }
                            photoCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivityForResult(photoCamera, RESULT_LOAD_PHOTO);
                        }
                        break;
                    case 1:
                        if (!MyPermissions.checkReadWriteStoragePermission(FullReviewActivity.this)) {
                            Log.d("Read write", "EditProfileActivity -> permissions doesn't granted");
                            ActivityCompat.requestPermissions(
                                    FullReviewActivity.this,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    2);
                        } else {
                            System.out.println("Upload image");
                            dialogShowLoad.dismiss();
                            Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                            photoPickerIntent.setType("image/*");
                            startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
                        }
                        break;
                }
            }
        });

        buttonAttachPhoto = (Button) findViewById(R.id.buttonFullReviewAttachPhoto);
        buttonAttachPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogShowLoad.show();
            }
        });

        Intent intent = new Intent(this, Service.class);
        bindStatus = bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED){
                dialogShowLoad.dismiss();
                Intent photoCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                File f = new File(Environment.getExternalStorageDirectory(), "PlantSeeds_" + timeStamp + ".jpg");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Uri photoURI = GenericFileProvider.getUriForFile(getApplicationContext(),
                            "ru.apfo.plantseeds.util.GenericFileProvider",
                            f);
                    photoCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    imageToUploadUri = photoURI;
                    System.out.println("PATH FUCK N: " + photoURI.getPath());
                } else{
                    photoCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    imageToUploadUri = Uri.fromFile(f);
                    System.out.println("PATH FUCK Not N: " + Uri.fromFile(f));
                }
                photoCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(photoCamera, RESULT_LOAD_PHOTO);
            } else {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_DENIED
                        && grantResults[2] == PackageManager.PERMISSION_DENIED) {
                    dialogShowLoad.dismiss();
                    System.out.println("camera permission granted but read/write doesn't");
                    showSettingsSnackbar("Доступ к изображениям не был разрешён");
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    dialogShowLoad.dismiss();
                    System.out.println("camera permission denied but read/write granted");
                    showSettingsSnackbar("Доступ к камере не был разрешён");
                } else {
                    dialogShowLoad.dismiss();
                    System.out.println("nothing granted. All denied");
                    showSnackbarMessage("Доступ к камере и изображениям не был разрешён");
                }
            }
        } else if (requestCode == 2){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                dialogShowLoad.dismiss();
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
            } else{
                dialogShowLoad.dismiss();
                System.out.println("read/write denied");
                showSettingsSnackbar("Доступ к изображениям не был разрешён");
            }
        }
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        InputStream imageStream = null;

        if (resultCode == RESULT_OK && reqCode == RESULT_LOAD_IMG) {
            try {
                final Uri imageUri = data.getData();
                imageStream = getContentResolver().openInputStream(imageUri);
                Bitmap selectedImage = new ImageUtils(this).decodeFileFromPath(imageUri);
                String imageBase64 = new ImageUtils(this).bitmapToBase64(selectedImage);

                addPhoto(selectedImage, FileWork.getPath(this, imageUri), imageBase64);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.d(getClass().getSimpleName(), "upload image -> Something went wrong");
            } finally {
                if (imageStream != null) {
                    try {
                        imageStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (resultCode == RESULT_OK && reqCode == RESULT_LOAD_PHOTO){
            Uri selectedImage = imageToUploadUri;
            getContentResolver().notifyChange(selectedImage, null);

            //TODO: протестировать работу
            Bitmap reducedSizeBitmap = new ImageUtils(this).decodeFileFromPath(imageToUploadUri);
            String imageBase64 = new ImageUtils(this).bitmapToBase64(reducedSizeBitmap);

            new File(selectedImage.getPath());

            addPhoto(reducedSizeBitmap, FileWork.getPath(this, selectedImage), imageBase64);
            if (new File(selectedImage.getPath()).exists()){
                System.out.println("FILE EXISTS!!!");
            } else{
                System.out.println("FILE DOESN't EXISTS!!!");
            }
        } else{
            Log.d(getClass().getSimpleName(), "upload image -> You haven't picked Image");
        }

        System.out.println("TEST IMAGES: ");
        for (int i = 0; i < imagePaths.size(); i++){
            for (int j = 0; j < 100; j++){
                if (imagePaths.get(i).containsKey(j)){
                    System.out.println("Image #" + i + ": " + imagePaths.get(i).get(j));
                    break;
                }
            }
        }
    }

    private void addPhoto(final Bitmap bitmap, final String imageUri, String imageBase64){
        if (linearLayoutImages.getVisibility() == View.GONE) linearLayoutImages.setVisibility(View.VISIBLE);
        imagesBase64.add(imageBase64);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.setMargins(10,0, 0, 0);

        final ImageView imageView = new ImageView(this);
        imageView.setId(imagePaths.size());
        imageView.setImageBitmap(bitmap);
        imageView.setAdjustViewBounds(true);
        imageView.setLayoutParams(layoutParams);

        linearLayoutImages.addView(imageView);
        HashMap<Integer, String> hm = new HashMap<>();
        hm.put(imageView.getId(), imageUri);
        imagePaths.add(hm);
        System.out.println("Image id = " + imageView.getId());

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPhoto.show();
                clickedImageView = imageView;
                clickedBitmap = bitmap;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(CompanyActivity.ACTIVITY_FOR_RESULT_CODE, new Intent().putExtra(MyIntentKeys.APP_INTENT_FULL_REVIEW_IS_REVIEW_ADDED, false));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bindStatus){
            unbindService(this);
            bindStatus = false;
        }
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder) iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(this)));
        bindStatus = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void startLoadingContent() {

    }

    @Override
    public void stopLoadingContent() {

    }

    @Override
    public void startLoadingReviews() {
        relativeLayoutLoading.setVisibility(View.VISIBLE);
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoadingReviews() {
        relativeLayoutLoading.setVisibility(View.GONE);
        progressBarLoading.setVisibility(View.GONE);
    }

    @Override
    public void addContent(DetailCompany detailCompany) {

    }

    @Override
    public void addReviews(List<CompanyReview> companyReviews) {

    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(relativeLayoutLoading, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void removeAllReview() {

    }

    @Override
    public void fullReviewAdded() {
        setResult(CompanyActivity.ACTIVITY_FOR_RESULT_CODE, new Intent().putExtra(MyIntentKeys.APP_INTENT_FULL_REVIEW_IS_REVIEW_ADDED, true));
        finish();
    }

    private void showSettingsSnackbar(String content){
        Snackbar.make(relativeLayoutLoading, content, Snackbar.LENGTH_LONG)
                .setAction("Настройка", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                })
                .setDuration(5000)
                .show();
    }

    private void bottomBarChoose(int position){
        Intent intent = new Intent(FullReviewActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(MyIntentKeys.APP_INTENT_BOTTOM_BAR_POSITION, position);
        startActivity(intent);
    }
}