package ru.apfo.plantseeds.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.SubtitleCollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.adapter.ListHomeSortAdapter;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.data.api.model.ProductListItem;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.presenter.HomeProductsPresenter;
import ru.apfo.plantseeds.view.HomeProductView;

public class HomeSortActivity extends AppCompatActivity implements ServiceConnection, HomeProductView {
    public static final int ACTIVITY_FOR_RESULT_CODE = 100;

    public static final int DEFAULT_FROM = 1;
    public static final int DEFAULT_TO = 19;
    public static final int DEFAULT_PAGINATION_INCREMENT = 19;
    private int from = DEFAULT_FROM;
    private int to = DEFAULT_TO;
    private int paginationIncrement = DEFAULT_PAGINATION_INCREMENT;
    private boolean flagIsLoadingMore = false;

    private boolean isSorted = false;
    private boolean isSortedFromAToZ = true;
    private ImageView imageViewSort;

    private HomeProductsPresenter presenter;

    private GridViewWithHeaderAndFooter gridView;
    private ListHomeSortAdapter adapter;
    private List<ProductListItem> products;

    private ProgressBar progressBarBottomLoading;

    private HomeCategory category;
    private String subCategory;
    private DetailCompany detailCompany;

    private boolean bindStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_sort);

        initialise();
    }

    private void initialise(){
        category = (HomeCategory) getIntent().getSerializableExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBSUBCATEGORY);
        subCategory = getIntent().getStringExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY);
        detailCompany = (DetailCompany) getIntent().getSerializableExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY);

        BottomNavigationBar bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottomNavigationHomeSort);
        BottomBarItem bottomBarItemHome = new BottomBarItem(R.drawable.icon_flowers, R.string.bottom_bar_home);
        BottomBarItem bottomBarItemSearch = new BottomBarItem(R.drawable.icon_search, R.string.bottom_bar_search);
        BottomBarItem bottomBarItemFavourites = new BottomBarItem(R.drawable.icon_favourite_bottom, R.string.bottom_bar_favourites);
        bottomNavigationBar.addTab(bottomBarItemHome)
                .addTab(bottomBarItemSearch)
                .addTab(bottomBarItemFavourites);
        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                bottomBarChoose(position);
            }
        });
        bottomNavigationBar.setOnReselectListener(new BottomNavigationBar.OnReselectListener() {
            @Override
            public void onReselect(int position) {
                bottomBarChoose(position);
            }
        });

        SubtitleCollapsingToolbarLayout subtitleCollapsingToolbarLayout = (SubtitleCollapsingToolbarLayout) findViewById(R.id.toolbar_layoutHomeSort);
        subtitleCollapsingToolbarLayout.setTitleEnabled(true);
        subtitleCollapsingToolbarLayout.setTitle(category.getTitle());
        if (detailCompany == null) subtitleCollapsingToolbarLayout.setSubtitle(subCategory);
        else subtitleCollapsingToolbarLayout.setSubtitle("Производитель: " + detailCompany.getTitle());
//
//        TextView textViewTitle = (TextView) findViewById(R.id.textViewHomeSortTitle);
//        TextView textViewSubTitle = (TextView) findViewById(R.id.textViewHomeSortSubtitle);
//        textViewTitle.setText(category.getTitle());
//        textViewSubTitle.setText(subCategory);

        Picasso.with(this)
                .load(category.getImg_url())
                .into((ImageView) findViewById(R.id.imageViewHomeSort));


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarHomeSort);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setStatusBarColor(Color.TRANSPARENT);

        gridView = (GridViewWithHeaderAndFooter) findViewById(R.id.gridViewHomeSort);
        gridView.setNestedScrollingEnabled(true);
        LayoutInflater inflater = LayoutInflater.from(this);
        View footer = inflater.inflate(R.layout.card_home_bottom_sort, null);
        progressBarBottomLoading = (ProgressBar) footer.findViewById(R.id.progressBarHomeSortLoading);
        progressBarBottomLoading.setVisibility(View.GONE);
        gridView.addFooterView(footer);

        View header =  inflater.inflate(R.layout.card_home_top_sort, null);
        TextView textViewSort = (TextView) header.findViewById(R.id.textViewHomeSortByName);
        imageViewSort = (ImageView) header.findViewById(R.id.imageViewHomeSortByName);
        if (detailCompany != null) ((TextView) header.findViewById(R.id.textViewHomeTopSortMeta)).setVisibility(View.INVISIBLE);
        View.OnClickListener onClickListenerSort = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sort();
            }
        };
        textViewSort.setOnClickListener(onClickListenerSort);
        imageViewSort.setOnClickListener(onClickListenerSort);
        gridView.addHeaderView(header);
        products = new ArrayList<>();
        adapter = new ListHomeSortAdapter(this, products, this);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openDetail(adapter.getItem(i));
            }
        });
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if(!flagIsLoadingMore) {
                        flagIsLoadingMore = true;
                        loadMoreProducts();
                    }
                }
            }
        });

        presenter = new HomeProductsPresenter(this);

        Intent intent = new Intent(this, Service.class);
        bindStatus = bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bindStatus){
            unbindService(this);
            bindStatus = false;
        }
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(this)));
        bindStatus = true;
        presenter.getProducts(category.getId(), from, to);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void startLoading() {
        progressBarBottomLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBarBottomLoading.setVisibility(View.GONE);
    }

    @Override
    public void addProducts(List<ProductListItem> products) {
        flagIsLoadingMore = false;
        adapter.setItems(products);
        if (isSorted){
            if (isSortedFromAToZ){
                adapter.sortFromAToZ();
            } else {
                adapter.sortFromZToA();
            }
        }
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(gridView, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void openDetail(ProductListItem productListItem) {
        Intent intent = new Intent(this, HomeProductActivity.class);
        intent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SORT, productListItem);
        intent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, subCategory);
        startActivityForResult(intent, ACTIVITY_FOR_RESULT_CODE);
    }

    @Override
    public void addToFavourite(int id) {
        presenter.addProductToFavourite(id);
    }

    @Override
    public void deleteFromFavourite(int id) {
        presenter.deleteProductFromFavourite(id);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) return;
        else {
            ProductListItem returnedItem = (ProductListItem) data.getSerializableExtra(MyIntentKeys.APP_INTENT_HOME_PRODUCT_RESULT);
            adapter.updateListItem(returnedItem);
        }
    }

    private void sort(){
        isSorted = true;
        imageViewSort.setVisibility(View.VISIBLE);
        if (isSortedFromAToZ){
            isSortedFromAToZ = false;
            adapter.sortFromZToA();
            imageViewSort.setImageResource(R.drawable.icon_arrow_forward_up);
        } else {
            isSortedFromAToZ = true;
            adapter.sortFromAToZ();
            imageViewSort.setImageResource(R.drawable.icon_arrow_forward_down);
        }
    }

    private void loadMoreProducts(){
        incrementFromAndToValues();
        presenter.getProducts(category.getId(), from, to);
    }

    private void incrementFromAndToValues(){
        from += paginationIncrement;
        to += paginationIncrement;
    }

    private void bottomBarChoose(int position){
        Intent intent = new Intent(HomeSortActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(MyIntentKeys.APP_INTENT_BOTTOM_BAR_POSITION, position);
        startActivity(intent);
    }
}