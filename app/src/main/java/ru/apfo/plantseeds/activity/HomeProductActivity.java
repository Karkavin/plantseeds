package ru.apfo.plantseeds.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.LinkedList;
import java.util.List;

import ru.apfo.plantseeds.MyBundleKeys;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.ProductListItem;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.fragments.product.HomeProductCompaniesFragment;
import ru.apfo.plantseeds.fragments.product.HomeProductInfoFragment;
import ru.apfo.plantseeds.presenter.HomeProductDetailPresenter;
import ru.apfo.plantseeds.util.ImageUtils;
import ru.apfo.plantseeds.view.HomeProductDetailView;

public class HomeProductActivity extends AppCompatActivity implements HomeProductDetailView, ServiceConnection{

    private HomeProductDetailPresenter presenter;
    private boolean bindStatus;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private ProductListItem productListItem;
    private String sortOfProduct;

    private List<String> imagesList;
    private Uri imageUriToShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_product);

        initialise();
    }

    private void initialise(){
        Fresco.initialize(this);

        productListItem = (ProductListItem) getIntent().getSerializableExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SORT);
        sortOfProduct = getIntent().getStringExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarHomeProduct);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setStatusBarColor(Color.TRANSPARENT);

        BottomNavigationBar bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottomNavigationHomeProduct);
        BottomBarItem bottomBarItemHome = new BottomBarItem(R.drawable.icon_flowers, R.string.bottom_bar_home);
        BottomBarItem bottomBarItemSearch = new BottomBarItem(R.drawable.icon_search, R.string.bottom_bar_search);
        BottomBarItem bottomBarItemFavourites = new BottomBarItem(R.drawable.icon_favourite_bottom, R.string.bottom_bar_favourites);
        bottomNavigationBar.addTab(bottomBarItemHome)
                .addTab(bottomBarItemSearch)
                .addTab(bottomBarItemFavourites);
        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                bottomBarChoose(position);
            }
        });
        bottomNavigationBar.setOnReselectListener(new BottomNavigationBar.OnReselectListener() {
            @Override
            public void onReselect(int position) {
                bottomBarChoose(position);
            }
        });

        presenter = new HomeProductDetailPresenter(this);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabsHomeProduct);
        if (productListItem.getCompanies() != null) {
            tabLayout.getTabAt(1).setText("Производители (" + productListItem.getCompanies().size() + ")");
        } else{
            tabLayout.getTabAt(1).setText("Производители (" + 0 + ")");
        }

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        TextView textViewHomeProductTitle = (TextView) findViewById(R.id.textViewHomeProductTitle);
        textViewHomeProductTitle.setText(productListItem.getProduct().getTitle());

        TextView textViewHomeProductSubTitle = (TextView) findViewById(R.id.textViewHomeProductSubTitle);
        textViewHomeProductSubTitle.setText(sortOfProduct);

        imagesList = new LinkedList<>();
        if (productListItem.getImages() != null) {
            for (int i = 0; i < productListItem.getImages().size(); i++) {
                imagesList.add(productListItem.getImages().get(i).getUrl().getHd());
            }
        }
        if (imagesList.size() > 0){
            Picasso.with(getApplicationContext()).load(productListItem.getImages().get(0).getUrl().getHd()).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    imageUriToShare = new ImageUtils(getApplicationContext()).getLocalBitmapUri(bitmap);
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
        }

        SliderLayout sliderLayout = (SliderLayout) findViewById(R.id.sliderHomeProduct);
        sliderLayout.stopAutoCycle();
        sliderLayout.setCustomIndicator((PagerIndicator) findViewById(R.id.pageIndicatorHomeProduct));
        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for(int i = 0; i < imagesList.size(); i++){
            DefaultSliderView textSliderView = new DefaultSliderView(this);
            int finalI = i;
            textSliderView
                    .image(imagesList.get(i))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            View overlayViewGallery = mInflater.inflate(R.layout.overlay_view_gallery, null);

                            ImageViewer imageViewer = new ImageViewer.Builder(HomeProductActivity.this, imagesList)
                                    .setStartPosition(finalI)
                                    .setOverlayView(overlayViewGallery)
                                    .build();

                            ImageView imageViewExit = (ImageView) overlayViewGallery.findViewById(R.id.imageViewOverlayGallery);
                            imageViewExit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    imageViewer.onDismiss();
                                }
                            });
                            imageViewer.show();
                        }
                    });

            //add your extra information
//            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle()
//                    .putString("extra", name);

            sliderLayout.addSlider(textSliderView);
        }

        Intent intent = new Intent(this, Service.class);
        bindStatus = bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_home_product_is_favourite:
                if (bindStatus) {
                    if (productListItem.getProduct().getFavorite().equals("yes")) {
                        productListItem.getProduct().setFavorite("no");
                        item.setIcon(ContextCompat.getDrawable(getApplicationContext(), R.drawable.icon_favourite_mini));
                        deleteFromFavourite((int) productListItem.getProduct().getId());
                    } else {
                        productListItem.getProduct().setFavorite("yes");
                        item.setIcon(ContextCompat.getDrawable(getApplicationContext(), R.drawable.favourite_active_mini));
                        addToFavourite((int) productListItem.getProduct().getId());
                    }
                }
                break;
            case R.id.action_home_product_other:
                if (productListItem.getProduct().getImage() != null) {
                    Picasso.with(getApplicationContext()).load(productListItem.getProduct().getImage()).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, "'Посади семена'\n\nТовар: '" + productListItem.getProduct().getTitle() + "'\n\n" + productListItem.getProduct().getDescr());
                            intent.putExtra(Intent.EXTRA_STREAM, new ImageUtils(getApplicationContext()).getLocalBitmapUri(bitmap));
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            intent.setType("image/*");
                            startActivity(Intent.createChooser(intent, "Поделиться товаром"));
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            showSnackbarMessage("Невозможно поделиться. Ошибка при получении изображения");
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
                } else{
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, "'Посади семена'\n\nТовар: '" + productListItem.getProduct().getTitle() + "'\n\n" + productListItem.getProduct().getDescr());
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setType("text/plain");
                    startActivity(Intent.createChooser(intent, "Поделиться товаром"));
                }
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home_product, menu);

        if (productListItem.getProduct().getFavorite().equals("yes")) {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.favourite_active_mini));
        } else{
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.icon_favourite_mini));
        }

        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bindStatus){
            unbindService(this);
            bindStatus = false;
        }
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onBackPressed() {
        Log.d(getClass().getSimpleName(), "Before... like is: " + productListItem.getProduct().getFavorite());
        setResult(HomeSortActivity.ACTIVITY_FOR_RESULT_CODE, new Intent().putExtra(MyIntentKeys.APP_INTENT_HOME_PRODUCT_RESULT, productListItem));
        finish();
    }

    private void bottomBarChoose(int position){
        Intent intent = new Intent(HomeProductActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(MyIntentKeys.APP_INTENT_BOTTOM_BAR_POSITION, position);
        startActivity(intent);
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(this)));
        bindStatus = true;
        Log.d(getClass().getSimpleName(), "Service Connected Product!!!");
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(mViewPager, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void addToFavourite(int id) {
        presenter.addProductToFavourite(id);
    }

    @Override
    public void deleteFromFavourite(int id) {
        presenter.deleteProductFromFavourite(id);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    HomeProductInfoFragment homeProductInfoFragment = new HomeProductInfoFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putSerializable(MyBundleKeys.APP_BUNDLE_HOME_PRODUCT_INFO_FRAGMENT, productListItem);
                    homeProductInfoFragment.setArguments(bundle1);
                    return homeProductInfoFragment;
                case 1:
                    HomeProductCompaniesFragment homeProductCompaniesFragment= new HomeProductCompaniesFragment();
                    Bundle bundle2 = new Bundle();
                    bundle2.putSerializable(MyBundleKeys.APP_BUNDLE_HOME_PRODUCT_COMPANIES_FRAGMENT, productListItem);
                    homeProductCompaniesFragment.setArguments(bundle2);
                    return homeProductCompaniesFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}