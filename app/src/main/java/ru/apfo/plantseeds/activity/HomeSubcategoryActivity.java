package ru.apfo.plantseeds.activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.fragments.main.HomeSubcategoryFragment;

public class HomeSubcategoryActivity extends AppCompatActivity
        implements HomeSubcategoryFragment.ActivityCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_subcategory);

        initialise();
    }

    private void initialise(){
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FragmentManager fragmentManager = getSupportFragmentManager();

        HomeCategory homeCategory = (HomeCategory) getIntent().getSerializableExtra(MyIntentKeys.APP_INTENT_HOME_FROM_CATEGORY);
        DetailCompany detailCompany = (DetailCompany) getIntent().getSerializableExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY);
        HomeSubcategoryFragment homeSubcategoryFragment = HomeSubcategoryFragment.newInstance(homeCategory, true, detailCompany);
        homeSubcategoryFragment.setActivity(this);
        fragmentManager.beginTransaction().add(R.id.frameLayoutHomeSubcategoryActivity, homeSubcategoryFragment).commit();
    }

    @Override
    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) finish();
        return super.onOptionsItemSelected(item);
    }
}