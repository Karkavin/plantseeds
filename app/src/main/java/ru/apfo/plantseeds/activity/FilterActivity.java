package ru.apfo.plantseeds.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.adapter.filter.BaseFilterAdapter;
import ru.apfo.plantseeds.adapter.filter.FilterTypeColorSelectAdapter;
import ru.apfo.plantseeds.adapter.filter.FilterTypeMultiplyCollapsedSelectAdapter;
import ru.apfo.plantseeds.adapter.filter.FilterTypeMultiplySelectAdapter;
import ru.apfo.plantseeds.adapter.filter.FilterTypeRangeSelectAdapter;
import ru.apfo.plantseeds.adapter.filter.FilterTypeSingleSelectAdapter;
import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueMultiply;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueRange;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueSingleSelect;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.presenter.FilterPresenter;
import ru.apfo.plantseeds.view.FilterView;

public class FilterActivity extends AppCompatActivity implements ServiceConnection, FilterView {
    public static final int ACTIVITY_RESULT_CODE = 110;

    private ScrollView scrollViewFilters;
    private RelativeLayout relativeLayoutLoading;

    private FilterPresenter presenter;
    private boolean bindStatus;

    private List<BaseFilter> filters;
    private List<BaseFilterAdapter> baseFilterAdapters;

    private int categoryType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        getSupportActionBar().hide();

        Intent intent = getIntent();
        categoryType = intent.getIntExtra(MyIntentKeys.APP_INTENT_FILTER_CATEGORY_TYPE, 0);
        baseFilterAdapters = new ArrayList<>();
        filters = (List<BaseFilter>) intent.getSerializableExtra(MyIntentKeys.APP_INTENT_FILTERS);

        presenter = new FilterPresenter(this);

        scrollViewFilters = (ScrollView) findViewById(R.id.scrollViewFilters);
        scrollViewFilters.setVisibility(View.GONE);
        relativeLayoutLoading = (RelativeLayout) findViewById(R.id.relativeLayoutFilterLoading);
        relativeLayoutLoading.setVisibility(View.VISIBLE);

        findViewById(R.id.imageViewFilterClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.textViewFilterTitle)).setText(intent.getStringExtra(MyIntentKeys.APP_INTENT_FILTER_TITLE));

        ((RelativeLayout) findViewById(R.id.relativeLayoutFilterClear)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < baseFilterAdapters.size(); i++){
                    baseFilterAdapters.get(i).disableAllData();
                }

                Snackbar.make(findViewById(R.id.imageViewFilterClose), "Фильтры успешно очищены", Snackbar.LENGTH_LONG).show();
            }
        });

        findViewById(R.id.buttonFiltersAccept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

//        if (filters != null){
//            stopLoading();
//            addFilters(filters);
//        } else {
            Intent intentBind = new Intent(this, Service.class);
            bindStatus = bindService(intentBind, this, Context.BIND_AUTO_CREATE);
//        }
    }

    @Override
    public void onBackPressed() {
        for (int i = 0; i < filters.size(); i++){
            filters.set(i, baseFilterAdapters.get(i).getResultFilter());
        }

        Intent sendData = new Intent();
        sendData.putExtra(MyIntentKeys.APP_INTENT_FILTERS, (Serializable) filters);
        setResult(Activity.RESULT_OK, sendData);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACTIVITY_RESULT_CODE) {
            if (resultCode == Activity.RESULT_OK){
                FilterValueMultiply filterValueCollapsed = (FilterValueMultiply) data.getSerializableExtra(MyIntentKeys.APP_INTENT_MULTIPLY_FILTER_VALUES_SEND);
                int spec_id = data.getIntExtra(MyIntentKeys.APP_INTENT_MULTIPLY_FILTER_VALUES_ID, 0);
                for (int i = 0; i < filters.size(); i++){
                    if ((filters.get(i)).getSpec_id() == spec_id){
                        ((FilterTypeMultiplyCollapsedSelectAdapter) baseFilterAdapters.get(i)).updateFilter(filterValueCollapsed);
                        break;
                    }
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(getClass().getSimpleName(), "Get Filters (onDestroy)!!!!!");
        if (bindStatus){
            unbindService(this);
            bindStatus = false;
        }
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(this)));
        bindStatus = true;
//        if (filters == null) {
            Log.d(getClass().getSimpleName(), "Get Filters!!!!!");
            presenter.getFilters();

//        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void addFilters(List<BaseFilter> filters) {
        this.filters = filters;
        LinearLayout linearLayoutBase = ((LinearLayout) findViewById(R.id.linearLayoutFilterBase));
        for (int i = 0; i < filters.size(); i++){
            switch (filters.get(i).getType()){
                case "checked_single":
                    FilterValueSingleSelect filterValueSingleSelect = (FilterValueSingleSelect) filters.get(i);
                    FilterTypeSingleSelectAdapter filterTypeSingleSelectAdapter = new FilterTypeSingleSelectAdapter(
                            this,
                            filterValueSingleSelect,
                            linearLayoutBase
                    );
                    baseFilterAdapters.add(filterTypeSingleSelectAdapter);
                    break;
                case "range":
                    FilterValueRange filterValueRange = (FilterValueRange) filters.get(i);
                    FilterTypeRangeSelectAdapter filterTypeRangeSelectAdapter = new FilterTypeRangeSelectAdapter(
                            this,
                            filterValueRange,
                            linearLayoutBase
                    );
                    baseFilterAdapters.add(filterTypeRangeSelectAdapter);
                    break;
                case "checked_multi":
                    FilterValueMultiply filterValueMultiply = (FilterValueMultiply) filters.get(i);
                    if (filterValueMultiply.getCollapsed().equals("no")){
                        FilterTypeMultiplySelectAdapter filterTypeMultiplySelectAdapter = new FilterTypeMultiplySelectAdapter(
                                this,
                                filterValueMultiply,
                                linearLayoutBase
                        );
                        baseFilterAdapters.add(filterTypeMultiplySelectAdapter);
                    } else if (filterValueMultiply.getCollapsed().equals("yes")){
                        FilterTypeMultiplyCollapsedSelectAdapter filterTypeMultiplyCollapsedSelectAdapter = new FilterTypeMultiplyCollapsedSelectAdapter(
                                this,
                                filterValueMultiply,
                                linearLayoutBase
                        );
                        baseFilterAdapters.add(filterTypeMultiplyCollapsedSelectAdapter);
                    }
                    break;
                case "colors":
                    FilterValueMultiply filterValueMultiplyColors = (FilterValueMultiply) filters.get(i);
                    FilterTypeColorSelectAdapter filterTypeColorSelectAdapter = new FilterTypeColorSelectAdapter(
                            this,
                            filterValueMultiplyColors,
                            linearLayoutBase
                    );
                    baseFilterAdapters.add(filterTypeColorSelectAdapter);
                    break;
            }
        }
    }

    @Override
    public void startLoading() {
        scrollViewFilters.setVisibility(View.GONE);
        relativeLayoutLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        scrollViewFilters.setVisibility(View.VISIBLE);
        relativeLayoutLoading.setVisibility(View.GONE);
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(findViewById(R.id.imageViewFilterClose), msg, Snackbar.LENGTH_LONG).show();
    }
}