package ru.apfo.plantseeds.fragments.nav;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.CompanyCategoryActivity;
import ru.apfo.plantseeds.adapter.ListHomeProductCompaniesAdapter;
import ru.apfo.plantseeds.adapter.ListHomeProductCompaniesSortAdapter;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.fragments.dialog.HomeProductBottomSheetDialogFragment;
import ru.apfo.plantseeds.presenter.CompaniesPresenter;
import ru.apfo.plantseeds.view.CompaniesView;

public class CompaniesMainFragment extends Fragment implements ServiceConnection, CompaniesView,
        HomeProductBottomSheetDialogFragment.HomeProductBottomSheetDialogFragmentListener{
    private ProgressBar progressBarLoading;

    private Context context;

    private boolean bindStatus;

    private CompaniesPresenter presenter;

    private List<DetailCompany> companies;
    private ListHomeProductCompaniesAdapter adapter;
    private ListView listViewCompanies;

    private HomeProductBottomSheetDialogFragment bottomSheetDialogFragment;

    private TextView textViewSort;
    private ImageView imageViewSort;

    public CompaniesMainFragment() {

    }

    public static CompaniesMainFragment newInstance() {
        return new CompaniesMainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_companies_main, container, false);

        context = getActivity().getApplicationContext();

        progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarFragmentCompanies);
        progressBarLoading.setVisibility(View.VISIBLE);

        companies = new ArrayList<>();
        listViewCompanies = (ListView) view.findViewById(R.id.listViewCompanies);
        adapter = new ListHomeProductCompaniesAdapter(getActivity(), DetailCompany.detailCompanyListToCompanyList(companies));
        listViewCompanies.setAdapter(adapter);

        LayoutInflater mInflater = LayoutInflater.from(getActivity());
        View header =  inflater.inflate(R.layout.card_home_product_top_sort, null);
        textViewSort = (TextView) header.findViewById(R.id.textViewHomeProductSortByName);
        imageViewSort = (ImageView) header.findViewById(R.id.imageViewHomeProductSort);
        bottomSheetDialogFragment = new HomeProductBottomSheetDialogFragment();
        bottomSheetDialogFragment.setListener(this);
        View.OnClickListener onClickListenerSort = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogFragment.show(getFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        };
        textViewSort.setOnClickListener(onClickListenerSort);
        imageViewSort.setOnClickListener(onClickListenerSort);

        listViewCompanies.addHeaderView(header);
        listViewCompanies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    Intent intent = new Intent(context, CompanyCategoryActivity.class);
                    intent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, companies.get(i-1));
                    startActivity(intent);
                }
            }
        });

        Intent intent = new Intent(getActivity(), Service.class);
        bindStatus = getActivity().bindService(intent, this, Context.BIND_AUTO_CREATE);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new CompaniesPresenter(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(context)));
        bindStatus = true;
        presenter.getCompanies();
    }

    @Override
    public void startLoading() {
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBarLoading.setVisibility(View.GONE);
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(progressBarLoading, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void companiesLoaded(List<DetailCompany> listCompanies) {
        this.companies = listCompanies;
        adapter.sortFromAToZ();
        adapter.setItems(DetailCompany.detailCompanyListToCompanyList(companies));
    }

    @Override
    public void onSortTypeSelected(ListHomeProductCompaniesSortAdapter.SortType sortType) {
        switch (sortType){
            case SORT_TYPE_FROM_A_TO_Z:
                textViewSort.setText("По названию");
                imageViewSort.setVisibility(View.VISIBLE);
                imageViewSort.setImageResource(R.drawable.icon_arrow_forward_down);
                adapter.sortFromAToZ();
                break;
            case SORT_TYPE_FROM_Z_TO_A:
                textViewSort.setText("По названию");
                imageViewSort.setVisibility(View.VISIBLE);
                imageViewSort.setImageResource(R.drawable.icon_arrow_forward_up);
                adapter.sortFromZToA();
                break;
            case RATING:
                textViewSort.setText("По рейтингу");
                imageViewSort.setVisibility(View.GONE);
                adapter.sortByRating();
                break;
            default:
                textViewSort.setText("По названию");
                imageViewSort.setVisibility(View.VISIBLE);
                imageViewSort.setImageResource(R.drawable.icon_arrow_forward_down);
                adapter.sortFromAToZ();
                break;
        }
    }
}