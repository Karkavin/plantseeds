package ru.apfo.plantseeds.fragments.nav;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import ru.apfo.plantseeds.MyBundleKeys;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.HomeSortActivity;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.fragments.main.HomeSubSubcategoryFragment;
import ru.apfo.plantseeds.fragments.main.HomeSubcategoryFragment;

public class FlowersMainFragment extends Fragment {
    private ProgressBar progressBarLoading;

    public FlowersMainFragment() {

    }

    public static FlowersMainFragment newInstance(HomeCategory homeCategory) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(MyBundleKeys.APP_BUNDLE_FLOWERS_HOME_CATEGORY, homeCategory);

        FlowersMainFragment fragment = new FlowersMainFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_flowers_main, container, false);

        progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarFragmentFlowers);
        progressBarLoading.setVisibility(View.VISIBLE);

        HomeCategory homeCategory = (HomeCategory) getArguments().getSerializable(MyBundleKeys.APP_BUNDLE_FLOWERS_HOME_CATEGORY);
        openNextCategories(homeCategory);

        return view;
    }

    private void openNextCategories(HomeCategory category) {
        progressBarLoading.setVisibility(View.GONE);

        if (category.getIs_last() == 1){
            Intent intent = new Intent(getActivity(), HomeSortActivity.class);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBSUBCATEGORY, category);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, category.getTitle());
            startActivity(intent);
        } else {
            switch (category.getView_type()){
                case 3:
                    HomeSubcategoryFragment homeSubcategoryFragment = HomeSubcategoryFragment.newInstance(category, false);
                    getFragmentManager().beginTransaction().replace(R.id.frameLayoutFlowers, homeSubcategoryFragment).commit();
                    break;
                case 4:
                    HomeSubSubcategoryFragment homeSubSubcategoryFragment = HomeSubSubcategoryFragment.newInstance(category, false);
                    getFragmentManager().beginTransaction().replace(R.id.frameLayoutFlowers, homeSubSubcategoryFragment).commit();
                    break;
                default:
                    break;
            }
        }
    }
}