package ru.apfo.plantseeds.fragments.main;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.HomeProductActivity;
import ru.apfo.plantseeds.adapter.ListHomeSortAdapter;
import ru.apfo.plantseeds.data.api.model.ProductListItem;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.presenter.HomeProductsPresenter;
import ru.apfo.plantseeds.view.HomeProductView;

public class FavouriteMainFragment extends Fragment implements ServiceConnection, HomeProductView {
    public static final int ACTIVITY_FOR_RESULT_CODE = 100;

    private boolean isSorted = false;
    private boolean isSortedFromAToZ = true;
    private ImageView imageViewSort;

    private Context context;

    private ProgressBar progressBarBottomLoading;

    private GridViewWithHeaderAndFooter gridView;
    private ListHomeSortAdapter adapter;
    private List<ProductListItem> products;

    private HomeProductsPresenter presenter;

    private boolean bindStatus;

    public FavouriteMainFragment() {

    }

    public static FavouriteMainFragment newInstance() {
        return new FavouriteMainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite_main, container, false);

        context = getActivity().getApplicationContext();

        gridView = (GridViewWithHeaderAndFooter) view.findViewById(R.id.gridViewFavourite);
        LayoutInflater mInflater = LayoutInflater.from(context);
        View footer = mInflater.inflate(R.layout.card_home_bottom_sort, null);
        progressBarBottomLoading = (ProgressBar) footer.findViewById(R.id.progressBarHomeSortLoading);
        progressBarBottomLoading.setVisibility(View.GONE);
        gridView.addFooterView(footer);

        View header =  inflater.inflate(R.layout.card_home_top_sort, null);
        TextView textViewSort = (TextView) header.findViewById(R.id.textViewHomeSortByName);
        ((TextView) header.findViewById(R.id.textViewHomeTopSortMeta)).setVisibility(View.GONE);
        imageViewSort = (ImageView) header.findViewById(R.id.imageViewHomeSortByName);
        View.OnClickListener onClickListenerSort = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sort();
            }
        };

        textViewSort.setOnClickListener(onClickListenerSort);
        imageViewSort.setOnClickListener(onClickListenerSort);
        gridView.addHeaderView(header);
        products = new ArrayList<>();
        adapter = new ListHomeSortAdapter(context, products, this);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openDetail(adapter.getItem(i));
            }
        });

        Intent intent = new Intent(getActivity(), Service.class);
        bindStatus = getActivity().bindService(intent, this, Context.BIND_AUTO_CREATE);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new HomeProductsPresenter(this);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden && bindStatus){
            adapter.clean();
            presenter.getFavouriteProducts();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(context)));
        bindStatus = true;
        presenter.getFavouriteProducts();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void startLoading() {
        progressBarBottomLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBarBottomLoading.setVisibility(View.GONE);
    }

    @Override
    public void addProducts(List<ProductListItem> products) {
        adapter.setItems(products);
        if (isSorted){
            if (isSortedFromAToZ){
                adapter.sortFromAToZ();
            } else {
                adapter.sortFromZToA();
            }
        }
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(gridView, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void openDetail(ProductListItem productListItem) {
        Intent intent = new Intent(getActivity(), HomeProductActivity.class);
        intent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SORT, productListItem);
        intent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, "Избранные");
        startActivityForResult(intent, ACTIVITY_FOR_RESULT_CODE);
    }

    @Override
    public void addToFavourite(int id) {
        presenter.addProductToFavourite(id);
    }

    @Override
    public void deleteFromFavourite(int id) {
        presenter.deleteProductFromFavourite(id);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) return;
        else {
            ProductListItem returnedItem = (ProductListItem) data.getSerializableExtra(MyIntentKeys.APP_INTENT_HOME_PRODUCT_RESULT);
            adapter.updateListItem(returnedItem);
        }
    }

    private void sort() {
        isSorted = true;
        imageViewSort.setVisibility(View.VISIBLE);
        if (isSortedFromAToZ){
            isSortedFromAToZ = false;
            adapter.sortFromZToA();
            imageViewSort.setImageResource(R.drawable.icon_arrow_forward_up);
        } else {
            isSortedFromAToZ = true;
            adapter.sortFromAToZ();
            imageViewSort.setImageResource(R.drawable.icon_arrow_forward_down);
        }
    }
}