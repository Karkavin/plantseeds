package ru.apfo.plantseeds.fragments.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import ru.apfo.plantseeds.MyBundleKeys;
import ru.apfo.plantseeds.R;

public class CompanyReviewDialogFragment extends DialogFragment {
    public static final String TAG_REVIEW = "Review";
    private CompanyReviewDialogFragmentListener mListener;

    public static CompanyReviewDialogFragment newInstance(int rating){
        Bundle arguments = new Bundle();
        arguments.putInt(MyBundleKeys.APP_BUNDLE_COMPANY_REVIEW_RATING, rating);
        CompanyReviewDialogFragment dialogFragment = new CompanyReviewDialogFragment();
        dialogFragment.setArguments(arguments);

        return dialogFragment;
    }

    public void setListener(CompanyReviewDialogFragmentListener mListener){
        this.mListener = mListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int rating = getArguments().getInt(MyBundleKeys.APP_BUNDLE_COMPANY_REVIEW_RATING);

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.review_dialog_fragment, null);
        TextView textViewRating = (TextView) view.findViewById(R.id.textViewReviewDialogRating);
        MaterialRatingBar ratingBar = (MaterialRatingBar) view.findViewById(R.id.ratingBarReviewDialog);
        ((Button) view.findViewById(R.id.buttonReviewDialogRatingFullReview)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onDialogCompanyFullReview(CompanyReviewDialogFragment.this, rating);
                    dismiss();
                }
            }
        });

        ((Button) view.findViewById(R.id.buttonReviewDialogRatingShortReview)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onDialogCompanyShortReview(CompanyReviewDialogFragment.this, rating);
                    dismiss();
                }
            }
        });
        textViewRating.setText(rating + "");
        ratingBar.setRating(rating);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);

        return builder.create();
    }

    public interface CompanyReviewDialogFragmentListener{
        void onDialogCompanyFullReview(DialogFragment dialog, int rating);
        void onDialogCompanyShortReview(DialogFragment dialog, int rating);
    }
}