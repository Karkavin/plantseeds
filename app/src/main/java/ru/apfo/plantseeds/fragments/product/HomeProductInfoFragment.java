package ru.apfo.plantseeds.fragments.product;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.apfo.plantseeds.MyBundleKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.ProductListItem;

public class HomeProductInfoFragment extends Fragment{
    private ProductListItem productListItem;

    public HomeProductInfoFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home_product_info, container, false);

        Bundle bundle = getArguments();
        productListItem = (ProductListItem) bundle.getSerializable(MyBundleKeys.APP_BUNDLE_HOME_PRODUCT_INFO_FRAGMENT);

        LinearLayout linearLayout = (LinearLayout) rootView.findViewById(R.id.linearLayoutHomeProductInfo);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView textViewTitle = new TextView(getContext());
        textViewTitle.setLayoutParams(layoutParams);
        textViewTitle.setTextColor(Color.BLACK);
        textViewTitle.setPadding(0, 0, 0, 5);

        TextView textViewContent = new TextView(getContext());
        textViewContent.setLayoutParams(layoutParams);
        textViewContent.setPadding(0, 0, 0, 15);

        TextView textViewHomeProductInfoDescr = (TextView) rootView.findViewById(R.id.textViewHomeProductInfoDescr);
        textViewHomeProductInfoDescr.setText(productListItem.getProduct().getDescr() + "");

        //Gosreestr
        textViewTitle.setText("Входит в госреестр");
        textViewContent.setText(productListItem.getProduct().getGosreestr().equals("yes") ? "Да" : "Нет");
        linearLayout.addView(textViewTitle);
        linearLayout.addView(textViewContent);

        //Spec
        if (productListItem.getSpec() != null) {
            for (int i = 0; i < productListItem.getSpec().size(); i++) {
                textViewTitle = new TextView(getContext());
                textViewTitle.setLayoutParams(layoutParams);
                textViewTitle.setTextColor(Color.BLACK);
                textViewTitle.setPadding(0, 0, 0, 5);

                textViewContent = new TextView(getContext());
                textViewContent.setLayoutParams(layoutParams);
                textViewContent.setPadding(0, 0, 0, 15);

                textViewTitle.setText(productListItem.getSpec().get(i).getTitle() + "");
                textViewContent.setText(productListItem.getSpec().get(i).getValue() + "");
                linearLayout.addView(textViewTitle);
                linearLayout.addView(textViewContent);
            }
        }
        return rootView;
    }
}