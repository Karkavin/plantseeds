package ru.apfo.plantseeds.fragments.main;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.MyBundleKeys;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.HomeSortActivity;
import ru.apfo.plantseeds.activity.HomeSubSubcategoryActivity;
import ru.apfo.plantseeds.activity.HomeSubcategoryActivity;
import ru.apfo.plantseeds.activity.MainActivity;
import ru.apfo.plantseeds.adapter.ListHomeSubcategoryAdapter;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.presenter.HomeCategoryPresenter;
import ru.apfo.plantseeds.view.HomeCategoryView;

public class HomeSubcategoryFragment extends Fragment implements HomeCategoryView, ServiceConnection {
    private Context context;

    private HomeCategoryPresenter presenter;
    private ActivityCallback activityCallback;

    private ProgressBar progressBarLoading;

    private ListHomeSubcategoryAdapter adapter;
    private List<HomeCategory> subcategories;
    private ListView listView;

    private boolean bindStatus;

    private HomeCategory homeCategory;

    private DetailCompany detailCompanyFromNav;

    public HomeSubcategoryFragment() {

    }

    public static HomeSubcategoryFragment newInstance(HomeCategory homeCategory, boolean bottomBarVisibility) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUBCATEGORY, homeCategory);
        arguments.putBoolean(MyBundleKeys.APP_BUNDLE_HOME_SUBCATEGORY_BOTTOM_BAR_VISIBILITY, bottomBarVisibility);
        HomeSubcategoryFragment fragment = new HomeSubcategoryFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    public static HomeSubcategoryFragment newInstance(HomeCategory homeCategory, boolean bottomBarVisibility, DetailCompany detailCompany) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUBCATEGORY, homeCategory);
        arguments.putBoolean(MyBundleKeys.APP_BUNDLE_HOME_SUBCATEGORY_BOTTOM_BAR_VISIBILITY, bottomBarVisibility);
        arguments.putSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUB_CATEGORY_DETAIL_COMPANY_FROM_NAV, detailCompany);
        HomeSubcategoryFragment fragment = new HomeSubcategoryFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    public void setActivity(ActivityCallback activityCallback){
        this.activityCallback = activityCallback;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_subcategory, container, false);

        context = getActivity().getApplicationContext();

        homeCategory = (HomeCategory) getArguments().getSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUBCATEGORY);
        boolean bottomBarVisibility = getArguments().getBoolean(MyBundleKeys.APP_BUNDLE_HOME_SUBCATEGORY_BOTTOM_BAR_VISIBILITY);
        detailCompanyFromNav = (DetailCompany) getArguments().getSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUB_CATEGORY_DETAIL_COMPANY_FROM_NAV);
        if (activityCallback != null) {
            activityCallback.setActionBarTitle(homeCategory.getTitle());
        }

        BottomNavigationBar bottomNavigationBar = (BottomNavigationBar) view.findViewById(R.id.bottomNavigationSubcategory);
        if (bottomBarVisibility) bottomNavigationBar.setVisibility(View.VISIBLE);
        BottomBarItem bottomBarItemHome = new BottomBarItem(R.drawable.icon_flowers, R.string.bottom_bar_home);
        BottomBarItem bottomBarItemSearch = new BottomBarItem(R.drawable.icon_search, R.string.bottom_bar_search);
        BottomBarItem bottomBarItemFavourites = new BottomBarItem(R.drawable.icon_favourite_bottom, R.string.bottom_bar_favourites);
        bottomNavigationBar.addTab(bottomBarItemHome)
                .addTab(bottomBarItemSearch)
                .addTab(bottomBarItemFavourites);
        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                bottomBarChoose(position);
            }
        });
        bottomNavigationBar.setOnReselectListener(new BottomNavigationBar.OnReselectListener() {
            @Override
            public void onReselect(int position) {
                bottomBarChoose(position);
            }
        });

        progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarHomeSubcategoryLoading);

        listView = (ListView) view.findViewById(R.id.listViewHomeSubcategories);
        subcategories = new ArrayList<>();
        adapter = new ListHomeSubcategoryAdapter(getActivity(), subcategories);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openNextCategories(adapter.getItem(i));
            }
        });

        Intent intent = new Intent(getActivity(), Service.class);
        bindStatus = getActivity().bindService(intent, this, Context.BIND_AUTO_CREATE);

        return view;
    }

    private void bottomBarChoose(int position){
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(MyIntentKeys.APP_INTENT_BOTTOM_BAR_POSITION, position);
        startActivity(intent);
    }

    @Override
    public void startLoading() {
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBarLoading.setVisibility(View.GONE);
    }

    @Override
    public void addCategories(List<HomeCategory> categories) {
        adapter.setItems(categories);
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(listView, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void openNextCategories(HomeCategory category) {
        Log.d(getClass().getSimpleName(), "SUPERMAN Open categories from HomeSubCategoryFragment!");
        if (detailCompanyFromNav == null) Log.d(getClass().getSimpleName(), "SUPERMAN DetailCompany title: sorry, u are not from companies!");
        else Log.d(getClass().getSimpleName(), "SUPERMAN DetailCompany title: " + detailCompanyFromNav.getTitle());
        if (category.getIs_last() == 1){
            Intent intent = new Intent(getActivity(), HomeSortActivity.class);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBSUBCATEGORY, category);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, homeCategory.getTitle());
            intent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompanyFromNav);
            startActivity(intent);
        } else {
            Intent destIntent;
            switch (category.getView_type()){
                case 3:
                    destIntent = new Intent(getActivity(), HomeSubcategoryActivity.class);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_CATEGORY, category);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, homeCategory.getTitle());
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompanyFromNav);
                    break;
                case 4:
                    destIntent = new Intent(getActivity(), HomeSubSubcategoryActivity.class);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBCATEGORY, category);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, homeCategory.getTitle());
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompanyFromNav);
                    break;
                default:
                    return;
            }
            startActivity(destIntent);
        }
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        Log.d(getClass().getSimpleName(), "Presenter: " + (presenter == null));
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(context)));
        bindStatus = true;
        long company_id = 0;
        if (detailCompanyFromNav != null) company_id = detailCompanyFromNav.getId();
        presenter.getAllSubcategories(homeCategory.getId(), (int) company_id);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new HomeCategoryPresenter(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    public interface ActivityCallback{
        void setActionBarTitle(String title);
    }
}
