package ru.apfo.plantseeds.fragments.main;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mancj.materialsearchbar.MaterialSearchBar;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.HomeSortActivity;
import ru.apfo.plantseeds.activity.HomeSubSubcategoryActivity;
import ru.apfo.plantseeds.activity.HomeSubcategoryActivity;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.fragments.search.SearchFlowersFragment;
import ru.apfo.plantseeds.fragments.search.SearchVegetablesFragment;
import ru.apfo.plantseeds.presenter.HomeSearchPresenter;
import ru.apfo.plantseeds.util.PreferencesAdapter;
import ru.apfo.plantseeds.util.suggestion.HomeCategorySearchHistoryAdapter;
import ru.apfo.plantseeds.util.suggestion.HomeCategorySearchSuggestionsAdapter;
import ru.apfo.plantseeds.view.HomeSearchView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class SearchMainFragment extends Fragment implements ServiceConnection, HomeSearchView{

    private Context context;

    private boolean bindStatus;
    private HomeSearchPresenter presenter;

    private SearchMainFragment.SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private int currentTabIndex = 0; //0 - Vegetables, 1 - Flowers

    private RelativeLayout relativeLayoutFragmentSearchMain;

    private MaterialSearchBar searchBar;
    private HomeCategorySearchSuggestionsAdapter adapterSuggections;
    private HomeCategorySearchHistoryAdapter adapterHistory;
    private List<HomeCategory> searchDataVegetables = new ArrayList<>();
    private List<HomeCategory> searchDataFlowers = new ArrayList<>();

    private SharedPreferences preferences;
    private PreferencesAdapter preferencesAdapter;
    private List<HomeCategory> lastSuggestions = new ArrayList<>();

    public SearchMainFragment() {

    }

    public static SearchMainFragment newInstance() {
        return new SearchMainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_main, container, false);

        context = getActivity().getApplicationContext();
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferencesAdapter = new PreferencesAdapter();
        lastSuggestions = preferencesAdapter.getSuggestionsFromPreferences(preferences);

        relativeLayoutFragmentSearchMain = (RelativeLayout) view.findViewById(R.id.relativeLayoutFragmentSearchMain);
        relativeLayoutFragmentSearchMain.setVisibility(View.VISIBLE);

        searchBar = getActivity().findViewById(R.id.searchBar);
        searchBar.setHint(currentTabIndex == 0 ? "Искать овощи" : "Искать цветы");

        mSectionsPagerAdapter = new SearchMainFragment.SectionsPagerAdapter(getActivity().getSupportFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.viewPagerSearch);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabsSearch);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                currentTabIndex = tab.getPosition();
                searchBar.setHint(currentTabIndex == 0 ? "Искать овощи" : "Искать цветы");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (searchBar.getText().length() > 0){
                    if (currentTabIndex == 0){
                        adapterSuggections.setSuggestions(searchDataVegetables);
                    } else {
                        adapterSuggections.setSuggestions(searchDataFlowers);
                    }
                    searchBar.setCustomSuggestionAdapter(adapterSuggections);
                    adapterSuggections.getFilter().filter(searchBar.getText());
                } else{
                    if (lastSuggestions.size() == 0){
                        if (currentTabIndex == 0) {
                            adapterSuggections.setSuggestions(searchDataVegetables);
                        } else{
                            adapterSuggections.setSuggestions(searchDataFlowers);
                        }
                        searchBar.setCustomSuggestionAdapter(adapterSuggections);
                    } else {
                        adapterHistory.setSuggestions(lastSuggestions);
                        searchBar.setCustomSuggestionAdapter(adapterHistory);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                if (enabled) {
                    if (lastSuggestions.size() == 0){
                        if (currentTabIndex == 0) {
                            adapterSuggections.setSuggestions(searchDataVegetables);
                        } else{
                            adapterSuggections.setSuggestions(searchDataFlowers);
                        }
                        searchBar.setCustomSuggestionAdapter(adapterSuggections);
                    } else {
                        adapterHistory.setSuggestions(lastSuggestions);
                        searchBar.setCustomSuggestionAdapter(adapterHistory);
                    }
                    searchBar.showSuggestionsList();
                    relativeLayoutFragmentSearchMain.setVisibility(View.GONE);
                } else{
                    searchBar.setText("");
                    searchBar.hideSuggestionsList();
                    relativeLayoutFragmentSearchMain.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                searchBar.showSuggestionsList();
                adapterSuggections.getFilter().filter(searchBar.getText());
                relativeLayoutFragmentSearchMain.setVisibility(View.GONE);
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });

        adapterSuggections = new HomeCategorySearchSuggestionsAdapter((LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE), new HomeCategorySearchSuggestionsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(HomeCategory item) {
                if (!lastSuggestions.contains(item)){
                    lastSuggestions.add(item);
                }

                preferencesAdapter.setSuggestionToPreferences(preferences, lastSuggestions);
                searchBar.setText(item.getTitle());
//                searchBar.hideSuggestionsList();
//                relativeLayoutFragmentSearchMain.setVisibility(View.VISIBLE);
//                View view = getActivity().getCurrentFocus();
//                if (view != null) {
//                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                }

                openNextCategories(item);
            }
        });
        adapterHistory = new HomeCategorySearchHistoryAdapter((LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE), new HomeCategorySearchHistoryAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(HomeCategory item) {
                if (!lastSuggestions.contains(item)){
                    lastSuggestions.add(item);
                }

                preferencesAdapter.setSuggestionToPreferences(preferences, lastSuggestions);
                searchBar.setText(item.getTitle());
//                searchBar.hideSuggestionsList();
//                relativeLayoutFragmentSearchMain.setVisibility(View.VISIBLE);
//                View view = getActivity().getCurrentFocus();
//                if (view != null) {
//                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                }

                openNextCategories(item);
            }
        });

        Intent intent = new Intent(getActivity(), Service.class);
        bindStatus = getActivity().bindService(intent, this, Context.BIND_AUTO_CREATE);

        return view;
    }

    public void openNextCategories(HomeCategory category) {
        DetailCompany detailCompany = null;
        if (category.getIs_last() == 1){
            Intent intent = new Intent(getActivity(), HomeSortActivity.class);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBSUBCATEGORY, category);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, "");
            intent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompany);
            startActivity(intent);
        } else {
            Intent destIntent;
            switch (category.getView_type()){
                case 3:
                    destIntent = new Intent(getActivity(), HomeSubcategoryActivity.class);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_CATEGORY, category);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, "");
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompany);
                    break;
                case 4:
                    destIntent = new Intent(getActivity(), HomeSubSubcategoryActivity.class);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBCATEGORY, category);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, "");
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompany);
                    break;
                default:
                    return;
            }
            startActivity(destIntent);
        }
    }

    @Override
    public void addVegetablesSearchData(List<HomeCategory> vegetablesSearchData) {
        if (vegetablesSearchData != null) this.searchDataVegetables = vegetablesSearchData;
    }

    @Override
    public void addFlowersSearchData(List<HomeCategory> flowersSearchData) {
        if (flowersSearchData != null) this.searchDataFlowers = flowersSearchData;
    }

    @Override
    public void showSnackbarMessage(String msg) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new HomeSearchPresenter(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(context)));
        bindStatus = true;
        presenter.downloadVegetablesSearchData();
        presenter.downloadFlowersSearchData();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new SearchVegetablesFragment();
                case 1:
                    return new SearchFlowersFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}