package ru.apfo.plantseeds.fragments.main;

import android.arch.persistence.room.Room;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.MyBundleKeys;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.HomeSortActivity;
import ru.apfo.plantseeds.activity.HomeSubSubcategoryActivity;
import ru.apfo.plantseeds.activity.HomeSubcategoryActivity;
import ru.apfo.plantseeds.adapter.ListHomeCategoryAdapter;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.data.db.MyDatabase;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.presenter.HomeCategoryPresenter;
import ru.apfo.plantseeds.view.HomeCategoryView;

public class HomeCategoryFragment extends Fragment implements ServiceConnection, HomeCategoryView{
    private HomeCategoryPresenter presenter;
    private ActivityCallback activityCallback;

    private ListView listView;
    private List<HomeCategory> categories;
    private ListHomeCategoryAdapter adapter;

    private ProgressBar progressBarLoading;
    private SwipeRefreshLayout swipeRefreshLayout;

    private boolean bindStatus;

    private Context context;

    private DetailCompany detailCompany;

    public HomeCategoryFragment() {

    }

    public void setActiviy(ActivityCallback activityCallback){
        this.activityCallback = activityCallback;
    }

    public static HomeCategoryFragment newInstance() {
        return new HomeCategoryFragment();
    }

    public static HomeCategoryFragment newInstance(DetailCompany detailCompany) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(MyBundleKeys.APP_BUNDLE_HOME_CATEGORY_DETAIL_COMPANY_FROM_NAV, detailCompany);
        HomeCategoryFragment fragment = new HomeCategoryFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_home, container, false);

        if (getArguments() != null){
            detailCompany = (DetailCompany) getArguments().getSerializable(MyBundleKeys.APP_BUNDLE_HOME_CATEGORY_DETAIL_COMPANY_FROM_NAV);
        }

        context = getActivity().getApplicationContext();

        progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarHomeCategoryLoading);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayoutHomeCategories);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        listView = (ListView) view.findViewById(R.id.listViewHomeCategories);
        categories = new ArrayList<>();
        adapter = new ListHomeCategoryAdapter(context, categories);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openNextCategories(adapter.getItem(i));
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.clean();
                try {
                    presenter.updateCategories(getActivity().getPackageManager().getApplicationInfo("ru.apfo.plantseeds", 0).uid + "");
                    Log.d(getClass().getSimpleName(), getActivity().getPackageManager().getApplicationInfo("ru.apfo.plantseeds", 0).uid + " - UID");
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        Intent intent = new Intent(getActivity(), Service.class);
        bindStatus = getActivity().bindService(intent, this, Context.BIND_AUTO_CREATE);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new HomeCategoryPresenter(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(
                new DataRepository(((Service.LocalBinder)iBinder).getService(),
                        Room.databaseBuilder(context, MyDatabase.class, HomeCategory.TABLE_NAME)
                                .fallbackToDestructiveMigration()
                                .build(),
                        PreferenceManager.getDefaultSharedPreferences(getContext())
                ));
        bindStatus = true;
        try {
            presenter.getAllCategories(getActivity().getPackageManager().getApplicationInfo("ru.apfo.plantseeds", 0).uid + "");
            Log.d(getClass().getSimpleName(), getActivity().getPackageManager().getApplicationInfo("ru.apfo.plantseeds", 0).uid + " - UID");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

            Log.d(getClass().getSimpleName(), "ERROR WITH GET UID!!!!!");
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void startLoading() {
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBarLoading.setVisibility(View.GONE);
        if (swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void addCategories(List<HomeCategory> categories) {
        adapter.setItems(categories);
        if (activityCallback != null) {
            activityCallback.setCategories(categories);
        }
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(listView, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void openNextCategories(HomeCategory category) {
        Log.d(getClass().getSimpleName(), "SUPERMAN Open categories from HomeCategoryFragment!");
        if (detailCompany == null) Log.d(getClass().getSimpleName(), "SUPERMAN DetailCompany title: sorry, u are not from companies!");
        else Log.d(getClass().getSimpleName(), "SUPERMAN DetailCompany title: " + detailCompany.getTitle());
        if (category.getIs_last() == 1){
            Intent intent = new Intent(context, HomeSortActivity.class);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBSUBCATEGORY, category);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, "");
            intent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompany);
            startActivity(intent);
            Log.d(getClass().getSimpleName(), "OPEN PRODUCTS!!!");
        } else {
            Intent destIntent;
            switch (category.getView_type()){
                case 3:
                    destIntent = new Intent(context, HomeSubcategoryActivity.class);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_CATEGORY, category);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, "");
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompany);
                    break;
                case 4:
                    destIntent = new Intent(context, HomeSubSubcategoryActivity.class);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBCATEGORY, category);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, "");
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompany);;
                    break;
                default:
                    return;
            }
            startActivity(destIntent);
        }
    }

    public interface ActivityCallback{
        void setCategories(List<HomeCategory> categories);
    }
}