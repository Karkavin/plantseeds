package ru.apfo.plantseeds.fragments.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ru.apfo.plantseeds.MyBundleKeys;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.CompanyActivity;
import ru.apfo.plantseeds.adapter.ListHomeProductCompaniesAdapter;
import ru.apfo.plantseeds.adapter.ListHomeProductCompaniesSortAdapter;
import ru.apfo.plantseeds.data.api.model.Company;
import ru.apfo.plantseeds.data.api.model.ProductListItem;
import ru.apfo.plantseeds.fragments.dialog.HomeProductBottomSheetDialogFragment;

public class HomeProductCompaniesFragment extends Fragment implements HomeProductBottomSheetDialogFragment.HomeProductBottomSheetDialogFragmentListener {
    private ListHomeProductCompaniesAdapter adapter;
    private ListView listViewHomeProductCompanies;

    private ProductListItem productListItem;

    private HomeProductBottomSheetDialogFragment bottomSheetDialogFragment;

    private TextView textViewSort;
    private ImageView imageViewSort;

    public HomeProductCompaniesFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_product_companies, container, false);

        Bundle bundle = getArguments();
        productListItem = (ProductListItem) bundle.getSerializable(MyBundleKeys.APP_BUNDLE_HOME_PRODUCT_COMPANIES_FRAGMENT);

        adapter = new ListHomeProductCompaniesAdapter(getActivity(), productListItem.getCompanies());

        listViewHomeProductCompanies = (ListView) rootView.findViewById(R.id.listViewHomeProductCompanies);
        listViewHomeProductCompanies.setAdapter(adapter);

        LayoutInflater mInflater = LayoutInflater.from(getActivity());
        View header =  inflater.inflate(R.layout.card_home_product_top_sort, null);
        textViewSort = (TextView) header.findViewById(R.id.textViewHomeProductSortByName);
        imageViewSort = (ImageView) header.findViewById(R.id.imageViewHomeProductSort);
        bottomSheetDialogFragment = new HomeProductBottomSheetDialogFragment();
        bottomSheetDialogFragment.setListener(this);
        View.OnClickListener onClickListenerSort = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogFragment.show(getFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        };
        textViewSort.setOnClickListener(onClickListenerSort);
        imageViewSort.setOnClickListener(onClickListenerSort);

        listViewHomeProductCompanies.addHeaderView(header);
        listViewHomeProductCompanies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0) {
                    Intent intent = new Intent(getContext(), CompanyActivity.class);
                    intent.putExtra(MyIntentKeys.APP_INTENT_HOME_COMPANY_BASE_INFO, productListItem.getCompanies().get(i - 1));
                    intent.putExtra(MyIntentKeys.APP_INTENT_HOME_COMPANY_PRODUCT, productListItem.getProduct());
//                Log.d(getClass().getSimpleName(), "Company id: " + productListItem.getCompanies().get(0).getId() + ", name: " + productListItem.getCompanies().get(0).getTitle());
//                Log.d(getClass().getSimpleName(), "Product id:" + productListItem.getProduct().getId() + ", name: " + productListItem.getProduct().getTitle());
                    startActivity(intent);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onSortTypeSelected(ListHomeProductCompaniesSortAdapter.SortType sortType) {
        switch (sortType){
            case SORT_TYPE_FROM_A_TO_Z:
                textViewSort.setText("По названию");
                imageViewSort.setVisibility(View.VISIBLE);
                imageViewSort.setImageResource(R.drawable.icon_arrow_forward_down);
                adapter.sortFromAToZ();
                break;
            case SORT_TYPE_FROM_Z_TO_A:
                textViewSort.setText("По названию");
                imageViewSort.setVisibility(View.VISIBLE);
                imageViewSort.setImageResource(R.drawable.icon_arrow_forward_up);
                adapter.sortFromZToA();
                break;
            case RATING:
                textViewSort.setText("По рейтингу");
                imageViewSort.setVisibility(View.GONE);
                adapter.sortByRating();
                break;
            default:
                textViewSort.setText("По названию");
                imageViewSort.setVisibility(View.VISIBLE);
                imageViewSort.setImageResource(R.drawable.icon_arrow_forward_down);
                adapter.sortFromAToZ();
                break;
        }
    }
}