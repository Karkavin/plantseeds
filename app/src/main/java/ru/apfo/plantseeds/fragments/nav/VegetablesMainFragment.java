package ru.apfo.plantseeds.fragments.nav;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import ru.apfo.plantseeds.MyBundleKeys;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.HomeSortActivity;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.fragments.main.HomeSubSubcategoryFragment;
import ru.apfo.plantseeds.fragments.main.HomeSubcategoryFragment;

public class VegetablesMainFragment extends Fragment {
    private ProgressBar progressBarLoading;

    public VegetablesMainFragment() {

    }

    public static VegetablesMainFragment newInstance(HomeCategory homeCategory) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(MyBundleKeys.APP_BUNDLE_VEGETABLES_HOME_CATEGORY, homeCategory);

        VegetablesMainFragment fragment = new VegetablesMainFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vegetables_main, container, false);

        progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarFragmentVegetables);
        progressBarLoading.setVisibility(View.VISIBLE);

        HomeCategory homeCategory = (HomeCategory) getArguments().getSerializable(MyBundleKeys.APP_BUNDLE_VEGETABLES_HOME_CATEGORY);
        openNextCategories(homeCategory);

        return view;
    }

    private void openNextCategories(HomeCategory category) {
        progressBarLoading.setVisibility(View.GONE);

        if (category.getIs_last() == 1){
            Intent intent = new Intent(getActivity(), HomeSortActivity.class);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBSUBCATEGORY, category);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, category.getTitle());
            startActivity(intent);
        } else {
            switch (category.getView_type()){
                case 3:
                    HomeSubcategoryFragment homeSubcategoryFragment = HomeSubcategoryFragment.newInstance(category, false);
                    getFragmentManager().beginTransaction().replace(R.id.frameLayoutVegetables, homeSubcategoryFragment).commit();
                    break;
                case 4:
                    HomeSubSubcategoryFragment homeSubSubcategoryFragment = HomeSubSubcategoryFragment.newInstance(category, false);
                    getFragmentManager().beginTransaction().replace(R.id.frameLayoutVegetables, homeSubSubcategoryFragment).commit();
                    break;
                default:
                    break;
            }
        }
    }
}