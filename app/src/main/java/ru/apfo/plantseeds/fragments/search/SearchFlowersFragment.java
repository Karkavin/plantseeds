package ru.apfo.plantseeds.fragments.search;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.GridViewWithHeaderAndFooter;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.FilterActivity;
import ru.apfo.plantseeds.activity.HomeProductActivity;
import ru.apfo.plantseeds.adapter.ListHomeSortAdapter;
import ru.apfo.plantseeds.adapter.filter.FiltersResultAdapter;
import ru.apfo.plantseeds.data.api.model.ProductListItem;
import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.presenter.FilterProductsPresenter;
import ru.apfo.plantseeds.view.HomeProductView;

public class SearchFlowersFragment extends Fragment implements ServiceConnection, HomeProductView, FiltersResultAdapter.OnFilterResultClicked{
    public List<BaseFilter> filters;
    public static final int ACTIVITY_RESULT_CODE = 108;

    private Context context;

    private FilterProductsPresenter presenter;
    private boolean bindStatus;

    private View v;
    private View header;

    private boolean isSorted = false;
    private boolean isSortedFromAToZ = true;
    private TextView textViewSort;
    private ImageView imageViewSort;

    private GridViewWithHeaderAndFooter gridView;
    private ListHomeSortAdapter adapter;
    private List<ProductListItem> products;

    private ProgressBar progressBarLoading;

    public static final int DEFAULT_FROM = 1;
    public static final int DEFAULT_TO = 19;
    public static final int DEFAULT_PAGINATION_INCREMENT = 19;
    private int from = DEFAULT_FROM;
    private int to = DEFAULT_TO;
    private int paginationIncrement = DEFAULT_PAGINATION_INCREMENT;
    private boolean flagIsLoadingMore = false;

    public SearchFlowersFragment() {

    }

    public static SearchFlowersFragment newInstance() {
        return new SearchFlowersFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_search_flowers, container, false);

        context = getActivity().getApplicationContext();

        gridView = (GridViewWithHeaderAndFooter) v.findViewById(R.id.gridViewFilterSearchFlowers);
        gridView.setNestedScrollingEnabled(true);
        View footer = inflater.inflate(R.layout.card_home_bottom_sort, null);
        progressBarLoading = (ProgressBar) footer.findViewById(R.id.progressBarHomeSortLoading);
        progressBarLoading.setVisibility(View.GONE);
        gridView.addFooterView(footer);
        header =  inflater.inflate(R.layout.card_home_top_sort_filter, null);
        textViewSort = (TextView) header.findViewById(R.id.textViewHomeSortByName);
        textViewSort.setText("Сортировать по ...");
        textViewSort.setTextColor(Color.parseColor("#808080"));
        ((TextView) header.findViewById(R.id.textViewHomeTopSortMeta)).setVisibility(View.GONE);
        imageViewSort = (ImageView) header.findViewById(R.id.imageViewHomeSortByName);
        View.OnClickListener onClickListenerSort = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sort();
            }
        };

        header.findViewById(R.id.relativeLayoutFilterAction).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FilterActivity.class);
                intent.putExtra(MyIntentKeys.APP_INTENT_FILTER_TITLE, "Фильтры для поиска цветов");
                intent.putExtra(MyIntentKeys.APP_INTENT_FILTER_CATEGORY_TYPE, 3);
                intent.putExtra(MyIntentKeys.APP_INTENT_FILTERS, (Serializable) filters);
                startActivityForResult(intent, ACTIVITY_RESULT_CODE);
            }
        });

        textViewSort.setOnClickListener(onClickListenerSort);
        imageViewSort.setOnClickListener(onClickListenerSort);
        gridView.addHeaderView(header);
        products = new ArrayList<>();
        adapter = new ListHomeSortAdapter(context, products, this);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openDetail(adapter.getItem(i));
            }
        });
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount != 0) {
                    if(!flagIsLoadingMore) {
                        flagIsLoadingMore = true;
                        loadMoreProducts();
                    }
                }
            }
        });

        Intent intent = new Intent(getActivity(), Service.class);
        bindStatus = getActivity().bindService(intent, this, Context.BIND_AUTO_CREATE);

        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new FilterProductsPresenter(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onResume() {
        super.onResume();

        TextView textViewFilters = (TextView) header.findViewById(R.id.textViewFilterViewAction);
        ImageView imageViewFilters = (ImageView) header.findViewById(R.id.imageViewFilterViewAction);
        imageViewSort.setVisibility(View.GONE);
        textViewSort.setText("Сортировать по ...");
        textViewSort.setTextColor(Color.parseColor("#808080"));
        isSorted = false;
        isSortedFromAToZ = true;
        if (this.filters != null) {
            new FiltersResultAdapter(filters, getLayoutInflater(), header.findViewById(R.id.flexboxLayoutFilter), this);
            textViewFilters.setTextColor(ContextCompat.getColor(getContext(), R.color.blue));
            imageViewFilters.setColorFilter(ContextCompat.getColor(getContext(), R.color.blue), PorterDuff.Mode.SRC_IN);
            if (bindStatus) {
                clearProducts();
                from = DEFAULT_FROM;
                to = DEFAULT_TO;

                presenter.getProducts(3, from, to, filters);
            }
        } else {
            textViewFilters.setTextColor(ContextCompat.getColor(getContext(), R.color.mediumGrey));
            imageViewFilters.setColorFilter(ContextCompat.getColor(getContext(), R.color.mediumGrey), PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (data == null) return;
//        else {
//            ProductListItem returnedItem = (ProductListItem) data.getSerializableExtra(MyIntentKeys.APP_INTENT_HOME_PRODUCT_RESULT);
//            if (returnedItem != null) {
//                adapter.updateListItem(returnedItem);
//            }
//        }
        if (requestCode == ACTIVITY_RESULT_CODE) {
            if(resultCode == Activity.RESULT_OK) {
                this.filters = (ArrayList<BaseFilter>) data.getSerializableExtra(MyIntentKeys.APP_INTENT_FILTERS);
            }
        }
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(context)));
        bindStatus = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void startLoading() {
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBarLoading.setVisibility(View.GONE);
    }

    @Override
    public void addProducts(List<ProductListItem> products) {
        flagIsLoadingMore = false;
        adapter.setItems(products);
        if (adapter.getCount() > 0){
            gridView.setVisibility(View.VISIBLE);
            ((TextView) v.findViewById(R.id.textViewFilterNoDataFlowers)).setVisibility(View.GONE);
            textViewSort.setText("По названию");
            textViewSort.setTextColor(Color.parseColor("#347CFF"));
        } else{
            ((TextView) v.findViewById(R.id.textViewFilterNoDataFlowers)).setVisibility(View.VISIBLE);
            textViewSort.setText("Сортировать по ...");
            textViewSort.setTextColor(Color.parseColor("#808080"));
            imageViewSort.setVisibility(View.GONE);
            isSorted = false;
            isSortedFromAToZ = true;
        }

        if (isSorted){
            if (isSortedFromAToZ){
                adapter.sortFromAToZ();
            } else {
                adapter.sortFromZToA();
            }
        }
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(gridView, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void openDetail(ProductListItem productListItem) {
        Intent intent = new Intent(getActivity(), HomeProductActivity.class);
        intent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SORT, productListItem);
        intent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, "Поиск по фильтрам");
        startActivityForResult(intent, ACTIVITY_RESULT_CODE);
    }

    @Override
    public void addToFavourite(int id) {
        presenter.addProductToFavourite(id);
    }

    @Override
    public void deleteFromFavourite(int id) {
        presenter.deleteProductFromFavourite(id);
    }

    private void sort() {
        if (adapter.getCount() > 0) {
            isSorted = true;
            imageViewSort.setVisibility(View.VISIBLE);
            textViewSort.setText("По названию");
            textViewSort.setTextColor(Color.parseColor("#347CFF"));
            if (isSortedFromAToZ) {
                isSortedFromAToZ = false;
                adapter.sortFromZToA();
                imageViewSort.setImageResource(R.drawable.icon_arrow_forward_up);
            } else {
                isSortedFromAToZ = true;
                adapter.sortFromAToZ();
                imageViewSort.setImageResource(R.drawable.icon_arrow_forward_down);
            }
        }
    }

    private void clearProducts(){
        adapter.clean();
    }

    private void loadMoreProducts(){
        incrementFromAndToValues();
        Log.d(getClass().getSimpleName(), "Load more... from = " + from + "; to = " + to);
        presenter.getProducts(3, from, to, filters);
    }

    private void incrementFromAndToValues(){
        from += paginationIncrement;
        to += paginationIncrement;
    }

    @Override
    public void onFilterResultClicked(List<BaseFilter> newFilters) {
        this.filters = newFilters;
        onResume();
    }
}