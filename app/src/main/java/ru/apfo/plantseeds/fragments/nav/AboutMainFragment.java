package ru.apfo.plantseeds.fragments.nav;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.About;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.presenter.AboutPresenter;
import ru.apfo.plantseeds.view.AboutView;

public class AboutMainFragment extends Fragment implements ServiceConnection, AboutView {
    private AboutPresenter presenter;

    private Context context;

    private boolean bindStatus;

    private ProgressBar progressBarLoading;
    private ScrollView scrollViewAbout;
    private TextView textViewTitle;
    private TextView textViewDescr;
    private TextView textViewEmail;

    public AboutMainFragment() {

    }

    public static AboutMainFragment newInstance() {
        return new AboutMainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_main, container, false);

        context = getActivity().getApplicationContext();

        progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarFragmentAbout);
        progressBarLoading.setVisibility(View.VISIBLE);

        scrollViewAbout = (ScrollView) view.findViewById(R.id.scrollViewAbout);
        textViewTitle = (TextView) view.findViewById(R.id.textViewAboutTitle);
        textViewDescr = (TextView) view.findViewById(R.id.textViewAboutDescr);
        textViewEmail = (TextView) view.findViewById(R.id.textViewAboutEmail);
        scrollViewAbout.setVisibility(View.GONE);

        Intent intent = new Intent(getActivity(), Service.class);
        bindStatus = getActivity().bindService(intent, this, Context.BIND_AUTO_CREATE);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new AboutPresenter(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(context)));
        bindStatus = true;
        presenter.getAbout();
    }

    @Override
    public void startLoading() {
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBarLoading.setVisibility(View.GONE);
    }

    @Override
    public void aboutLoaded(About about) {
        scrollViewAbout.setVisibility(View.VISIBLE);
        textViewTitle.setText(about.getTitle());
        textViewDescr.setText(about.getDescr());
        textViewEmail.setText(about.getEmail());
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(progressBarLoading, msg, Snackbar.LENGTH_LONG).show();
    }
}