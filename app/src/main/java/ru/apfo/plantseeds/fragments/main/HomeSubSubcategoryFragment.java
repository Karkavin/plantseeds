package ru.apfo.plantseeds.fragments.main;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.MyBundleKeys;
import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.HomeSortActivity;
import ru.apfo.plantseeds.activity.HomeSubSubcategoryActivity;
import ru.apfo.plantseeds.activity.HomeSubcategoryActivity;
import ru.apfo.plantseeds.activity.MainActivity;
import ru.apfo.plantseeds.adapter.ListHomeSubSubcategoryAdapter;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.data.repo.DataRepository;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.presenter.HomeCategoryPresenter;
import ru.apfo.plantseeds.view.HomeCategoryView;

public class HomeSubSubcategoryFragment extends Fragment implements ServiceConnection, HomeCategoryView {
    private Context context;

    private HomeCategoryPresenter presenter;
    private ActivityCallback activityCallback;

    private GridView gridView;
    private ListHomeSubSubcategoryAdapter adapter;
    private List<HomeCategory> subSubcategories;
    private ProgressBar progressBarLoading;

    private boolean bindStatus;

    private HomeCategory homeSubcategory;

    private DetailCompany detailCompanyFromNav;

    public HomeSubSubcategoryFragment() {

    }

    public static HomeSubSubcategoryFragment newInstance(HomeCategory homeSubcategory, boolean bottomBarVisibility) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUB_SUB_CATEGORY, homeSubcategory);
        arguments.putBoolean(MyBundleKeys.APP_BUNDLE_HOME_SUB_SUB_CATEGORY_BOTTOM_BAR_VISIBILITY, bottomBarVisibility);
        HomeSubSubcategoryFragment fragment = new HomeSubSubcategoryFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    public static HomeSubSubcategoryFragment newInstance(HomeCategory homeSubcategory, boolean bottomBarVisibility, DetailCompany detailCompany) {
        Bundle arguments = new Bundle();
        arguments.putSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUB_SUB_CATEGORY, homeSubcategory);
        arguments.putBoolean(MyBundleKeys.APP_BUNDLE_HOME_SUB_SUB_CATEGORY_BOTTOM_BAR_VISIBILITY, bottomBarVisibility);
        arguments.putSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUB_SUB_CATEGORY_DETAIL_COMPANY_FROM_NAV, detailCompany);
        HomeSubSubcategoryFragment fragment = new HomeSubSubcategoryFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    public void setActivity(ActivityCallback activityCallback){
        this.activityCallback = activityCallback;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_sub_subcategory, container, false);

        context = getActivity().getApplicationContext();

        homeSubcategory = (HomeCategory) getArguments().getSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUB_SUB_CATEGORY);
        boolean bottomBarVisibility = getArguments().getBoolean(MyBundleKeys.APP_BUNDLE_HOME_SUB_SUB_CATEGORY_BOTTOM_BAR_VISIBILITY);
        detailCompanyFromNav = (DetailCompany) getArguments().getSerializable(MyBundleKeys.APP_BUNDLE_HOME_SUB_SUB_CATEGORY_DETAIL_COMPANY_FROM_NAV);
        if (activityCallback != null) {
            activityCallback.setActionBarTitle(homeSubcategory.getTitle());
        }

        BottomNavigationBar bottomNavigationBar = (BottomNavigationBar) view.findViewById(R.id.bottomNavigationSubSubcategory);
        if (bottomBarVisibility) bottomNavigationBar.setVisibility(View.VISIBLE);
        BottomBarItem bottomBarItemHome = new BottomBarItem(R.drawable.icon_flowers, R.string.bottom_bar_home);
        BottomBarItem bottomBarItemSearch = new BottomBarItem(R.drawable.icon_search, R.string.bottom_bar_search);
        BottomBarItem bottomBarItemFavourites = new BottomBarItem(R.drawable.icon_favourite_bottom, R.string.bottom_bar_favourites);
        bottomNavigationBar.addTab(bottomBarItemHome)
                .addTab(bottomBarItemSearch)
                .addTab(bottomBarItemFavourites);
        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                bottomBarChoose(position);
            }
        });
        bottomNavigationBar.setOnReselectListener(new BottomNavigationBar.OnReselectListener() {
            @Override
            public void onReselect(int position) {
                bottomBarChoose(position);
            }
        });

        progressBarLoading = (ProgressBar) view.findViewById(R.id.progressBarHomeSubSubcategoryLoading);

        gridView = (GridView) view.findViewById(R.id.gridViewHomeSubSubcategory);
        subSubcategories = new ArrayList<>();
        adapter = new ListHomeSubSubcategoryAdapter(getActivity(), subSubcategories);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                openNextCategories(adapter.getItem(i));
            }
        });

        Intent intent = new Intent(getActivity(), Service.class);
        bindStatus = getActivity().bindService(intent, this, Context.BIND_AUTO_CREATE);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new HomeCategoryPresenter(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
        presenter.detachDataLayer();
    }

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        presenter.attachDataLayer(new DataRepository(((Service.LocalBinder)iBinder).getService(),
                PreferenceManager.getDefaultSharedPreferences(context)));
        bindStatus = true;
        long company_id = 0;
        if (detailCompanyFromNav != null) company_id = detailCompanyFromNav.getId();
        presenter.getAllSubcategories(homeSubcategory.getId(), (int) company_id);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        bindStatus = false;
    }

    @Override
    public void startLoading() {
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progressBarLoading.setVisibility(View.GONE);
    }

    @Override
    public void addCategories(List<HomeCategory> categories) {
        adapter.setItems(categories);
    }

    @Override
    public void showSnackbarMessage(String msg) {
        Snackbar.make(gridView, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void openNextCategories(HomeCategory category) {
        Log.d(getClass().getSimpleName(), "SUPERMAN Open categories from HomeSubSubCategoryFragment!");
        if (detailCompanyFromNav == null) Log.d(getClass().getSimpleName(), "SUPERMAN DetailCompany title: sorry, u are not from companies!");
        else Log.d(getClass().getSimpleName(), "SUPERMAN DetailCompany title: " + detailCompanyFromNav.getTitle());
        if (category.getIs_last() == 1){
            Intent intent = new Intent(getActivity(), HomeSortActivity.class);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBSUBCATEGORY, category);
            intent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, homeSubcategory.getTitle());
            intent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompanyFromNav);
            startActivity(intent);
        } else {
            Intent destIntent;
            switch (category.getView_type()){
                case 3:
                    destIntent = new Intent(getActivity(), HomeSubcategoryActivity.class);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_CATEGORY, category);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, homeSubcategory.getTitle());
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompanyFromNav);
                    break;
                case 4:
                    destIntent = new Intent(getActivity(), HomeSubSubcategoryActivity.class);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_FROM_SUBCATEGORY, category);
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_HOME_CATEGORY, homeSubcategory.getTitle());
                    destIntent.putExtra(MyIntentKeys.APP_INTENT_DETAIL_COMPANY_CATEGORY, detailCompanyFromNav);
                    break;
                default:
                    return;
            }
            startActivity(destIntent);
        }
    }

    private void bottomBarChoose(int position){
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(MyIntentKeys.APP_INTENT_BOTTOM_BAR_POSITION, position);
        startActivity(intent);
    }

    public interface ActivityCallback{
        void setActionBarTitle(String title);
    }
}