package ru.apfo.plantseeds.fragments.dialog;

import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.adapter.ListHomeProductCompaniesSortAdapter;

public class HomeProductBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private ListHomeProductCompaniesSortAdapter.SortType sortType;
    private HomeProductBottomSheetDialogFragmentListener mListener;

    public HomeProductBottomSheetDialogFragment() {
        sortType = ListHomeProductCompaniesSortAdapter.SortType.SORT_TYPE_FROM_A_TO_Z;
    }

    public void setListener(HomeProductBottomSheetDialogFragmentListener listener){
        mListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.sort_bottom_sheet_dialog_fragment, container, false);
        ListView listView = (ListView) contentView.findViewById(R.id.listViewHomeProductSortBottom);
        ListHomeProductCompaniesSortAdapter adapter = new ListHomeProductCompaniesSortAdapter(getActivity());
        adapter.setSortType(sortType);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                adapter.setSortType(i);
                sortType = adapter.getSortType();
                mListener.onSortTypeSelected(sortType);
                dismiss();
            }
        });

        return contentView;
    }

    public interface HomeProductBottomSheetDialogFragmentListener{
        void onSortTypeSelected(ListHomeProductCompaniesSortAdapter.SortType sortType);
    }
}