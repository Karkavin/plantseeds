package ru.apfo.plantseeds.presenter;

import android.util.Log;

import ru.apfo.plantseeds.data.callback.HomeProductDetailCallback;
import ru.apfo.plantseeds.data.repo.Repository;
import ru.apfo.plantseeds.view.HomeProductDetailView;

public class HomeProductDetailPresenter implements HomeProductDetailCallback {
    private HomeProductDetailView view;
    private Repository repository;

    public HomeProductDetailPresenter(HomeProductDetailView view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    public void attachDataLayer(Repository repository) {
        this.repository = repository;
    }

    public void detachDataLayer(){
        repository = null;
    }

    public void addProductToFavourite(int id){
        Log.d(getClass().getSimpleName(), "addProductToFavourite (outside) add..");
        Log.d(getClass().getSimpleName(), "repository null? " + (repository == null));
        Log.d(getClass().getSimpleName(), "view null? " + (view == null));
        if (repository != null && view != null){
            repository.addToFavourite(this, id);
            Log.d(getClass().getSimpleName(), "addProductToFavourite (inside) add..");
        }
    }

    public void deleteProductFromFavourite(int id){
        Log.d(getClass().getSimpleName(), "deleteProductFromFavourite (outside) add..");
        Log.d(getClass().getSimpleName(), "repository null? " + (repository == null));
        Log.d(getClass().getSimpleName(), "view null? " + (view == null));
        if (repository != null && view != null){
            repository.deleteFromFavourite(this, id);
            Log.d(getClass().getSimpleName(), "deleteProductFromFavourite (inside) add..");
        }
    }

    @Override
    public void onError(String msg) {
        if (view != null) {
            view.showSnackbarMessage(msg);
        }
    }

    @Override
    public void onError() {

    }
}