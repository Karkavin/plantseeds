package ru.apfo.plantseeds.presenter;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.CompanyReview;
import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.callback.CompanyReviewCallback;
import ru.apfo.plantseeds.data.callback.HomeCompanyCallback;
import ru.apfo.plantseeds.data.repo.Repository;
import ru.apfo.plantseeds.view.HomeCompanyView;

public class HomeCompanyPresenter implements HomeCompanyCallback, CompanyReviewCallback {
    private HomeCompanyView view;
    private Repository repository;

    public HomeCompanyPresenter(HomeCompanyView view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    public void attachDataLayer(Repository repository) {
        this.repository = repository;
    }

    public void detachDataLayer(){
        repository = null;
    }

    public void getContent(int company_id){
        if (repository != null && view != null){
            view.startLoadingContent();
            repository.getDetailCompanyContent(this, company_id);
        }
    }

    public void getReviews(int company_id, int product_id){
        if (repository != null && view != null){
            view.startLoadingReviews();
            repository.getDetailCompanyReviews(this, company_id, product_id);
        }
    }

    public void addShortReview(int company_id, int product_id, int rating){
        if (repository != null && view != null){
            view.startLoadingContent();
            view.startLoadingReviews();
            repository.addShortCompanyReview(this, company_id, product_id, rating);
        }
    }

    public void addFullReview(long company_id, long product_id, String author, String title, String descr, int rating,
                              String imageTitle, List<String> imagesBase64){
        if (repository != null && view != null){
            view.startLoadingReviews();
            repository.addFullCompanyReview(this, company_id, product_id, author, title, descr,
                    rating, imageTitle, imagesBase64);
        }
    }

    @Override
    public void onError(String msg) {
        if (view != null) {
            view.stopLoadingContent();
            view.stopLoadingReviews();
            view.showSnackbarMessage(msg);
        }
    }

    @Override
    public void onError() {
        if (view != null) {
            view.stopLoadingContent();
            view.stopLoadingReviews();
        }
    }

    @Override
    public void onSuccessGetContent(List<DetailCompany> detailCompanies) {
        if (view != null) {
            view.stopLoadingContent();
            view.addContent(detailCompanies.get(0));
        }
    }

    @Override
    public void onSuccessGetReviews(List<CompanyReview> companyReviews) {
        if (view != null) {
            view.stopLoadingReviews();
            view.addReviews(companyReviews);
        }
    }

    @Override
    public void onSuccessAddedShortReview(int company_id, int product_id) {
        if (view != null && repository != null) {
            view.showSnackbarMessage("Отзыв добавлен");
            view.removeAllReview();
            repository.getDetailCompanyContent(this, company_id);
            repository.getDetailCompanyReviews(this, company_id, product_id);
        }
    }

    @Override
    public void onSuccessAddedFullReview() {
        if (view != null) {
            view.stopLoadingReviews();
            view.fullReviewAdded();
        }
    }
}