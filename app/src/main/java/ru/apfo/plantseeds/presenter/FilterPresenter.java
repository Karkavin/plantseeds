package ru.apfo.plantseeds.presenter;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;
import ru.apfo.plantseeds.data.callback.FilterCallback;
import ru.apfo.plantseeds.data.repo.Repository;
import ru.apfo.plantseeds.view.FilterView;

public class FilterPresenter implements FilterCallback{
    private FilterView view;
    private Repository repository;

    public FilterPresenter(FilterView view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    public void attachDataLayer(Repository repository) {
        this.repository = repository;
    }

    public void detachDataLayer(){
        repository = null;
    }

    public void getFilters(){
        if (repository != null && view != null){
            view.startLoading();
            repository.getFilters(this);
        }
    }

    @Override
    public void onError(String msg) {
        if (view != null){
            view.stopLoading();
            view.showSnackbarMessage(msg);
        }
    }

    @Override
    public void onError() {
        if (view != null){
            view.stopLoading();
        }
    }

    @Override
    public void onSuccessfullGetFilters(List<BaseFilter> filters) {
        if (view != null) {
            view.stopLoading();
            view.addFilters(filters);
        }
    }
}