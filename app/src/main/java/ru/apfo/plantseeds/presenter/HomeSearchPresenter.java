package ru.apfo.plantseeds.presenter;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.data.callback.HomeSearchCallback;
import ru.apfo.plantseeds.data.repo.Repository;
import ru.apfo.plantseeds.view.HomeSearchView;

public class HomeSearchPresenter implements HomeSearchCallback{
    private HomeSearchView view;
    private Repository repository;

    public HomeSearchPresenter(HomeSearchView view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    public void attachDataLayer(Repository repository) {
        this.repository = repository;
    }

    public void detachDataLayer(){
        repository = null;
    }

    public void downloadVegetablesSearchData(){
        if (repository != null && view != null){
            repository.downloadVegetablesSearchData(this);
        }
    }

    public void downloadFlowersSearchData(){
        if (repository != null && view != null){
            repository.downloadFlowersSearchData(this);
        }
    }

    @Override
    public void onError(String msg) {
        if (view != null){
            view.showSnackbarMessage(msg);
        }
    }

    @Override
    public void onError() {

    }

    @Override
    public void onSuccessGetVegetablesSearchData(List<HomeCategory> vegetablesSearchData) {
        if (view != null){
            view.addVegetablesSearchData(vegetablesSearchData);
        }
    }

    @Override
    public void onSuccessGetFlowersSearchData(List<HomeCategory> flowersSearchData) {
        if (view != null){
            view.addFlowersSearchData(flowersSearchData);
        }
    }
}