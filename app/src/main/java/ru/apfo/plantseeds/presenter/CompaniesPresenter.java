package ru.apfo.plantseeds.presenter;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.DetailCompany;
import ru.apfo.plantseeds.data.callback.CompaniesCallback;
import ru.apfo.plantseeds.data.repo.Repository;
import ru.apfo.plantseeds.view.CompaniesView;

public class CompaniesPresenter implements CompaniesCallback {
    private CompaniesView view;
    private Repository repository;

    public CompaniesPresenter(CompaniesView view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    public void attachDataLayer(Repository repository) {
        this.repository = repository;
    }

    public void detachDataLayer(){
        repository = null;
    }

    public void getCompanies(){
        if (repository != null && view != null){
            view.startLoading();
            repository.getCompanies(this);
        }
    }

    @Override
    public void onError(String msg) {
        if (view != null) {
            view.stopLoading();
            view.showSnackbarMessage(msg);
        }
    }

    @Override
    public void onError() {
        if (view != null) {
            view.stopLoading();
        }
    }

    @Override
    public void onSuccessGetCompanies(List<DetailCompany> companies) {
        if (view != null) {
            view.stopLoading();
            view.companiesLoaded(companies);
        }
    }
}