package ru.apfo.plantseeds.presenter;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.ProductListItem;
import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;
import ru.apfo.plantseeds.data.callback.FilterProductsCallback;
import ru.apfo.plantseeds.data.repo.Repository;
import ru.apfo.plantseeds.view.HomeProductView;

public class FilterProductsPresenter implements FilterProductsCallback {
    private HomeProductView view;
    private Repository repository;

    public FilterProductsPresenter(HomeProductView view) {
        this.view = view;
    }

    public void detachView() {
        view = null;
    }

    public void attachDataLayer(Repository repository) {
        this.repository = repository;
    }

    public void detachDataLayer() {
        repository = null;
    }

    public void getProducts(int pid, int from, int to, List<BaseFilter> filters) {
        if (repository != null && view != null) {
            view.startLoading();
            repository.getProductsByFilter(this, pid, from, to, filters);
        }
    }

    public void addProductToFavourite(int id){
        if (repository != null && view != null){
            repository.addToFavourite(this, id);
        }
    }

    public void deleteProductFromFavourite(int id){
        if (repository != null && view != null){
            repository.deleteFromFavourite(this, id);
        }
    }

    @Override
    public void onError(String msg) {
        if (view != null){
            view.stopLoading();
            view.showSnackbarMessage(msg);
        }
    }

    @Override
    public void onError() {
        if (view != null){
            view.stopLoading();
        }
    }

    @Override
    public void onSuccessfullGetProductsByFilters(List<ProductListItem> products) {
        if (view != null){
            view.stopLoading();
            view.addProducts(products);
        }
    }
}