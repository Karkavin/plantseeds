package ru.apfo.plantseeds.presenter;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.ProductListItem;
import ru.apfo.plantseeds.data.callback.HomeProductsCallback;
import ru.apfo.plantseeds.data.repo.Repository;
import ru.apfo.plantseeds.view.HomeProductView;

public class HomeProductsPresenter implements HomeProductsCallback{
    private HomeProductView view;
    private Repository repository;

    public HomeProductsPresenter(HomeProductView view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    public void attachDataLayer(Repository repository) {
        this.repository = repository;
    }

    public void detachDataLayer(){
        repository = null;
    }

    public void getProducts(int id, int from, int to){
        if (repository != null && view != null){
            view.startLoading();
            repository.downloadProducts(this, id, from, to);
        }
    }

    public void addProductToFavourite(int id){
        if (repository != null && view != null){
            repository.addToFavourite(this, id);
        }
    }

    public void deleteProductFromFavourite(int id){
        if (repository != null && view != null){
            repository.deleteFromFavourite(this, id);
        }
    }

    public void getFavouriteProducts(){
        if (repository != null && view != null){
            view.startLoading();
            repository.getFavouriteProducts(this);
        }
    }

    @Override
    public void onSuccessGetProducts(List<ProductListItem> products) {
        if (view != null) {
            view.addProducts(products);
            view.stopLoading();
        }
    }

    @Override
    public void onError(String msg) {
        if (view != null) {
            view.showSnackbarMessage(msg);
            view.stopLoading();
        }
    }

    @Override
    public void onError() {
        if (view != null) {
            view.stopLoading();
        }
    }
}
