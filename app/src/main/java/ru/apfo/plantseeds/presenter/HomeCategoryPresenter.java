package ru.apfo.plantseeds.presenter;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.HomeCategory;
import ru.apfo.plantseeds.data.callback.HomeCategoriesCallback;
import ru.apfo.plantseeds.data.repo.Repository;
import ru.apfo.plantseeds.view.HomeCategoryView;

public class HomeCategoryPresenter implements HomeCategoriesCallback{
    private HomeCategoryView view;
    private Repository repository;

    public HomeCategoryPresenter(HomeCategoryView view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    public void attachDataLayer(Repository repository) {
        this.repository = repository;
    }

    public void detachDataLayer(){
        repository = null;
    }

    public void getAllCategories(String uid){
        if (repository != null && view != null){
            view.startLoading();
            repository.downloadAllCategories(this, uid);
        }
    }

    public void getAllSubcategories(int id, int company_id){
        if (repository != null && view != null){
            view.startLoading();
            repository.downloadAllSubcategories(this, id, company_id);
        }
    }

    public void updateCategories(String uid){
        if (repository != null && view != null){
            repository.downloadAllCategories(this, uid);
        }
    }

    @Override
    public void onSuccessGetCategories(List<HomeCategory> categories) {
        if (view != null) {
            view.stopLoading();
            view.addCategories(categories);
        }
    }

    @Override
    public void onError(String msg) {
        if (view != null) {
            view.stopLoading();
            view.showSnackbarMessage(msg);
        }
    }
}