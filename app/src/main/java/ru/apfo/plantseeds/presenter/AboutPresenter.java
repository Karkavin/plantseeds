package ru.apfo.plantseeds.presenter;

import ru.apfo.plantseeds.data.api.model.About;
import ru.apfo.plantseeds.data.callback.AboutCallbask;
import ru.apfo.plantseeds.data.repo.Repository;
import ru.apfo.plantseeds.view.AboutView;

public class AboutPresenter implements AboutCallbask {
    private AboutView view;
    private Repository repository;

    public AboutPresenter(AboutView view){
        this.view = view;
    }

    public void detachView(){
        view = null;
    }

    public void attachDataLayer(Repository repository) {
        this.repository = repository;
    }

    public void detachDataLayer(){
        repository = null;
    }

    public void getAbout(){
        if (repository != null && view != null){
            view.startLoading();
            repository.getAbout(this);
        }
    }

    @Override
    public void onError(String msg) {
        if (view != null) {
            view.stopLoading();
            view.showSnackbarMessage(msg);
        }
    }

    @Override
    public void onError() {
        if (view != null) {
            view.stopLoading();
        }
    }

    @Override
    public void onSuccessGetAboutData(About about) {
        if (view != null) {
            view.stopLoading();
            view.aboutLoaded(about);
        }
    }
}