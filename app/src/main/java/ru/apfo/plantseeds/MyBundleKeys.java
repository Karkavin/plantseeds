package ru.apfo.plantseeds;

public class MyBundleKeys {
    public static final String APP_BUNDLE_HOME_PRODUCT_INFO_FRAGMENT = "homeProductInfoFragment";
    public static final String APP_BUNDLE_HOME_PRODUCT_COMPANIES_FRAGMENT = "homeProductInfoFragment";
    public static final String APP_BUNDLE_COMPANY_REVIEW_RATING = "companyReviewRating";

    public static final String APP_BUNDLE_HOME_CATEGORY_DETAIL_COMPANY_FROM_NAV = "homecategoryDetailCompanyFromNav";

    public static final String APP_BUNDLE_HOME_SUBCATEGORY = "homeSubcategory";
    public static final String APP_BUNDLE_HOME_SUBCATEGORY_BOTTOM_BAR_VISIBILITY = "homeSubcategoryBottomBarVisibility";
    public static final String APP_BUNDLE_HOME_SUB_CATEGORY_DETAIL_COMPANY_FROM_NAV = "homeSubcategoryDetailCompanyFromNav";

    public static final String APP_BUNDLE_HOME_SUB_SUB_CATEGORY = "homeSubSubcategory";
    public static final String APP_BUNDLE_HOME_SUB_SUB_CATEGORY_BOTTOM_BAR_VISIBILITY = "homeSubSubcategoryBottomBarVisibility";
    public static final String APP_BUNDLE_HOME_SUB_SUB_CATEGORY_DETAIL_COMPANY_FROM_NAV = "homeSubSubcategoryDetailCompanyFromNav";

    public static final String APP_BUNDLE_VEGETABLES_HOME_CATEGORY = "vegetablesHomeCategory";
    public static final String APP_BUNDLE_FLOWERS_HOME_CATEGORY = "flowersHomeCategory";

    public static final String APP_BUNDLE_SEARCH_SEARCHBAR = "searchSearchBar";
}