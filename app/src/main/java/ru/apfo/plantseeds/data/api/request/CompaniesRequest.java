package ru.apfo.plantseeds.data.api.request;

public class CompaniesRequest {
    private String method;
    private String access_token;

    public CompaniesRequest(){

    }

    public CompaniesRequest(String method, String access_token) {
        this.method = method;
        this.access_token = access_token;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}