package ru.apfo.plantseeds.data.api.model.filter.base;

import java.io.Serializable;

import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueSingleSelectResult;

public class FilterValueSingleSelect extends BaseFilter implements Serializable{
    private FilterValueSingleSelectResult filterValueSingleSelectResult;

    public FilterValueSingleSelect() {

    }

    public FilterValueSingleSelect(int spec_id, String type, String code, String title, String collapsed) {
        super(spec_id, type, code, title, collapsed);
    }

    public FilterValueSingleSelectResult getFilterValueSingleSelectResult() {
        return filterValueSingleSelectResult;
    }

    public void setFilterValueSingleSelectResult(FilterValueSingleSelectResult filterValueSingleSelectResult) {
        this.filterValueSingleSelectResult = filterValueSingleSelectResult;
    }
}