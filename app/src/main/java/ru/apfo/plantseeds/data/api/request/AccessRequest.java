package ru.apfo.plantseeds.data.api.request;

public class AccessRequest {
    private String access_token;
    private String method;
    private Device device;

    public AccessRequest() {

    }

    public AccessRequest(String access_token, String method, String uid, String reg_token, String date) {
        this.access_token = access_token;
        this.method = method;
        this.device = device;
        this.device = new Device(uid, reg_token, date);
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    class Device{
        private String uid;
        private String reg_token;
        private String date;

        public Device(){

        }

        public Device(String uid, String reg_token, String date) {
            this.uid = uid;
            this.reg_token = reg_token;
            this.date = date;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getReg_token() {
            return reg_token;
        }

        public void setReg_token(String reg_token) {
            this.reg_token = reg_token;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }
}
