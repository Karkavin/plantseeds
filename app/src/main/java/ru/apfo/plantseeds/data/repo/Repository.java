package ru.apfo.plantseeds.data.repo;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;
import ru.apfo.plantseeds.data.callback.AboutCallbask;
import ru.apfo.plantseeds.data.callback.BaseHomeProductCallback;
import ru.apfo.plantseeds.data.callback.CompaniesCallback;
import ru.apfo.plantseeds.data.callback.CompanyReviewCallback;
import ru.apfo.plantseeds.data.callback.FilterCallback;
import ru.apfo.plantseeds.data.callback.FilterProductsCallback;
import ru.apfo.plantseeds.data.callback.HomeCategoriesCallback;
import ru.apfo.plantseeds.data.callback.HomeCompanyCallback;
import ru.apfo.plantseeds.data.callback.HomeProductsCallback;
import ru.apfo.plantseeds.data.callback.HomeSearchCallback;

public interface Repository {
    void downloadAllCategories(HomeCategoriesCallback categoriesCallback, String uid);

    void downloadAllSubcategories(HomeCategoriesCallback subcategoriesCallback, int id, int company_id);

    void downloadProducts(HomeProductsCallback productsCallback, int id, int from, int to);

    void addToFavourite(BaseHomeProductCallback baseHomeProductCallback, int id);

    void deleteFromFavourite(BaseHomeProductCallback baseHomeProductCallback, int id);

    void getDetailCompanyContent(HomeCompanyCallback companyCallback, int company_id);

    void getDetailCompanyReviews(HomeCompanyCallback companyCallback, int company_id, int product_id);

    void addShortCompanyReview(CompanyReviewCallback companyReviewCallback, int company_id, int product_id, int rating);

    void addFullCompanyReview(CompanyReviewCallback companyReviewCallback, long company_id, long product_id, String author,
                              String title, String descr, int rating, String imageTitle, List<String> imagesBase64);

    void getFavouriteProducts(HomeProductsCallback productsCallback);

    void getAbout(AboutCallbask aboutCallbask);

    void getCompanies(CompaniesCallback companiesCallback);

    void downloadVegetablesSearchData(HomeSearchCallback homeSearchCallback);

    void downloadFlowersSearchData(HomeSearchCallback homeSearchCallback);

    void getFilters(FilterCallback filterCallback);

    void getProductsByFilter(FilterProductsCallback filterProductsCallback, int pid, int from, int to, List<BaseFilter> filters);
}