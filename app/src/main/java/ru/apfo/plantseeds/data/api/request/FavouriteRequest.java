package ru.apfo.plantseeds.data.api.request;

public class FavouriteRequest {
    private String method;
    private String access_token;
    private int product_id;

    public FavouriteRequest() {

    }

    public FavouriteRequest(String method, String access_token, int product_id) {
        this.method = method;
        this.access_token = access_token;
        this.product_id = product_id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
}