package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;

public class Company implements Serializable {
    private long id;
    private String title;
    private String descr;
    private float rating;
    private int voices;

    public Company() {

    }

    public Company(long id, String title, String descr, float rating, int voices) {
        this.id = id;
        this.title = title;
        this.descr = descr;
        this.rating = rating;
        this.voices = voices;
    }

    public Company(DetailCompany detailCompany) {
        this.id = detailCompany.getId();
        this.title = detailCompany.getTitle();
        this.descr = detailCompany.getDescr();
        this.rating = detailCompany.getRating();
        this.voices = 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getVoices() {
        return voices;
    }

    public void setVoices(int voices) {
        this.voices = voices;
    }
}