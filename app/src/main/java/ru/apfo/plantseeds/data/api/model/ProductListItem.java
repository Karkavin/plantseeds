package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;
import java.util.List;

public class ProductListItem implements Serializable{
    private Product product;
    private Category category;
    private List<Image> images;
    private List<Spec> spec;
    private List<Company> companies;

    public ProductListItem(){

    }

    public ProductListItem(Product product, Category category, List<Image> images, List<Spec> spec, List<Company> companies) {
        this.product = product;
        this.category = category;
        this.images = images;
        this.spec = spec;
        this.companies = companies;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<Spec> getSpec() {
        return spec;
    }

    public void setSpec(List<Spec> spec) {
        this.spec = spec;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }
}