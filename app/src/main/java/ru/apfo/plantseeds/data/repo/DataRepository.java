package ru.apfo.plantseeds.data.repo;

import android.content.SharedPreferences;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import ru.apfo.plantseeds.MyPreferences;
import ru.apfo.plantseeds.data.api.model.About;
import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;
import ru.apfo.plantseeds.data.api.request.AboutRequest;
import ru.apfo.plantseeds.data.api.request.AccessRequest;
import ru.apfo.plantseeds.data.api.request.CompaniesRequest;
import ru.apfo.plantseeds.data.api.request.FavouriteRequest;
import ru.apfo.plantseeds.data.api.request.FilterRequest;
import ru.apfo.plantseeds.data.api.request.HomeCategoryRequest;
import ru.apfo.plantseeds.data.api.request.HomeDetailCompanyContentRequest;
import ru.apfo.plantseeds.data.api.request.HomeDetailCompanyReviewsRequest;
import ru.apfo.plantseeds.data.api.request.HomeProductRequest;
import ru.apfo.plantseeds.data.api.request.ReviewAddRequest;
import ru.apfo.plantseeds.data.api.response.HomeCategoryResponse;
import ru.apfo.plantseeds.data.callback.AboutCallbask;
import ru.apfo.plantseeds.data.callback.BaseHomeProductCallback;
import ru.apfo.plantseeds.data.callback.CompaniesCallback;
import ru.apfo.plantseeds.data.callback.CompanyReviewCallback;
import ru.apfo.plantseeds.data.callback.FilterCallback;
import ru.apfo.plantseeds.data.callback.FilterProductsCallback;
import ru.apfo.plantseeds.data.callback.HomeCategoriesCallback;
import ru.apfo.plantseeds.data.callback.HomeCompanyCallback;
import ru.apfo.plantseeds.data.callback.HomeProductsCallback;
import ru.apfo.plantseeds.data.callback.HomeSearchCallback;
import ru.apfo.plantseeds.data.db.CategoryDAO;
import ru.apfo.plantseeds.data.db.MyDatabase;
import ru.apfo.plantseeds.data.service.Service;
import ru.apfo.plantseeds.util.JSONParser;

public class DataRepository implements Repository {
    private Service service;
    private CategoryDAO categoryDAO;
    private SharedPreferences sharedPreferences;

    public DataRepository(Service service, SharedPreferences sharedPreferences) {
        this.service = service;
        this.sharedPreferences = sharedPreferences;
    }

    public DataRepository(Service service, MyDatabase db, SharedPreferences sharedPreferences) {
        this.service = service;
        this.categoryDAO = db.categoryDao();
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void downloadAllCategories(final HomeCategoriesCallback categoriesCallback, String uid) {
        final long date = System.currentTimeMillis() / 1000L;
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyy HH:mm:ss");
        Date newDate = (new Date(date));
        String formattedDate = sdf.format(newDate);
        service.getAccess(new AccessRequest(sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, ""),
                "RegDevice",
                uid,
                sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, ""),
                formattedDate))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(accessResponse -> {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, accessResponse.getAccess_token());
                    editor.commit();
                    service.getCategories(new HomeCategoryRequest("GetCategories", accessResponse.getAccess_token(), 1, 0))
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribeWith(new DisposableObserver<HomeCategoryResponse>() {
                                @Override
                                public void onNext(final HomeCategoryResponse homeCategoryResponse) {
                                    Completable.fromAction(() -> {
                                        categoryDAO.insertAll(homeCategoryResponse.getList());
                                        categoryDAO.getAll()
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribeOn(Schedulers.io())
                                                .subscribe(categoriesCallback::onSuccessGetCategories);
                                    }).subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe();
                                }

                                @Override
                                public void onError(Throwable e) {
                                    categoryDAO.getAll()
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribeOn(Schedulers.io())
                                            .subscribe(categories -> {
                                                if (categories.size() == 0)
                                                    categoriesCallback.onError("Ошибка соединения с сервером");
                                                else
                                                    categoriesCallback.onSuccessGetCategories(categories);
                                            });
                                }

                                @Override
                                public void onComplete() {

                                }
                            });
                }, throwable -> categoriesCallback.onError("Ошибка соединения с сервером"));
    }

    @Override
    public void downloadAllSubcategories(HomeCategoriesCallback categoriesCallback, int id, int company_id) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        service.getSubCategories(new HomeCategoryRequest("GetCategories", accessToken, id, company_id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(homeCategoryResponse -> categoriesCallback.onSuccessGetCategories(homeCategoryResponse.getList()),
                        throwable -> categoriesCallback.onError("Ошибка соединения с сервером"));
    }

    @Override
    public void downloadProducts(HomeProductsCallback productsCallback, int id, int from, int to) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        HomeProductRequest request = new HomeProductRequest(accessToken, "GetProducts",
                id, from, to, "yes", "yes", "yes", "yes");
        service.getProducts(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(homeProductResponse -> productsCallback.onSuccessGetProducts(homeProductResponse.getProducts()),
                        throwable -> productsCallback.onError("Ошибка соединения с сервером"));
    }

    @Override
    public void addToFavourite(BaseHomeProductCallback baseHomeProductCallback, int id) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        service.addToFavourite(new FavouriteRequest("FavoriteAdded", accessToken, id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(homeFavouriteResponse -> {
                    Log.d(getClass().getSimpleName(), id + " Added to favourite");
                }, throwable -> baseHomeProductCallback.onError("Ошибка добавления в избранное"));
    }

    @Override
    public void deleteFromFavourite(BaseHomeProductCallback baseHomeProductCallback, int id) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        service.deleteFavourite(new FavouriteRequest("FavoriteDelete", accessToken, id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(homeFavouriteResponse -> {
                    Log.d(getClass().getSimpleName(), id + " Deleted to favourite");
                }, throwable -> baseHomeProductCallback.onError("Ошибка удаления из избранное"));
    }

    @Override
    public void getDetailCompanyContent(HomeCompanyCallback homeCompanyCallback, int company_id) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        Log.d(getClass().getSimpleName(), "company_id = " + company_id);
        Log.d(getClass().getSimpleName(), "accessToken" + accessToken);
        //TODO: тестовые данные (company_id = 2)
        service.getDetailCompanyContent(new HomeDetailCompanyContentRequest(accessToken, "GetCompany", "yes", 2))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(homeDetailCompanyResponse -> homeCompanyCallback.onSuccessGetContent(homeDetailCompanyResponse.getCompany()),
                        throwable -> homeCompanyCallback.onError("Ошибка получения данных о компании"));
    }

    @Override
    public void getDetailCompanyReviews(HomeCompanyCallback companyCallback, int company_id, int product_id) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        Log.d(getClass().getSimpleName(), "GetReview");
        Log.d(getClass().getSimpleName(), "company_id = " + company_id);
        Log.d(getClass().getSimpleName(), "product_id = " + product_id);
        Log.d(getClass().getSimpleName(), "accessToken" + accessToken);
        //TODO: тестовые данные (company_id = 2) и (product_id = 116083)
        service.getDetailCompanyReviews(new HomeDetailCompanyReviewsRequest(accessToken, "GetReviews", "yes", 2, 116083))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(result -> companyCallback.onSuccessGetReviews(JSONParser.parseHomeCompanyReview(result.toString())),
                        throwable -> companyCallback.onError("Ошибка получения отзывов о компании"));
    }

    @Override
    public void addShortCompanyReview(CompanyReviewCallback companyReviewCallback, int company_id, int product_id, int rating) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        Log.d(getClass().getSimpleName(), "company_id = " + company_id);
        Log.d(getClass().getSimpleName(), "rating = " + rating);
        Log.d(getClass().getSimpleName(), "product_id = " + product_id);
        Log.d(getClass().getSimpleName(), "accessToken" + accessToken);
        //TODO: тестовые данные (company_id = 2) и (product_id = 116083)
        service.addCompanyReview(new ReviewAddRequest(accessToken, "ReviewAdded", 2, 116083, rating))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(reviewCompanyResponse -> companyReviewCallback.onSuccessAddedShortReview(company_id, product_id),
                        throwable -> companyReviewCallback.onError("Ошибка добавления отзыва"));
    }

    @Override
    public void addFullCompanyReview(CompanyReviewCallback companyReviewCallback, long company_id, long product_id, String author,
                                     String title, String descr, int rating, String imageTitle, List<String> imagesBase64) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        //TODO: тестовые данные (company_id = 2) и (product_id = 116083) и Author и Title и ImageTitle
        ReviewAddRequest reviewAddRequest = new ReviewAddRequest(accessToken, "ReviewAdded", 2, 116083,
                author, "Test title", descr, rating);
        reviewAddRequest.addImages(imagesBase64);
//        TODO: презентер должен отправить только данные, логики с выбором не должно быть
//        if (imageBase64 != null && !imageBase64.replaceAll(" ", "").equals("")){
//            reviewAddRequest.addImage("imtitle", imageBase64);
//            Log.d(getClass().getSimpleName(), "Add images: image: " + imageBase64);
//        } else{
//            Log.d(getClass().getSimpleName(), "No images!!!!!");
//            reviewAddRequest.setImages(null);
//        }
        service.addCompanyReview(reviewAddRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(reviewCompanyResponse -> companyReviewCallback.onSuccessAddedFullReview(),
                        throwable -> companyReviewCallback.onError("Ошибка добавления отзыва"));
    }

    @Override
    public void getFavouriteProducts(HomeProductsCallback productsCallback) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        Log.d("", accessToken);
        service.getFavouriteProducts(new HomeProductRequest(accessToken, "GetFavoritesList",
                "yes", "yes", "yes", "yes"))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(productResponse -> {
                    Log.d(getClass().getSimpleName(), "Favourite count = " + productResponse.getProducts().size());
                    productsCallback.onSuccessGetProducts(productResponse.getProducts());
                        },
                        throwable -> {
                    if (!(throwable instanceof NullPointerException)) {
                        productsCallback.onError("Ошибка получения избранных товаров");
                    } else{
                        productsCallback.onError();
                    }
                });
    }

    @Override
    public void getAbout(AboutCallbask aboutCallbask) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        Log.d("Get ABOUT: accessToken", accessToken);
        // TODO: бага, периодично взникает null в полях, хотя объект не null. Проверить, пока в заглушке
        service.getAbout(new AboutRequest("About", accessToken))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(aboutResponse -> aboutCallbask.onSuccessGetAboutData(new About(aboutResponse)),
                        throwable -> aboutCallbask.onError("Ошибка получения описания"));
    }

    @Override
    public void getCompanies(CompaniesCallback companiesCallback) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        service.getCompanies(new CompaniesRequest("GetCompanies", accessToken))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(companiesResponse -> companiesCallback.onSuccessGetCompanies(companiesResponse.getList()),
                        throwable -> companiesCallback.onError("Ошибка получения списка компаний"));
    }

    @Override
    public void downloadVegetablesSearchData(HomeSearchCallback homeSearchCallback) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        service.getSubCategories(new HomeCategoryRequest("GetCategories", accessToken, 2, 0))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(homeCategoryResponse -> homeSearchCallback.onSuccessGetVegetablesSearchData(homeCategoryResponse.getList()),
                        throwable -> homeSearchCallback.onError("Ошибка соединения с сервером"));
    }

    @Override
    public void downloadFlowersSearchData(HomeSearchCallback homeSearchCallback) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        service.getSubCategories(new HomeCategoryRequest("GetCategories", accessToken, 3, 0))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(homeCategoryResponse -> homeSearchCallback.onSuccessGetFlowersSearchData(homeCategoryResponse.getList()),
                        throwable -> homeSearchCallback.onError("Ошибка соединения с сервером"));
    }

    @Override
    public void getFilters(FilterCallback filterCallback) {
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        service.getFilters(new FilterRequest("GetFilters", accessToken))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(result -> filterCallback.onSuccessfullGetFilters(JSONParser.parseFilters(result.toString())),
                        throwable -> filterCallback.onError("Ошибка соединения с сервером"));
    }

    @Override
    public void getProductsByFilter(FilterProductsCallback filterProductsCallback, int pid, int from, int to, List<BaseFilter> filters) {
        Log.d(getClass().getSimpleName(), "Downloading getProductsByFilter()");
        String accessToken = sharedPreferences.getString(MyPreferences.APP_PREFERENCES_ACCESS_TOKEN, "");
        service.getProductsFilters(JSONParser.parseDataForFiltersProduct(accessToken, "GetProducts", pid, from, to,
                "yes", "yes", "yes", "yes", filters))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(result -> filterProductsCallback.onSuccessfullGetProductsByFilters(result.getProducts()),
                        throwable -> filterProductsCallback.onError("Ошибка соединения с сервером"));
    }
}