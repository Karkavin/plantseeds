package ru.apfo.plantseeds.data.api.model.filter.result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FilterValueMultiplyResult extends BaseFilterResult implements Serializable{
    private List<String> values;

    public FilterValueMultiplyResult() {
        this.values = new ArrayList<>();
    }

    public FilterValueMultiplyResult(int spec_id, String type, String code) {
        super(spec_id, type, code);
        this.values = new ArrayList<>();
    }

    public FilterValueMultiplyResult(int spec_id, String type, String code, List<String> values) {
        super(spec_id, type, code);
        this.values = values;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public void addValue(String value){
        this.values.add(value);
    }

    public void remove(int index){
        this.values.remove(index);
    }
}