package ru.apfo.plantseeds.data.api.request;

public class HomeCategoryRequest {
    private String method;
    private String access_token;
    private int pid;
    private int company_id;

    public HomeCategoryRequest(){

    }

    public HomeCategoryRequest(String method, String access_token, int pid, int company_id) {
        this.method = method;
        this.access_token = access_token;
        this.pid = pid;
        this.company_id = company_id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }
}