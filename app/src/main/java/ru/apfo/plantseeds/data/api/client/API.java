package ru.apfo.plantseeds.data.api.client;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import ru.apfo.plantseeds.data.api.request.AboutRequest;
import ru.apfo.plantseeds.data.api.request.AccessRequest;
import ru.apfo.plantseeds.data.api.request.CompaniesRequest;
import ru.apfo.plantseeds.data.api.request.FilterRequest;
import ru.apfo.plantseeds.data.api.request.HomeProductRequest;
import ru.apfo.plantseeds.data.api.response.AboutResponse;
import ru.apfo.plantseeds.data.api.response.AccessResponse;
import ru.apfo.plantseeds.data.api.response.CompaniesResponse;
import ru.apfo.plantseeds.data.api.response.HomeProductResponse;

public interface API {
    @POST(".")
    Observable<AccessResponse> getAccess(@Body AccessRequest request);

    @POST(".")
    Observable<HomeProductResponse> getFavouriteProducts(@Body HomeProductRequest request);

    @POST(".")
    Observable<AboutResponse> getAbout(@Body AboutRequest request);

    @POST(".")
    Observable<CompaniesResponse> getCompanies(@Body CompaniesRequest request);

    @POST(".")
    Observable<JsonElement> getFilters(@Body FilterRequest request);

    @POST(".")
    Observable<HomeProductResponse> getProductsFilters(@Body JsonObject request);
}