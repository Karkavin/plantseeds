package ru.apfo.plantseeds.data.api.request;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;

public class FilterProductsRequest {
    private String access_token;
    private String method;
    private int pid;
    private List<BaseFilter> spec;

    public FilterProductsRequest(){
        spec = new ArrayList<>();
    }

    public FilterProductsRequest(String access_token, String method, int pid, List<BaseFilter> spec) {
        this.access_token = access_token;
        this.method = method;
        this.pid = pid;
        this.spec = spec;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public List<BaseFilter> getSpec() {
        return spec;
    }

    public void setSpec(List<BaseFilter> spec) {
        this.spec = spec;
    }
}
