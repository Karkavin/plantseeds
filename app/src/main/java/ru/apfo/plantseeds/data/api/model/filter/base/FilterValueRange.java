package ru.apfo.plantseeds.data.api.model.filter.base;

import java.io.Serializable;

import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueRangeResult;

public class FilterValueRange extends BaseFilter implements Serializable{
    private ValueRange values;
    private FilterValueRangeResult filterValueRangeResult;

    public FilterValueRange(){

    }

    public FilterValueRange(int spec_id, String type, String code, String title, String collapsed) {
        super(spec_id, type, code, title, collapsed);
    }

    public FilterValueRange(int spec_id, String type, String code, String title, String collapsed, ValueRange values) {
        super(spec_id, type, code, title, collapsed);
        this.values = values;
    }

    public ValueRange getValues() {
        return values;
    }

    public void setValues(ValueRange values) {
        this.values = values;
    }

    public FilterValueRangeResult getFilterValueRangeResult() {
        return filterValueRangeResult;
    }

    public void setFilterValueRangeResult(FilterValueRangeResult filterValueRangeResult) {
        this.filterValueRangeResult = filterValueRangeResult;
    }
}