package ru.apfo.plantseeds.data.api.model.filter.result;

import java.io.Serializable;

public class FilterValueRangeResult extends BaseFilterResult implements Serializable {
    private int from;
    private int to;

    public FilterValueRangeResult() {

    }

    public FilterValueRangeResult(int spec_id, String type, String code) {
        super(spec_id, type, code);
    }

    public FilterValueRangeResult(int spec_id, String type, String code, int from, int to) {
        super(spec_id, type, code);
        this.from = from;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}