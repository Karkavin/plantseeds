package ru.apfo.plantseeds.data.api.response;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.ProductListItem;

public class HomeProductResponse {
    private List<ProductListItem> products;

    public HomeProductResponse(){

    }

    public HomeProductResponse(List<ProductListItem> products) {
        this.products = products;
    }

    public List<ProductListItem> getProducts() {
        return products;
    }

    public void setProducts(List<ProductListItem> products) {
        this.products = products;
    }
}