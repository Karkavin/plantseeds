package ru.apfo.plantseeds.data.api.model.filter.result;

import java.io.Serializable;

public class BaseFilterResult implements Serializable{
    private int spec_id;
    private String type;
    private String code;

    public BaseFilterResult(){

    }

    public BaseFilterResult(int spec_id, String type, String code) {
        this.spec_id = spec_id;
        this.type = type;
        this.code = code;
    }

    public int getSpec_id() {
        return spec_id;
    }

    public void setSpec_id(int spec_id) {
        this.spec_id = spec_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}