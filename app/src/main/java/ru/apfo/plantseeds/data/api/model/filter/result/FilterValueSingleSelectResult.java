package ru.apfo.plantseeds.data.api.model.filter.result;

import java.io.Serializable;

public class FilterValueSingleSelectResult extends BaseFilterResult implements Serializable{
    private String value;

    public FilterValueSingleSelectResult(){

    }

    public FilterValueSingleSelectResult(int spec_id, String type, String code) {
        super(spec_id, type, code);
    }

    public FilterValueSingleSelectResult(int spec_id, String type, String code, String value) {
        super(spec_id, type, code);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}