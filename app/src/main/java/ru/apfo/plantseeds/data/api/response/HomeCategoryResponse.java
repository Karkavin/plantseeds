package ru.apfo.plantseeds.data.api.response;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.HomeCategory;

public class HomeCategoryResponse {
    private int counts;
    private List<HomeCategory> list;

    public HomeCategoryResponse(){

    }

    public HomeCategoryResponse(int counts, List<HomeCategory> list) {
        this.counts = counts;
        this.list = list;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public List<HomeCategory> getList() {
        return list;
    }

    public void setList(List<HomeCategory> list) {
        this.list = list;
    }
}