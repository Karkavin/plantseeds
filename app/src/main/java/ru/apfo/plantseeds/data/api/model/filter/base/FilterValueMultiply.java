package ru.apfo.plantseeds.data.api.model.filter.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueMultiplyResult;

public class FilterValueMultiply extends BaseFilter implements Serializable {
    private List<ValueMultiply> values;
    private FilterValueMultiplyResult filterValueMultiplyResult;

    public FilterValueMultiply() {
        values = new ArrayList<>();
    }

    public FilterValueMultiply(int spec_id, String type, String code, String title, String collapsed, List<ValueMultiply> values) {
        super(spec_id, type, code, title, collapsed);
        this.values = values;
    }

    public List<ValueMultiply> getValues() {
        return values;
    }

    public void setValues(List<ValueMultiply> values) {
        this.values = values;
    }

    public void addValue(ValueMultiply valueMultiply){
        if (values != null){
            values.add(valueMultiply);
        }
    }

    public FilterValueMultiplyResult getFilterValueMultiplyResult() {
        return filterValueMultiplyResult;
    }

    public void setFilterValueMultiplyResult(FilterValueMultiplyResult filterValueMultiplyResult) {
        this.filterValueMultiplyResult = filterValueMultiplyResult;
    }
}