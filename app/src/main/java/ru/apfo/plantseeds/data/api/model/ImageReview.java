package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;

public class ImageReview implements Serializable {
    private long id;
    private String url;

    public ImageReview() {

    }

    public ImageReview(long id, String url) {
        this.id = id;
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}