package ru.apfo.plantseeds.data.api.client;

import com.google.gson.JsonElement;

import org.json.JSONObject;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import ru.apfo.plantseeds.data.api.request.FavouriteRequest;
import ru.apfo.plantseeds.data.api.request.HomeCategoryRequest;
import ru.apfo.plantseeds.data.api.request.HomeDetailCompanyContentRequest;
import ru.apfo.plantseeds.data.api.request.HomeDetailCompanyReviewsRequest;
import ru.apfo.plantseeds.data.api.request.HomeProductRequest;
import ru.apfo.plantseeds.data.api.request.ReviewAddRequest;
import ru.apfo.plantseeds.data.api.response.HomeCategoryResponse;
import ru.apfo.plantseeds.data.api.response.HomeDetailCompanyContentResponse;
import ru.apfo.plantseeds.data.api.response.HomeFavouriteResponse;
import ru.apfo.plantseeds.data.api.response.HomeProductResponse;
import ru.apfo.plantseeds.data.api.response.ReviewCompanyResponse;

public interface HomeAPI {
    @POST(".")
    Observable<HomeCategoryResponse> getCategories(@Body HomeCategoryRequest request);

    @POST(".")
    Observable<HomeCategoryResponse> getSubCategories(@Body HomeCategoryRequest request);

    @POST(".")
    Observable<HomeProductResponse> getProducts(@Body HomeProductRequest request);

    @POST(".")
    Observable<HomeFavouriteResponse> addToFavourite(@Body FavouriteRequest request);

    @POST(".")
    Observable<HomeFavouriteResponse> deleteFromFavourite(@Body FavouriteRequest request);

    @POST(".")
    Observable<HomeDetailCompanyContentResponse> getDetailCompanyContent(@Body HomeDetailCompanyContentRequest request);

    @POST(".")
    Observable<JsonElement> getDetailCompanyReview(@Body HomeDetailCompanyReviewsRequest request);

    @POST(".")
    Observable<ReviewCompanyResponse> addCompanyReview(@Body ReviewAddRequest request);
}