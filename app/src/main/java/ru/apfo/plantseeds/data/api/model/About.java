package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;

import ru.apfo.plantseeds.data.api.response.AboutResponse;

public class About implements Serializable{
    private String title;
    private String descr;
    private String email;

    public About(){

    }

    public About(String title, String descr, String email) {
        this.title = title;
        this.descr = descr;
        this.email = email;
    }

    public About(AboutResponse response) {
        this.title = response.getTitle();
        this.descr = response.getDescr();
        this.email = response.getEmail();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}