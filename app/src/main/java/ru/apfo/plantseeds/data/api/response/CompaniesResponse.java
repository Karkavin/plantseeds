package ru.apfo.plantseeds.data.api.response;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.DetailCompany;

public class CompaniesResponse {
    private int counts;
    private List<DetailCompany> list;

    public CompaniesResponse(){

    }

    public CompaniesResponse(int counts, List<DetailCompany> list) {
        this.counts = counts;
        this.list = list;
    }

    public int getCounts() {
        return counts;
    }

    public void setCounts(int counts) {
        this.counts = counts;
    }

    public List<DetailCompany> getList() {
        return list;
    }

    public void setList(List<DetailCompany> list) {
        this.list = list;
    }
}