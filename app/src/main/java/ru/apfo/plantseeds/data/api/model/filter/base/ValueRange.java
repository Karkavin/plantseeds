package ru.apfo.plantseeds.data.api.model.filter.base;

import java.io.Serializable;

public class ValueRange implements Serializable {
    private int from;
    private int to;

    public ValueRange() {

    }

    public ValueRange(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}