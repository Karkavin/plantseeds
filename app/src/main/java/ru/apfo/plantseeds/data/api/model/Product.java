package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;

public class Product implements Serializable{
    private long id;
    private String title;
    private String descr;
    private String image;
    private String favorite;
    private String gosreestr;

    public Product(){

    }

    public Product(long id, String title, String descr, String image, String favorite, String gosreestr) {
        this.id = id;
        this.title = title;
        this.descr = descr;
        this.image = image;
        this.favorite = favorite;
        this.gosreestr = gosreestr;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getGosreestr() {
        return gosreestr;
    }

    public void setGosreestr(String gosreestr) {
        this.gosreestr = gosreestr;
    }
}