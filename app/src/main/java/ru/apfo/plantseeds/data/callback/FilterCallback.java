package ru.apfo.plantseeds.data.callback;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;

public interface FilterCallback extends BaseHomeProductCallback {
    void onSuccessfullGetFilters(List<BaseFilter> filters);
}