package ru.apfo.plantseeds.data.api.response;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.DetailCompany;

public class HomeDetailCompanyContentResponse {
    private List<DetailCompany> company;

    public HomeDetailCompanyContentResponse(){

    }

    public HomeDetailCompanyContentResponse(List<DetailCompany> company) {
        this.company = company;
    }

    public List<DetailCompany> getCompany() {
        return company;
    }

    public void setCompany(List<DetailCompany> company) {
        this.company = company;
    }
}