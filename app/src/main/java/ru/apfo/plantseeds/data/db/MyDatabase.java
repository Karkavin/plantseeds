package ru.apfo.plantseeds.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ru.apfo.plantseeds.data.api.model.HomeCategory;

@Database(entities = {HomeCategory.class}, version = 2)
public abstract class MyDatabase extends RoomDatabase {
    public abstract CategoryDAO categoryDao();
}