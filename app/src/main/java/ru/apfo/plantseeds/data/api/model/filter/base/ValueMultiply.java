package ru.apfo.plantseeds.data.api.model.filter.base;

import java.io.Serializable;

public class ValueMultiply implements Serializable {
    private String code;
    private int count;

    public ValueMultiply() {

    }

    public ValueMultiply(String code, int count) {
        this.code = code;
        this.count = count;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}