package ru.apfo.plantseeds.data.api.response;

public class AboutResponse {
    private String title;
    private String descr;
    private String email;

    public AboutResponse(){

    }

    public AboutResponse(String title, String descr, String email) {
        this.title = title;
        this.descr = descr;
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}