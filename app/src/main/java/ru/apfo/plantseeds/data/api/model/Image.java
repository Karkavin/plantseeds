package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;

public class Image implements Serializable {
    private long id;
    private long pos;
    private String filename;
    private Url url;
    private String alt;

    public Image(){

    }

    public Image(long id, long pos, String filename, Url url, String alt) {
        this.id = id;
        this.pos = pos;
        this.filename = filename;
        this.url = url;
        this.alt = alt;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPos() {
        return pos;
    }

    public void setPos(long pos) {
        this.pos = pos;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }
}