package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DetailCompany implements Serializable {
    private long id;
    private String title;
    private String descr;
    private float rating;
    private String logo_url;
    private String country;

    public DetailCompany() {

    }

    public DetailCompany(long id, String title, String descr, float rating, String logo_url, String country) {
        this.id = id;
        this.title = title;
        this.descr = descr;
        this.rating = rating;
        this.logo_url = logo_url;
        this.country = country;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getLogo_url() {
        return logo_url;
    }

    public void setLogo_url(String logo_url) {
        this.logo_url = logo_url;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public static List<Company> detailCompanyListToCompanyList(List<DetailCompany> detailCompanies){
        List<Company> companies = new ArrayList<>();

        for (int i = 0; i < detailCompanies.size(); i++){
            companies.add(new Company(detailCompanies.get(i)));
        }

        return companies;
    }
}