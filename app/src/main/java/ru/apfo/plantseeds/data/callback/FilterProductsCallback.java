package ru.apfo.plantseeds.data.callback;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.ProductListItem;

public interface FilterProductsCallback extends BaseHomeProductCallback {
    void onSuccessfullGetProductsByFilters(List<ProductListItem> products);
}