package ru.apfo.plantseeds.data.callback;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.DetailCompany;

public interface CompaniesCallback extends BaseHomeProductCallback {
    void onSuccessGetCompanies(List<DetailCompany> companies);

}