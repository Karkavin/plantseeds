package ru.apfo.plantseeds.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Maybe;
import ru.apfo.plantseeds.data.api.model.HomeCategory;

@Dao
public interface CategoryDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    List<Long> insertAll(List<HomeCategory> categories);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(HomeCategory category);

    @Query("SELECT * FROM " + HomeCategory.TABLE_NAME)
    Maybe<List<HomeCategory>> getAll();
}