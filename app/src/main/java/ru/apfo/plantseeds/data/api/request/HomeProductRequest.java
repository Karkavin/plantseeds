package ru.apfo.plantseeds.data.api.request;

public class HomeProductRequest {
    private String access_token;
    private String method;
    private FilterCategory filter;
    private Pagination pagination;
    private BlockView block_view;

    public HomeProductRequest(){

    }

    public HomeProductRequest(String access_token, String method,
                              long filterCategory,
                              int paginationFrom, int paginationTo,
                              String blockViewCategory, String blockViewImages, String blockViewSpec, String blockViewCompanies) {
        this.access_token = access_token;
        this.method = method;
        this.filter = new FilterCategory(filterCategory);
        this.pagination = new Pagination(paginationFrom, paginationTo);
        this.block_view = new BlockView(blockViewCategory, blockViewImages, blockViewSpec, blockViewCompanies);
    }

    public HomeProductRequest(String access_token, String method,
                              String blockViewCategory, String blockViewImages, String blockViewSpec, String blockViewCompanies) {
        this.access_token = access_token;
        this.method = method;
        this.filter = null;
        this.pagination = new Pagination(0, 1000);
        this.block_view = new BlockView(blockViewCategory, blockViewImages, blockViewSpec, blockViewCompanies);
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public FilterCategory getFilter() {
        return filter;
    }

    public void setFilter(FilterCategory filter) {
        this.filter = filter;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public BlockView getBlock_view() {
        return block_view;
    }

    public void setBlock_view(BlockView block_view) {
        this.block_view = block_view;
    }

    class FilterCategory {
        private long category;

        public FilterCategory(){

        }

        public FilterCategory(long category) {
            this.category = category;
        }

        public long getCategory() {
            return category;
        }

        public void setCategory(long category) {
            this.category = category;
        }
    }

    class FilterFavourite {
        private String favorites;

        public FilterFavourite(){

        }

        public FilterFavourite(String favorites){
            this.favorites = favorites;
        }

        public String getFavorites() {
            return favorites;
        }

        public void setFavorites(String favorites) {
            this.favorites = favorites;
        }
    }

    class Pagination {
        private int from;
        private int to;

        public Pagination(){

        }

        public Pagination(int from, int to) {
            this.from = from;
            this.to = to;
        }
    }

    class BlockView {
        private String category;
        private String images;
        private String spec;
        private String companies;

        public BlockView(){

        }

        public BlockView(String category, String images, String spec, String companies) {
            this.category = category;
            this.images = images;
            this.spec = spec;
            this.companies = companies;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getSpec() {
            return spec;
        }

        public void setSpec(String spec) {
            this.spec = spec;
        }

        public String getCompanies() {
            return companies;
        }

        public void setCompanies(String companies) {
            this.companies = companies;
        }
    }
}