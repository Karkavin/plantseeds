package ru.apfo.plantseeds.data.callback;

public interface CompanyReviewCallback extends BaseHomeProductCallback{
    void onSuccessAddedShortReview(int company_id, int product_id);

    void onSuccessAddedFullReview();
}