package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;

public class Url implements Serializable {
    private String sd;
    private String hd;

    public Url(){

    }

    public Url(String sd, String hd) {
        this.sd = sd;
        this.hd = hd;
    }

    public String getSd() {
        return sd;
    }

    public void setSd(String sd) {
        this.sd = sd;
    }

    public String getHd() {
        return hd;
    }

    public void setHd(String hd) {
        this.hd = hd;
    }
}