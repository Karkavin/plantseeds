package ru.apfo.plantseeds.data.api.response;

public class AccessResponse {
    private String status;
    private String message;
    private String access_token;

    public AccessResponse(){

    }

    public AccessResponse(String status, String message, String access_token) {
        this.status = status;
        this.message = message;
        this.access_token = access_token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}