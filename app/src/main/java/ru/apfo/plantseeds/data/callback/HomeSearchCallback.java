package ru.apfo.plantseeds.data.callback;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.HomeCategory;

public interface HomeSearchCallback extends BaseHomeProductCallback {
    void onSuccessGetVegetablesSearchData(List<HomeCategory> vegetablesSearchData);

    void onSuccessGetFlowersSearchData(List<HomeCategory> flowersSearchData);
}