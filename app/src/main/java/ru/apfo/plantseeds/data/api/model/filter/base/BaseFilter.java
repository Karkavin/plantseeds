package ru.apfo.plantseeds.data.api.model.filter.base;

import java.io.Serializable;

public class BaseFilter implements Serializable{
    private int spec_id;
    private String type;
    private String code;
    private String title;
    private String collapsed;

    public BaseFilter() {

    }

    public BaseFilter(int spec_id, String type, String code, String title, String collapsed) {
        this.spec_id = spec_id;
        this.type = type;
        this.code = code;
        this.title = title;
        this.collapsed = collapsed;
    }

    public int getSpec_id() {
        return spec_id;
    }

    public void setSpec_id(int spec_id) {
        this.spec_id = spec_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCollapsed() {
        return collapsed;
    }

    public void setCollapsed(String collapsed) {
        this.collapsed = collapsed;
    }
}