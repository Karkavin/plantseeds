package ru.apfo.plantseeds.data.callback;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.CompanyReview;
import ru.apfo.plantseeds.data.api.model.DetailCompany;

public interface HomeCompanyCallback extends BaseHomeProductCallback{
    void onSuccessGetContent(List<DetailCompany> detailCompanies);

    void onSuccessGetReviews(List<CompanyReview> companyReviews);
}