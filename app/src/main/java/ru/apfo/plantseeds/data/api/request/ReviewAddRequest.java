package ru.apfo.plantseeds.data.api.request;

import java.util.ArrayList;
import java.util.List;

public class ReviewAddRequest {
    private String access_token;
    private String method;
    private long company_id;
    private long product_id;
    private Review review;
    private List<Image> images;

    public ReviewAddRequest(){
        this.images = new ArrayList<>();
    }

    public ReviewAddRequest(String access_token, String method, long company_id, long product_id,
                            String author, String title, String descr, int rating) {
        this.access_token = access_token;
        this.method = method;
        this.company_id = company_id;
        this.product_id = product_id;
        this.review = new Review(author, title, descr, rating);
        this.images = new ArrayList<>();
    }

    public ReviewAddRequest(String access_token, String method, long company_id, long product_id, int rating) {
        this.access_token = access_token;
        this.method = method;
        this.company_id = company_id;
        this.product_id = product_id;
        this.review = new Review("", "", "", rating);
        this.images = new ArrayList<>();
    }

    public void addImages(List<String> imagesBase64){
        for (int i = 0; i < imagesBase64.size(); i++){
            this.images.add(new Image("", imagesBase64.get(i)));
        }
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(long company_id) {
        this.company_id = company_id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    class Review {
        private String author;
        private String title;
        private String descr;
        private int rating;

        public Review(){

        }

        public Review(String author, String title, String descr, int rating) {
            this.author = author;
            this.title = title;
            this.descr = descr;
            this.rating = rating;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescr() {
            return descr;
        }

        public void setDescr(String descr) {
            this.descr = descr;
        }

        public int getRating() {
            return rating;
        }

        public void setRating(int rating) {
            this.rating = rating;
        }
    }

    class Image{
        private String title;
        private String base64;

        public Image(){

        }

        public Image(String title, String base64) {
            this.title = title;
            this.base64 = base64;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBase64() {
            return base64;
        }

        public void setBase64(String base64) {
            this.base64 = base64;
        }
    }
}