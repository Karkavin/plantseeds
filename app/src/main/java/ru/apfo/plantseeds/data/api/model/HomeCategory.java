package ru.apfo.plantseeds.data.api.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.io.Serializable;

@Entity(tableName = HomeCategory.TABLE_NAME)
public class HomeCategory implements Serializable{
    public static final String TABLE_NAME = "HomeCategory";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PID = "pid";
    public static final String COLUMN_VIEW_TYPE = "view_type";
    public static final String COLUMN_IS_LAST = "is_last";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_ICON_URL = "icon_url";
    public static final String COLUMN_IMG_URL = "img_url";
    public static final String COLUMN_DESCR = "descr";

    @PrimaryKey
    @NonNull
    @ColumnInfo(index = true, name = COLUMN_ID)
    private int id;
    @ColumnInfo(name = COLUMN_PID)
    private int pid;
    @ColumnInfo(name = COLUMN_VIEW_TYPE)
    private int view_type;
    @ColumnInfo(name = COLUMN_IS_LAST)
    private int is_last;
    @ColumnInfo(name = COLUMN_TITLE)
    private String title;
    @ColumnInfo(name = COLUMN_ICON_URL)
    private String icon_url;
    @ColumnInfo(name = COLUMN_IMG_URL)
    private String img_url;
    @ColumnInfo(name = COLUMN_DESCR)
    private String descr;

    public HomeCategory(){

    }

    @Ignore
    public HomeCategory(@NonNull int id, int pid, int view_type, int is_last, String title, String icon_url, String img_url, String descr) {
        this.id = id;
        this.pid = pid;
        this.view_type = view_type;
        this.is_last = is_last;
        this.title = title;
        this.icon_url = icon_url;
        this.img_url = img_url;
        this.descr = descr;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public void setId(@NonNull int id) {
        this.id = id;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getView_type() {
        return view_type;
    }

    public void setView_type(int view_type) {
        this.view_type = view_type;
    }

    public int getIs_last() {
        return is_last;
    }

    public void setIs_last(int is_last) {
        this.is_last = is_last;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public void setIcon_url(String icon_url) {
        this.icon_url = icon_url;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    @Override
    public boolean equals(Object obj) {
        return !(obj == null || !(obj instanceof HomeCategory)) && this.getId() == ((HomeCategory) obj).getId();
    }
}