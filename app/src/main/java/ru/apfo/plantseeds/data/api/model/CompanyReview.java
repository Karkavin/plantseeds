package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;
import java.util.List;

public class CompanyReview implements Serializable{
    private long id;
    private String date;
    private float rating;
    private String author;
    private String text;
    private List<ImageReview> images;

    public CompanyReview() {

    }

    public CompanyReview(long id, String date, float rating, String author, String text, List<ImageReview> images) {
        this.id = id;
        this.date = date;
        this.rating = rating;
        this.author = author;
        this.text = text;
        this.images = images;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ImageReview> getImages() {
        return images;
    }

    public void setImages(List<ImageReview> images) {
        this.images = images;
    }
}