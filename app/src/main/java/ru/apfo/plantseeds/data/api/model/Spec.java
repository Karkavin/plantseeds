package ru.apfo.plantseeds.data.api.model;

import java.io.Serializable;

public class Spec implements Serializable {
    private int pos;
    private String title;
    private String value;

    public Spec(){

    }

    public Spec(int pos, String title, String value) {
        this.pos = pos;
        this.title = title;
        this.value = value;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}