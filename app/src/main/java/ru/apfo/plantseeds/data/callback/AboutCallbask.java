package ru.apfo.plantseeds.data.callback;

import ru.apfo.plantseeds.data.api.model.About;

public interface AboutCallbask extends BaseHomeProductCallback {
    void onSuccessGetAboutData(About about);
}