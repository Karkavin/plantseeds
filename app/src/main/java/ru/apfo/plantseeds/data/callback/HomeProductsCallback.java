package ru.apfo.plantseeds.data.callback;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.ProductListItem;

public interface HomeProductsCallback extends BaseHomeProductCallback{
    void onSuccessGetProducts(List<ProductListItem> products);
}