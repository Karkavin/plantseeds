package ru.apfo.plantseeds.data.api.request;

public class HomeDetailCompanyReviewsRequest {
    private String access_token;
    private String method;
    private String view_contacts;
    private long company_id;
    private long product_id;

    public HomeDetailCompanyReviewsRequest(){

    }

    public HomeDetailCompanyReviewsRequest(String access_token, String method, String view_contacts, long company_id, long product_id) {
        this.access_token = access_token;
        this.method = method;
        this.view_contacts = view_contacts;
        this.company_id = company_id;
        this.product_id = product_id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getView_contacts() {
        return view_contacts;
    }

    public void setView_contacts(String view_contacts) {
        this.view_contacts = view_contacts;
    }

    public long getCompany_id() {
        return company_id;
    }

    public void setCompany_id(long company_id) {
        this.company_id = company_id;
    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }
}