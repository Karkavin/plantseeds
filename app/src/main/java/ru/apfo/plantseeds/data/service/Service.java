package ru.apfo.plantseeds.data.service;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.reactivex.Observable;
import ru.apfo.plantseeds.APIGenerator;
import ru.apfo.plantseeds.data.api.client.API;
import ru.apfo.plantseeds.data.api.client.HomeAPI;
import ru.apfo.plantseeds.data.api.request.AboutRequest;
import ru.apfo.plantseeds.data.api.request.AccessRequest;
import ru.apfo.plantseeds.data.api.request.CompaniesRequest;
import ru.apfo.plantseeds.data.api.request.FavouriteRequest;
import ru.apfo.plantseeds.data.api.request.FilterRequest;
import ru.apfo.plantseeds.data.api.request.HomeCategoryRequest;
import ru.apfo.plantseeds.data.api.request.HomeDetailCompanyContentRequest;
import ru.apfo.plantseeds.data.api.request.HomeDetailCompanyReviewsRequest;
import ru.apfo.plantseeds.data.api.request.HomeProductRequest;
import ru.apfo.plantseeds.data.api.request.ReviewAddRequest;
import ru.apfo.plantseeds.data.api.response.AboutResponse;
import ru.apfo.plantseeds.data.api.response.AccessResponse;
import ru.apfo.plantseeds.data.api.response.CompaniesResponse;
import ru.apfo.plantseeds.data.api.response.HomeCategoryResponse;
import ru.apfo.plantseeds.data.api.response.HomeDetailCompanyContentResponse;
import ru.apfo.plantseeds.data.api.response.HomeFavouriteResponse;
import ru.apfo.plantseeds.data.api.response.HomeProductResponse;
import ru.apfo.plantseeds.data.api.response.ReviewCompanyResponse;

public class Service extends android.app.Service {
    private final IBinder binder = new Service.LocalBinder();

    public class LocalBinder extends Binder {
        public Service getService() {
            return Service.this;
        }
    }

    public Observable<AccessResponse> getAccess(AccessRequest request){
        return APIGenerator.create(API.class).getAccess(request);
    }

    public Observable<HomeCategoryResponse> getCategories(HomeCategoryRequest request){
        return APIGenerator.create(HomeAPI.class).getCategories(request);
    }

    public Observable<HomeCategoryResponse> getSubCategories(HomeCategoryRequest request){
        return APIGenerator.create(HomeAPI.class).getSubCategories(request);
    }

    public Observable<HomeProductResponse> getProducts(HomeProductRequest request){
        return APIGenerator.create(HomeAPI.class).getProducts(request);
    }

    public Observable<HomeFavouriteResponse> addToFavourite(FavouriteRequest request){
        return APIGenerator.create(HomeAPI.class).addToFavourite(request);
    }

    public Observable<HomeFavouriteResponse> deleteFavourite(FavouriteRequest request){
        return APIGenerator.create(HomeAPI.class).deleteFromFavourite(request);
    }

    public Observable<HomeDetailCompanyContentResponse> getDetailCompanyContent(HomeDetailCompanyContentRequest request){
        return APIGenerator.create(HomeAPI.class).getDetailCompanyContent(request);
    }

    public Observable<JsonElement> getDetailCompanyReviews(HomeDetailCompanyReviewsRequest request){
        return APIGenerator.create(HomeAPI.class).getDetailCompanyReview(request);
    }

    public Observable<ReviewCompanyResponse> addCompanyReview(ReviewAddRequest request){
        return APIGenerator.create(HomeAPI.class).addCompanyReview(request);
    }

    public Observable<HomeProductResponse> getFavouriteProducts(HomeProductRequest request){
        return APIGenerator.create(API.class).getFavouriteProducts(request);
    }

    public Observable<AboutResponse> getAbout(AboutRequest request){
        return APIGenerator.create(API.class).getAbout(request);
    }

    public Observable<CompaniesResponse> getCompanies(CompaniesRequest request){
        return APIGenerator.create(API.class).getCompanies(request);
    }

    public Observable<JsonElement> getFilters(FilterRequest request){
        return APIGenerator.create(API.class).getFilters(request);
    }

    public Observable<HomeProductResponse> getProductsFilters(JsonObject request){
        return APIGenerator.create(API.class).getProductsFilters(request);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }
}
