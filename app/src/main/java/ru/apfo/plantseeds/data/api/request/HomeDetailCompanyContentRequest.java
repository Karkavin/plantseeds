package ru.apfo.plantseeds.data.api.request;

public class HomeDetailCompanyContentRequest {
    private String access_token;
    private String method;
    private String view_contacts;
    private int company_id;

    public HomeDetailCompanyContentRequest() {

    }

    public HomeDetailCompanyContentRequest(String access_token, String method, String view_contacts, int company_id) {
        this.access_token = access_token;
        this.method = method;
        this.view_contacts = view_contacts;
        this.company_id = company_id;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getView_contacts() {
        return view_contacts;
    }

    public void setView_contacts(String view_contacts) {
        this.view_contacts = view_contacts;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }
}