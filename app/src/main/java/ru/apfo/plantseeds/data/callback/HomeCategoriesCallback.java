package ru.apfo.plantseeds.data.callback;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.HomeCategory;

public interface HomeCategoriesCallback {
    void onSuccessGetCategories(List<HomeCategory> categories);

    void onError(String msg);
}