package ru.apfo.plantseeds.util;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ru.apfo.plantseeds.data.api.model.CompanyReview;
import ru.apfo.plantseeds.data.api.model.ImageReview;
import ru.apfo.plantseeds.data.api.model.Product;
import ru.apfo.plantseeds.data.api.model.ProductListItem;
import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueMultiply;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueRange;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueSingleSelect;
import ru.apfo.plantseeds.data.api.model.filter.base.ValueMultiply;
import ru.apfo.plantseeds.data.api.model.filter.base.ValueRange;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueMultiplyResult;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueRangeResult;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueSingleSelectResult;

public class JSONParser {
    public static List<CompanyReview> parseHomeCompanyReview(String json){
        List<CompanyReview> companyReviews = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(json);
            int countReview = jsonObject.getInt("counts");
            JSONObject jsonObjectReviews = jsonObject.getJSONObject("reviews");
            for (int i = 1; i < countReview; i++){
                JSONObject jsonObjectReview = jsonObjectReviews.getJSONObject("" + i);
                List<ImageReview> imageReviews = new ArrayList<>();
                if (jsonObjectReview.has("images") && !jsonObjectReview.isNull("images")){
                    Object jsonImages = jsonObjectReview.get("images");
                    if (jsonImages instanceof JSONObject){
                        Log.d("JSONObject", jsonImages.toString());
                        JSONObject jsonObjectImages = (JSONObject) jsonImages;
                        Iterator<?> keys = jsonObjectImages.keys();
                        while (keys.hasNext()){
                            String key = (String) keys.next();
                            JSONObject jsonObjectByKey = new JSONObject(jsonObjectImages.get(key).toString());
                            imageReviews.add(new ImageReview(
                                    jsonObjectByKey.getInt("id"),
                                    jsonObjectByKey.getString("url")
                            ));
                        }
                    } else if (jsonImages instanceof JSONArray){
                        Log.d("JSONArray", jsonImages.toString());
                        JSONArray jsonArrayImages = (JSONArray) jsonImages;
                        for (int im = 0; im < jsonArrayImages.length(); im++){
                            imageReviews.add(new ImageReview(
                                    jsonArrayImages.getJSONObject(im).getInt("id"),
                                    jsonArrayImages.getJSONObject(im).getString("url")
                            ));
                        }
                    }
                }
                Log.d("jsonObjectReviews", jsonObjectReview.toString());
                CompanyReview companyReview = new CompanyReview(
                        jsonObjectReview.getLong("id"),
                        jsonObjectReview.getString("date"),
                        jsonObjectReview.getInt("rating"),
                        jsonObjectReview.getString("author"),
                        jsonObjectReview.getString("text"),
                        imageReviews
                );
                companyReviews.add(companyReview);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return companyReviews;
    }

    public static List<BaseFilter> parseFilters(String json){
        List<BaseFilter> filters = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("filters");
            for (int i = 0; i < jsonArray.length(); i ++){
                JSONObject jsonObjectFilter = jsonArray.getJSONObject(i);
                switch (jsonObjectFilter.getString("type")){
                    case "checked_single":
                        FilterValueSingleSelect filterValueSingleSelect = new FilterValueSingleSelect(
                                jsonObjectFilter.getInt("spec_id"),
                                jsonObjectFilter.getString("type"),
                                jsonObjectFilter.getString("code"),
                                jsonObjectFilter.getString("title"),
                                jsonObjectFilter.getString("collapsed")
                        );
                        filters.add(filterValueSingleSelect);
                        break;
                    case "range":
                        JSONObject jsonObjectRange = jsonObjectFilter.getJSONObject("values");
                        FilterValueRange filterValueRange = new FilterValueRange(
                                jsonObjectFilter.getInt("spec_id"),
                                jsonObjectFilter.getString("type"),
                                jsonObjectFilter.getString("code"),
                                jsonObjectFilter.getString("title"),
                                jsonObjectFilter.getString("collapsed"),
                                new ValueRange(
                                        jsonObjectRange.getInt("from"),
                                        jsonObjectRange.getInt("to")
                                )
                        );
                        filters.add(filterValueRange);
                        break;
                    case "checked_multi":
                        JSONArray jsonArrayValues = jsonObjectFilter.getJSONArray("values");
                        List<ValueMultiply> valueMultiplies = new ArrayList<>();
                        for (int j = 0; j < jsonArrayValues.length(); j++){
                            valueMultiplies.add(new ValueMultiply(
                                    jsonArrayValues.getJSONObject(j).getString("code"),
                                    jsonArrayValues.getJSONObject(j).getInt("count")
                            ));
                        }
                        FilterValueMultiply filterValueMultiply = new FilterValueMultiply(
                                jsonObjectFilter.getInt("spec_id"),
                                jsonObjectFilter.getString("type"),
                                jsonObjectFilter.getString("code"),
                                jsonObjectFilter.getString("title"),
                                jsonObjectFilter.getString("collapsed"),
                                valueMultiplies
                        );
                        filters.add(filterValueMultiply);
                        break;
                    case "colors":
                        JSONArray jsonArrayValuesColors = jsonObjectFilter.getJSONArray("values");
                        List<ValueMultiply> valueMultipliesColors = new ArrayList<>();
                        for (int j = 0; j < jsonArrayValuesColors.length(); j++){
                            valueMultipliesColors.add(new ValueMultiply(
                                    jsonArrayValuesColors.getJSONObject(j).getString("code"),
                                    jsonArrayValuesColors.getJSONObject(j).getInt("count")
                            ));
                        }
                        FilterValueMultiply filterValueMultiplyColors = new FilterValueMultiply(
                                jsonObjectFilter.getInt("spec_id"),
                                jsonObjectFilter.getString("type"),
                                jsonObjectFilter.getString("code"),
                                jsonObjectFilter.getString("title"),
                                jsonObjectFilter.getString("collapsed"),
                                valueMultipliesColors
                        );
                        filters.add(filterValueMultiplyColors);
                        break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return filters;
    }

    public static JsonObject parseDataForFiltersProduct(String accessToken, String method, int pid, int from, int to,
                                                        String category, String images, String spec, String companies,
                                                        List<BaseFilter> filters){
        Gson gson = new Gson();
        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("access_token", accessToken);
            jsonObject.addProperty("method", method);
            jsonObject.addProperty("pid", pid);

            JsonObject jsonObjectPagination = new JsonObject();
            jsonObjectPagination.addProperty("from", from);
            jsonObjectPagination.addProperty("to", to);
            jsonObject.add("pagination", jsonObjectPagination);

            JsonObject jsonObjectBlockView = new JsonObject();
            jsonObjectBlockView.addProperty("category", category);
            jsonObjectBlockView.addProperty("images", images);
            jsonObjectBlockView.addProperty("spec", spec);
            jsonObjectBlockView.addProperty("companies", companies);
            jsonObject.add("block_view", jsonObjectBlockView);

            JsonArray jsonArrayFilters = new JsonArray();
            for (int i = 0; i < filters.size(); i++){
                switch (filters.get(i).getType()){
                    case "checked_single":
                        FilterValueSingleSelect filterValueSingleSelect = (FilterValueSingleSelect) filters.get(i);
                        FilterValueSingleSelectResult filterValueSingleSelectResult = filterValueSingleSelect.getFilterValueSingleSelectResult();
//                        Type typeFilterValueSingleSelectResult = new TypeToken<FilterValueSingleSelectResult>(){}.getType();
                        jsonArrayFilters.add(gson.toJsonTree(filterValueSingleSelectResult, FilterValueSingleSelectResult.class));
                        break;
                    case "range":
                        FilterValueRange filterValueRange = (FilterValueRange) filters.get(i);
                        FilterValueRangeResult filterValueRangeResult = filterValueRange.getFilterValueRangeResult();
//                        Type typeFilterValueRangeResult = new TypeToken<FilterValueRangeResult>(){}.getType();
                        jsonArrayFilters.add(gson.toJsonTree(filterValueRangeResult, FilterValueRangeResult.class));
                        break;
                    case "checked_multi":
                        FilterValueMultiply filterValueMultiply = (FilterValueMultiply) filters.get(i);
                        FilterValueMultiplyResult filterValueMultiplyResult = filterValueMultiply.getFilterValueMultiplyResult();
//                        Type typeFilterValueMultiplyResult = new TypeToken<FilterValueMultiplyResult>(){}.getType();
                        jsonArrayFilters.add(gson.toJsonTree(filterValueMultiplyResult, FilterValueMultiplyResult.class));
                        break;
                    case "colors":
                        FilterValueMultiply filterValueMultiplyColors = (FilterValueMultiply) filters.get(i);
                        FilterValueMultiplyResult filterValueMultiplyResultColors = filterValueMultiplyColors.getFilterValueMultiplyResult();
//                        Type typefilterValueMultiplyResultColors = new TypeToken<FilterValueMultiplyResult>(){}.getType();
                        jsonArrayFilters.add(gson.toJsonTree(filterValueMultiplyResultColors, FilterValueMultiplyResult.class));
                        break;
                }
            }
            Log.d("JSONArray class:", "" + jsonArrayFilters.toString());
            jsonObject.add("spec", jsonArrayFilters);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d("JSONObject class:", "" + jsonObject.toString());

        return jsonObject;
    }

    public static List<ProductListItem> parseFiltersProducts(JsonObject jsonObject){
        List<ProductListItem> productListItems = new ArrayList<>();
        Gson gson = new Gson();

        JsonArray jsonArray = jsonObject.getAsJsonArray("products");
        for (int i = 0; i < jsonArray.size(); i++){
            JsonObject jsonObjectItem = (JsonObject) jsonArray.get(i);
            jsonObjectItem = jsonObjectItem.getAsJsonObject("product");
            productListItems.add(new ProductListItem(
                    new Product(
                            jsonObjectItem.getAsJsonPrimitive("id").getAsLong(),
                            jsonObjectItem.getAsJsonPrimitive("title").getAsString(),
                            jsonObjectItem.getAsJsonPrimitive("descr").getAsString(),
                            jsonObjectItem.getAsJsonPrimitive("image").getAsString(),
                            jsonObjectItem.getAsJsonPrimitive("favorite").getAsString(),
                            jsonObjectItem.getAsJsonPrimitive("gosreestr").getAsString()
                    ),
                    null,
                    null,
                    null,
                    null));
        }

        return productListItems;
    }
}