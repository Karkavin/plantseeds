package ru.apfo.plantseeds.util;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import ru.apfo.plantseeds.MyPreferences;
import ru.apfo.plantseeds.data.api.model.HomeCategory;

public class PreferencesAdapter {
    public void setSuggestionToPreferences(SharedPreferences preferences, List<HomeCategory> list){
        Gson gson = new Gson();
        String json = gson.toJson(list);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MyPreferences.APP_PREFERENCES_SEARCH_LAST_SUGGESTIONS, json);
        editor.commit();
    }

    public List<HomeCategory> getSuggestionsFromPreferences(SharedPreferences preferences){
        return new Gson().fromJson(
                preferences.getString(MyPreferences.APP_PREFERENCES_SEARCH_LAST_SUGGESTIONS, "[]"),
                new TypeToken<List<HomeCategory>>() {}.getType());
    }

    public void clearAllSuggestions(SharedPreferences preferences){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(MyPreferences.APP_PREFERENCES_SEARCH_LAST_SUGGESTIONS, "[]");
        editor.commit();
    }
}