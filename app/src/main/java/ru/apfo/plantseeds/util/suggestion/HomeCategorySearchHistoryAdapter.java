package ru.apfo.plantseeds.util.suggestion;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;

import com.mancj.materialsearchbar.adapter.SuggestionsAdapter;

import java.util.ArrayList;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.HomeCategory;

public class HomeCategorySearchHistoryAdapter extends SuggestionsAdapter<HomeCategory, HomeCategorySearchHistoryAdapter.SuggestionHolder> {

    private OnItemClickListener listener;

    public HomeCategorySearchHistoryAdapter(LayoutInflater inflater, OnItemClickListener listener) {
        super(inflater);
        this.listener = listener;
    }

    @Override
    public void onBindSuggestionHolder(HomeCategory suggestion, SuggestionHolder holder, int position) {
        holder.bind(suggestion, listener);
    }

    @Override
    public int getSingleViewHeight() {
        return 1000;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                String term = constraint.toString();
                if(term.isEmpty())
                    suggestions = suggestions_clone;
                else {
                    suggestions = new ArrayList<>();
                    for (HomeCategory item: suggestions_clone)
                        if(item.getTitle().toLowerCase().contains(term.toLowerCase()))
                            suggestions.add(item);
                }
                results.values = suggestions;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                suggestions = (ArrayList<HomeCategory>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @NonNull
    @Override
    public SuggestionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = getLayoutInflater().inflate(R.layout.search_item_history, parent, false);
        return new SuggestionHolder(mView);
    }

    public interface OnItemClickListener {
        void onItemClick(HomeCategory item);
    }

    class SuggestionHolder extends RecyclerView.ViewHolder{
        private TextView title;

        SuggestionHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.textViewSearchItemHistory);
        }

        void bind(HomeCategory homeCategory, OnItemClickListener listener){
            title.setText(homeCategory.getTitle());
            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(homeCategory);
                }
            });
        }
    }
}