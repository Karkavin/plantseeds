package ru.apfo.plantseeds.adapter.filter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.MyIntentKeys;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.activity.FilterActivity;
import ru.apfo.plantseeds.activity.MultiplyFilterActivity;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueMultiply;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueMultiplyResult;
import ru.apfo.plantseeds.util.MyConverter;

public class FilterTypeMultiplyCollapsedSelectAdapter implements BaseFilterAdapter{
    private Activity activity;
    private FilterValueMultiply filter;
    private LinearLayout linearLayoutBase;
    private LinearLayout linearLayoutContainer;

    private View viewContainer;

    public FilterTypeMultiplyCollapsedSelectAdapter(Activity activity, FilterValueMultiply filter, LinearLayout linearLayoutBase){
        this.activity = activity;
        this.filter = filter;
        this.linearLayoutBase = linearLayoutBase;

        initialise();
    }

    private void initialise(){
        LayoutInflater inflater = activity.getLayoutInflater();
        viewContainer = inflater.inflate(R.layout.filter_type_select, null, false);
        linearLayoutContainer = viewContainer.findViewById(R.id.linearLayoutFilterTypeCollapsedSelectContainer);
        ((TextView) viewContainer.findViewById(R.id.textViewFilterTypeSelectFirst)).setText(filter.getTitle());
        ((RelativeLayout) viewContainer.findViewById(R.id.relativeLayoutFilterTypeSelect)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, MultiplyFilterActivity.class);
                intent.putExtra(MyIntentKeys.APP_INTENT_MULTIPLY_FILTER_VALUES, filter);
                intent.putExtra(MyIntentKeys.APP_INTENT_MULTIPLY_FILTER_VALUES_ID, filter.getSpec_id());
                activity.startActivityForResult(intent, FilterActivity.ACTIVITY_RESULT_CODE);
            }
        });

        if (filter.getFilterValueMultiplyResult() == null || filter.getFilterValueMultiplyResult().getValues().size() == 0){
            viewContainer.findViewById(R.id.viewFilterTypeSelectSeparate).setVisibility(View.GONE);
            viewContainer.findViewById(R.id.textViewFilterTypeSelectTitle).setVisibility(View.GONE);
        } else if (filter.getFilterValueMultiplyResult() != null && filter.getFilterValueMultiplyResult().getValues().size() > 0){
            viewContainer.findViewById(R.id.viewFilterTypeSelectSeparate).setVisibility(View.VISIBLE);
            viewContainer.findViewById(R.id.textViewFilterTypeSelectTitle).setVisibility(View.VISIBLE);
        }

        linearLayoutBase.addView(viewContainer);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, MyConverter.dpToPx(30));
        Space space = new Space(activity);
        space.setLayoutParams(layoutParams);
        linearLayoutBase.addView(space);

        //If had been selected:
        if (filter.getFilterValueMultiplyResult() != null && filter.getFilterValueMultiplyResult().getValues().size() > 0){
            viewContainer.findViewById(R.id.viewFilterTypeSelectSeparate).setVisibility(View.VISIBLE);
            viewContainer.findViewById(R.id.textViewFilterTypeSelectTitle).setVisibility(View.VISIBLE);

            updateFilter(filter);
        }
    }

    public void updateFilter(FilterValueMultiply mFilter){
        this.filter = mFilter;

        if (filter.getFilterValueMultiplyResult() == null || filter.getFilterValueMultiplyResult().getValues().size() == 0){
            viewContainer.findViewById(R.id.viewFilterTypeSelectSeparate).setVisibility(View.GONE);
            viewContainer.findViewById(R.id.textViewFilterTypeSelectTitle).setVisibility(View.GONE);
        } else if (filter.getFilterValueMultiplyResult() != null && filter.getFilterValueMultiplyResult().getValues().size() > 0){
            viewContainer.findViewById(R.id.viewFilterTypeSelectSeparate).setVisibility(View.VISIBLE);
            viewContainer.findViewById(R.id.textViewFilterTypeSelectTitle).setVisibility(View.VISIBLE);
        }

        linearLayoutContainer.removeAllViews();
        LayoutInflater inflater = activity.getLayoutInflater();
        List<String> valuesSelected = filter.getFilterValueMultiplyResult().getValues();
        for (int i = 0; i < valuesSelected.size(); i++){
            View view = inflater.inflate(R.layout.filter_type_select_item, linearLayoutContainer, false);
            ((TextView) view.findViewById(R.id.textViewFilterTypeSelectItem)).setText(valuesSelected.get(i));
            int finalI = i;
            ((ImageView) view.findViewById(R.id.imageViewFilterTypeSelectItemDelete)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    linearLayoutContainer.removeAllViews();
                    filter.getFilterValueMultiplyResult().remove(finalI);
                    updateFilter(filter);
                }
            });
            linearLayoutContainer.addView(view);
        }
    }

    public void disableAllData(){
        linearLayoutContainer.removeAllViews();

        viewContainer.findViewById(R.id.viewFilterTypeSelectSeparate).setVisibility(View.GONE);
        viewContainer.findViewById(R.id.textViewFilterTypeSelectTitle).setVisibility(View.GONE);

        if (filter.getFilterValueMultiplyResult() != null){
            filter.setFilterValueMultiplyResult(
                    new FilterValueMultiplyResult(
                    filter.getSpec_id(),
                    filter.getType(),
                    filter.getCode())
            );
        }
    }

    public FilterValueMultiply getResultFilter(){
        List<String> valuesResult = new ArrayList<>();
        if (linearLayoutContainer != null) {
            for (int i = 0; i < linearLayoutContainer.getChildCount(); i++){
                valuesResult.add(((TextView)linearLayoutContainer.getChildAt(i).findViewById(R.id.textViewFilterTypeSelectItem)).getText().toString());
            }
        }

        filter.setFilterValueMultiplyResult(
                new FilterValueMultiplyResult(
                        filter.getSpec_id(),
                        filter.getType(),
                        filter.getCode(),
                        valuesResult)
        );

        return filter;
    }
}