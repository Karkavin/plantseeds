package ru.apfo.plantseeds.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.HomeCategory;

public class ListHomeSubSubcategoryAdapter extends BaseAdapter {
    private Context context;
    private List<HomeCategory> subcategories;

    private LayoutInflater mInflater;

    public ListHomeSubSubcategoryAdapter(Context context, List<HomeCategory> subcategories) {
        this.context = context;
        this.subcategories = subcategories;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return subcategories.size();
    }

    @Override
    public HomeCategory getItem(int i) {
        return subcategories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return subcategories.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.card_home_sub_subcategory, viewGroup, false);
        }

        ((TextView) view.findViewById(R.id.textViewCardHomeSubSubcategoryName)).setText(subcategories.get(i).getTitle());

        Picasso.with(context)
                .load(subcategories.get(i).getImg_url())
                .into(((ImageView) view.findViewById(R.id.imageViewCardHomeSubSubcategory)));

        return view;
    }

    public void setItems(List<HomeCategory> categories) {
        this.subcategories = categories;
        notifyDataSetChanged();
    }

    public void clean() {
        subcategories = new ArrayList<>();
        notifyDataSetChanged();
    }
}