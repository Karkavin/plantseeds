package ru.apfo.plantseeds.adapter.filter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueMultiply;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueRange;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueSingleSelect;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueMultiplyResult;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueRangeResult;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueSingleSelectResult;

public class FiltersResultAdapter {
    private FlexboxLayout flexboxLayoutBase;
    private LayoutInflater inflater;

    private OnFilterResultClicked onFilterResultClicked;

    private List<BaseFilter> filters;

    public FiltersResultAdapter(List<BaseFilter> filters, LayoutInflater inflater,
                                FlexboxLayout flexboxLayoutBase, OnFilterResultClicked onFilterResultClicked){
        this.flexboxLayoutBase = flexboxLayoutBase;
        this.inflater = inflater;

        this.onFilterResultClicked = onFilterResultClicked;

        this.filters = filters;

        initialise();
    }

    private void initialise(){
        clearOldItems();

        for (int i = 0; i < filters.size(); i++){
            BaseFilter baseFilter = filters.get(i);
            View view = null;
            switch (filters.get(i).getType()){
                case "checked_single":
                    FilterValueSingleSelect filterValueSingleSelect = (FilterValueSingleSelect) baseFilter;
                    FilterValueSingleSelectResult filterValueSingleSelectResult = filterValueSingleSelect.getFilterValueSingleSelectResult();
                    if (filterValueSingleSelectResult != null && filterValueSingleSelectResult.getValue().equals("yes")){
                        view = inflater.inflate(R.layout.item_filter_single_text, null, false);
                        TextView textViewItemFilterSingleText = (TextView) view.findViewById(R.id.textViewItemFilterSingleText);
                        textViewItemFilterSingleText.setText(filterValueSingleSelect.getTitle());
                        ImageView imageViewDelete = (ImageView) view.findViewById(R.id.imageViewItemFilterSingleTextDelete);
                        int finalI = i;
                        imageViewDelete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((FilterValueSingleSelect) filters.get(finalI)).getFilterValueSingleSelectResult().setValue("no");
                                onFilterResultClicked.onFilterResultClicked(filters);
                            }
                        });
                        flexboxLayoutBase.addView(view);
                    }
                    break;
                case "range":
                    FilterValueRange filterValueRange = (FilterValueRange) baseFilter;
                    FilterValueRangeResult filterValueRangeResult = filterValueRange.getFilterValueRangeResult();
                    if (filterValueRangeResult != null &&
                            (filterValueRangeResult.getFrom() != filterValueRange.getValues().getFrom() || filterValueRangeResult.getTo() != filterValueRange.getValues().getTo())){
                        view = inflater.inflate(R.layout.item_filter_single_text, null, false);
                        TextView textViewItemFilterSingleText = (TextView) view.findViewById(R.id.textViewItemFilterSingleText);
                        textViewItemFilterSingleText.setText(filterValueRange.getTitle() + ": " + filterValueRangeResult.getFrom() + " - " + filterValueRangeResult.getTo());
                        ImageView imageViewDelete = (ImageView) view.findViewById(R.id.imageViewItemFilterSingleTextDelete);
                        int finalI = i;
                        imageViewDelete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ((FilterValueRange) filters.get(finalI)).getFilterValueRangeResult().setFrom(filterValueRange.getValues().getFrom());
                                ((FilterValueRange) filters.get(finalI)).getFilterValueRangeResult().setTo(filterValueRange.getValues().getTo());
                                onFilterResultClicked.onFilterResultClicked(filters);
                            }
                        });
                        flexboxLayoutBase.addView(view);
                    }
                    break;
                case "checked_multi":
                    FilterValueMultiply filterValueMultiply = (FilterValueMultiply) baseFilter;
                    FilterValueMultiplyResult filterValueMultiplyResult = filterValueMultiply.getFilterValueMultiplyResult();
                    if (filterValueMultiplyResult != null && filterValueMultiplyResult.getValues().size() > 0){
                        for (int j = 0; j < filterValueMultiplyResult.getValues().size(); j++){
                            view = inflater.inflate(R.layout.item_filter_single_text, null, false);
                            TextView textViewItemFilterSingleText = (TextView) view.findViewById(R.id.textViewItemFilterSingleText);
                            textViewItemFilterSingleText.setText(filterValueMultiply.getTitle() + ": " + filterValueMultiplyResult.getValues().get(j));
                            ImageView imageViewDelete = (ImageView) view.findViewById(R.id.imageViewItemFilterSingleTextDelete);
                            int finalI = i;
                            int finalJ = j;
                            imageViewDelete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ((FilterValueMultiply) filters.get(finalI)).getFilterValueMultiplyResult().remove(finalJ);
                                    onFilterResultClicked.onFilterResultClicked(filters);
                                }
                            });
                            flexboxLayoutBase.addView(view);
                        }
                    }
                    break;
                case "colors":
                    FilterValueMultiply filterValueMultiplyColors = (FilterValueMultiply) baseFilter;
                    FilterValueMultiplyResult filterValueMultiplyResultColor = filterValueMultiplyColors.getFilterValueMultiplyResult();
                    if (filterValueMultiplyResultColor != null && filterValueMultiplyResultColor.getValues().size() > 0){
                        view = inflater.inflate(R.layout.item_filter_color, null, false);
                        FlexboxLayout flexboxLayoutItemFilterColor = (FlexboxLayout) view.findViewById(R.id.flexboxLayoutItemFilterColor);
                        for (int j = 0; j < filterValueMultiplyResultColor.getValues().size(); j++){
                            View viewItem = inflater.inflate(R.layout.item_filter_color_item, null, false);
                            CircularImageView circularImageView = (CircularImageView) viewItem.findViewById(R.id.circularImageViewItemFilterColor);
                            circularImageView.setBorderColor(Color.parseColor(filterValueMultiplyResultColor.getValues().get(j)));
                            int finalI = i;
                            int finalJ = j;
                            RelativeLayout relativeLayoutDelete = (RelativeLayout) viewItem.findViewById(R.id.circularImageViewItemFilterColorClose);
                            relativeLayoutDelete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ((FilterValueMultiply) filters.get(finalI)).getFilterValueMultiplyResult().remove(finalJ);
                                    onFilterResultClicked.onFilterResultClicked(filters);
                                }
                            });
                            flexboxLayoutItemFilterColor.addView(viewItem);
                        }
                        flexboxLayoutBase.addView(view);
                    }
                    break;
            }
        }
    }

    private void clearOldItems(){
        if (flexboxLayoutBase.getChildCount() > 1) {
            for (int i = flexboxLayoutBase.getChildCount()-1; i > 0; i--){
                flexboxLayoutBase.removeViewAt(i);
            }
        }
    }

    public interface OnFilterResultClicked{
        void onFilterResultClicked(List<BaseFilter> newFilters);
    }
}