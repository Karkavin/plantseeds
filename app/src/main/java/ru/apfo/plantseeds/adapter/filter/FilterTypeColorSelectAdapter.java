package ru.apfo.plantseeds.adapter.filter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;

import com.google.android.flexbox.FlexboxLayout;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueMultiply;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueMultiplyResult;
import ru.apfo.plantseeds.util.MyConverter;

public class FilterTypeColorSelectAdapter implements BaseFilterAdapter{
    private Activity activity;
    private FilterValueMultiply filter;
    private LinearLayout linearLayoutBase;
    private FlexboxLayout flexboxLayoutContainer;

    public FilterTypeColorSelectAdapter(Activity activity, FilterValueMultiply filter, LinearLayout linearLayoutBase){
        this.activity = activity;
        this.filter = filter;
        this.linearLayoutBase = linearLayoutBase;

        initialise();
    }

    private void initialise(){
        LayoutInflater inflater = activity.getLayoutInflater();
        View viewContainer = inflater.inflate(R.layout.filter_type_colors, null, false);
        flexboxLayoutContainer = (FlexboxLayout) viewContainer.findViewById(R.id.flexboxLayoutFilterTypeColors);

        for (int i = 0; i < filter.getValues().size(); i++){
            View view = inflater.inflate(R.layout.filter_type_colors_item, flexboxLayoutContainer, false);
            CircularImageView circularImageView = (CircularImageView) view.findViewById(R.id.circularImageViewFilterTypeColorsItem);
            RelativeLayout relativeLayoutHover = (RelativeLayout) view.findViewById(R.id.circularImageViewFilterTypeColorsItemCheck);
            circularImageView.setBorderColor(Color.parseColor(filter.getValues().get(i).getCode()));
            circularImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (relativeLayoutHover.getVisibility() == View.GONE){
                        relativeLayoutHover.setVisibility(View.VISIBLE);
                    } else if (relativeLayoutHover.getVisibility() == View.VISIBLE){
                        relativeLayoutHover.setVisibility(View.GONE);
                    }
                }
            });
            flexboxLayoutContainer.addView(view);
        }

        linearLayoutBase.addView(viewContainer);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, MyConverter.dpToPx(30));
        Space space = new Space(activity);
        space.setLayoutParams(layoutParams);
        linearLayoutBase.addView(space);

        //If had been selected:
        if (filter.getFilterValueMultiplyResult() != null && filter.getFilterValueMultiplyResult().getValues().size() > 0){
            for (int i = 0; i < filter.getValues().size(); i++){
                for (int j = 0; j < filter.getFilterValueMultiplyResult().getValues().size(); j++){
                    if (filter.getValues().get(i).getCode().equals(filter.getFilterValueMultiplyResult().getValues().get(j))){
                        flexboxLayoutContainer.getChildAt(i).findViewById(R.id.circularImageViewFilterTypeColorsItemCheck).setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    public void disableAllData(){
        if (flexboxLayoutContainer != null) {
            for (int i = 0; i < flexboxLayoutContainer.getChildCount(); i++){
                View view = flexboxLayoutContainer.getChildAt(i);
                ((RelativeLayout) view.findViewById(R.id.circularImageViewFilterTypeColorsItemCheck)).setVisibility(View.GONE);
            }
        }
    }

    public FilterValueMultiply getResultFilter(){
        List<String> valuesResult = new ArrayList<>();
        if (flexboxLayoutContainer != null) {
            for (int i = 0; i < flexboxLayoutContainer.getChildCount(); i++){
                if (((RelativeLayout) flexboxLayoutContainer.getChildAt(i).findViewById(R.id.circularImageViewFilterTypeColorsItemCheck)).getVisibility() == View.VISIBLE){
                    valuesResult.add(filter.getValues().get(i).getCode());
                }
            }
        }

        filter.setFilterValueMultiplyResult(
                new FilterValueMultiplyResult(
                        filter.getSpec_id(),
                        filter.getType(),
                        filter.getCode(),
                        valuesResult)
        );

        return filter;
    }
}