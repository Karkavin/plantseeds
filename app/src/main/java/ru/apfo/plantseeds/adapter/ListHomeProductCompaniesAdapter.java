package ru.apfo.plantseeds.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;
import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.Company;
import ru.apfo.plantseeds.data.api.model.ProductListItem;

public class ListHomeProductCompaniesAdapter extends BaseAdapter {
    private Context context;
    private List<Company> companies;

    private LayoutInflater mInflater;

    public ListHomeProductCompaniesAdapter(Context context, List<Company> companies) {
        this.context = context;
        this.companies = companies;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sortFromAToZ();
    }

    @Override
    public int getCount() {
        return companies.size();
    }

    @Override
    public Company getItem(int i) {
        return companies.get(i);
    }

    @Override
    public long getItemId(int i) {
        return companies.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.card_home_product_company, viewGroup, false);
        }

        LayerDrawable stars = (LayerDrawable) ((RatingBar) view.findViewById(R.id.ratingBarHomeProductCompany)).getProgressDrawable();
        stars.getDrawable(2).setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(context.getResources().getColor(R.color.defaultGrey), PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(context.getResources().getColor(R.color.defaultGrey), PorterDuff.Mode.SRC_ATOP);

        ((TextView) view.findViewById(R.id.textViewHomeProductCompanyName)).setText(companies.get(i).getTitle());
        ((TextView) view.findViewById(R.id.textViewHomeProductCompanyRating)).setText(companies.get(i).getRating() + "");
        ((MaterialRatingBar) view.findViewById(R.id.ratingBarHomeProductCompany)).setRating(companies.get(i).getRating());
        int countOfReview = companies.get(i).getVoices();
        ((TextView) view.findViewById(R.id.textViewHomeProductCompanyCountOfReview))
                .setVisibility(countOfReview > 0 ? View.VISIBLE : View.INVISIBLE);
        ((TextView) view.findViewById(R.id.textViewHomeProductCompanyCountOfReview)).setText(countOfReview + " отзыв (а/ов)");

        return view;
    }

    public void setItems(List<Company> categories) {
        this.companies = categories;
        sortFromAToZ();
        notifyDataSetChanged();
    }

    public void clean() {
        companies = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void sortFromAToZ(){
        Comparator<Company> ORDER_FROM_A_TO_Z = new Comparator<Company>() {
            public int compare(Company company1, Company company2) {
                return String.CASE_INSENSITIVE_ORDER.compare(company1.getTitle(), company2.getTitle());
            }
        };
        Collections.sort(companies, ORDER_FROM_A_TO_Z);
        notifyDataSetChanged();
    }

    public void sortFromZToA(){
        Comparator<Company> ORDER_FROM_Z_TO_A = new Comparator<Company>() {
            public int compare(Company company1, Company company2) {
                return String.CASE_INSENSITIVE_ORDER.compare(company2.getTitle(), company1.getTitle());
            }
        };
        Collections.sort(companies, ORDER_FROM_Z_TO_A);
        notifyDataSetChanged();
    }

    public void sortByRating(){
        Comparator<Company> ORDER_BY_RATING = new Comparator<Company>() {
            public int compare(Company company1, Company company2) {
                return Float.compare(company2.getRating(), company1.getRating());
            }
        };
        Collections.sort(companies, ORDER_BY_RATING);
        notifyDataSetChanged();
    }
}