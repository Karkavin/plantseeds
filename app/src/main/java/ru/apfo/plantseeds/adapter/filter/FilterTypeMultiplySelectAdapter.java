package ru.apfo.plantseeds.adapter.filter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueMultiply;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueMultiplyResult;
import ru.apfo.plantseeds.util.MyConverter;

public class FilterTypeMultiplySelectAdapter implements BaseFilterAdapter{
    private Activity activity;
    private FilterValueMultiply filter;
    private LinearLayout linearLayoutBase;
    private LinearLayout linearLayoutContainer;

    public FilterTypeMultiplySelectAdapter(Activity activity, FilterValueMultiply filter, LinearLayout linearLayoutBase){
        this.activity = activity;
        this.filter = filter;
        this.linearLayoutBase = linearLayoutBase;

        initialise();
    }

    private void initialise(){
        LayoutInflater inflater = activity.getLayoutInflater();
        View viewContainer = inflater.inflate(R.layout.filter_type_multiply_select, null, false);
        linearLayoutContainer = viewContainer.findViewById(R.id.linearLayoutFilterTypeMultiplySelectContainer);
        ((TextView) viewContainer.findViewById(R.id.textViewFilterTypeMultiplySelectTitle)).setText(filter.getTitle());

        for (int i = 0; i < filter.getValues().size(); i++){
            View view = inflater.inflate(R.layout.filter_type_multiply_select_item_simple, linearLayoutContainer, false);
            TextView textView = (TextView) view.findViewById(R.id.textViewFilterTypeMultiplySelectItemSimple);
            textView.setText(filter.getValues().get(i).getCode());
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkboxFilterTypeMultiplySelectItemSimple);
            checkBox.setChecked(false);
            linearLayoutContainer.addView(view);
        }

        linearLayoutBase.addView(viewContainer);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, MyConverter.dpToPx(30));
        Space space = new Space(activity);
        space.setLayoutParams(layoutParams);
        linearLayoutBase.addView(space);

        //If had been selected:
        if (filter.getFilterValueMultiplyResult() != null && filter.getFilterValueMultiplyResult().getValues().size() > 0){
            for (int i = 0; i < linearLayoutContainer.getChildCount(); i++){
                for (int j = 0; j < filter.getFilterValueMultiplyResult().getValues().size(); j++) {
                    String value = filter.getFilterValueMultiplyResult().getValues().get(j);
                    if (((TextView) linearLayoutContainer.getChildAt(i).findViewById(R.id.textViewFilterTypeMultiplySelectItemSimple)).getText().toString().equals(value)){
                        ((CheckBox) linearLayoutContainer.getChildAt(i).findViewById(R.id.checkboxFilterTypeMultiplySelectItemSimple)).setChecked(true);
                    }
                }
            }
        }
    }

    public void disableAllData(){
        if (linearLayoutContainer != null) {
            for (int i = 0; i < linearLayoutContainer.getChildCount(); i++){
                View view = linearLayoutContainer.getChildAt(i);
                ((CheckBox) view.findViewById(R.id.checkboxFilterTypeMultiplySelectItemSimple)).setChecked(false);
            }
        }
    }

    public FilterValueMultiply getResultFilter(){
        List<String> valuesResult = new ArrayList<>();
        if (linearLayoutContainer != null) {
            for (int i = 0; i < linearLayoutContainer.getChildCount(); i++){
                if (((CheckBox) linearLayoutContainer.getChildAt(i).findViewById(R.id.checkboxFilterTypeMultiplySelectItemSimple)).isChecked()){
                    valuesResult.add(filter.getValues().get(i).getCode());
                }
            }
        }

        filter.setFilterValueMultiplyResult(
                new FilterValueMultiplyResult(
                        filter.getSpec_id(),
                        filter.getType(),
                        filter.getCode(),
                        valuesResult)
        );

        return filter;
    }
}