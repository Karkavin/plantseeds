package ru.apfo.plantseeds.adapter.filter;

import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;

public interface BaseFilterAdapter {
    void disableAllData();
    BaseFilter getResultFilter();
}