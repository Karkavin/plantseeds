package ru.apfo.plantseeds.adapter.filter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import com.appyvet.materialrangebar.RangeBar;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueRange;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueRangeResult;
import ru.apfo.plantseeds.util.MyConverter;

public class FilterTypeRangeSelectAdapter implements BaseFilterAdapter{
    private Activity activity;
    private FilterValueRange filter;
    private LinearLayout linearLayoutBase;
    private View viewContainer;
    private Space space;

    private int index;

    public FilterTypeRangeSelectAdapter(Activity activity, FilterValueRange filter, LinearLayout linearLayoutBase){
        this.activity = activity;
        this.filter = filter;
        this.linearLayoutBase = linearLayoutBase;

        index = linearLayoutBase.getChildCount();

        initialise();
    }

    private void initialise(){
        LayoutInflater inflater = activity.getLayoutInflater();
        viewContainer = inflater.inflate(R.layout.filter_type_range, null, false);
        ((TextView) viewContainer.findViewById(R.id.textViewFilterTypeRangeTitle)).setText(filter.getTitle());
        TextView textViewLeft = (TextView) viewContainer.findViewById(R.id.textViewFilterTypeRangeLeft);
        textViewLeft.setText(filter.getValues().getFrom() + "");
        TextView textViewRight = (TextView) viewContainer.findViewById(R.id.textViewFilterTypeRangeRight);
        textViewRight.setText(filter.getValues().getTo() + "");
        RangeBar rangeBar = (RangeBar) viewContainer.findViewById(R.id.rangeBarFilterTypeRange);
        rangeBar.setTickStart(filter.getValues().getFrom());
        rangeBar.setTickEnd(filter.getValues().getTo());
        rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                textViewLeft.setText(leftPinValue);
                textViewRight.setText(rightPinValue);
            }
        });

        linearLayoutBase.addView(viewContainer, index);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, MyConverter.dpToPx(30));
        space = new Space(activity);
        space.setLayoutParams(layoutParams);
        linearLayoutBase.addView(space, index + 1);

        //If had been selected:
        if (filter.getFilterValueRangeResult() != null){
            if (filter.getFilterValueRangeResult().getFrom() != 0 && filter.getFilterValueRangeResult().getTo() != 0){
                rangeBar.setRangePinsByValue(filter.getFilterValueRangeResult().getFrom(), filter.getFilterValueRangeResult().getTo());
            }
        }
    }

    public void disableAllData(){
        RangeBar rangeBar = (RangeBar) viewContainer.findViewById(R.id.rangeBarFilterTypeRange);
        rangeBar.setRangePinsByValue(filter.getValues().getFrom(), filter.getValues().getTo());
    }

    public FilterValueRange getResultFilter(){
        filter.setFilterValueRangeResult(
                new FilterValueRangeResult(
                        filter.getSpec_id(),
                        filter.getType(),
                        filter.getCode(),
                        Integer.parseInt(((TextView) viewContainer.findViewById(R.id.textViewFilterTypeRangeLeft)).getText().toString()),
                        Integer.parseInt(((TextView) viewContainer.findViewById(R.id.textViewFilterTypeRangeRight)).getText().toString())
                )
        );

        return filter;
    }
}