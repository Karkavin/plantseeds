package ru.apfo.plantseeds.adapter.filter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.filter.base.FilterValueSingleSelect;
import ru.apfo.plantseeds.data.api.model.filter.result.FilterValueSingleSelectResult;
import ru.apfo.plantseeds.util.MyConverter;

public class FilterTypeSingleSelectAdapter implements BaseFilterAdapter{
    private Activity activity;
    private FilterValueSingleSelect filter;
    private LinearLayout linearLayoutBase;
    private View viewContainer;

    public FilterTypeSingleSelectAdapter(Activity activity, FilterValueSingleSelect filter, LinearLayout linearLayoutBase){
        this.activity = activity;
        this.filter = filter;
        this.linearLayoutBase = linearLayoutBase;

        initialise();
    }

    private void initialise(){
        LayoutInflater inflater = activity.getLayoutInflater();
        viewContainer = inflater.inflate(R.layout.filter_type_single_select, null, false);
        ((TextView) viewContainer.findViewById(R.id.textViewFilterTypeSingleSelect)).setText(filter.getTitle());
        ((CheckBox) viewContainer.findViewById(R.id.checkBoxFilterTypeSingleSelect)).setChecked(false);
        linearLayoutBase.addView(viewContainer);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, MyConverter.dpToPx(30));
        Space space = new Space(activity);
        space.setLayoutParams(layoutParams);
        linearLayoutBase.addView(space);

        //If had been selected:
        if (filter.getFilterValueSingleSelectResult() != null){
            if (filter.getFilterValueSingleSelectResult().getValue().equals("yes")){
                ((CheckBox) viewContainer.findViewById(R.id.checkBoxFilterTypeSingleSelect)).setChecked(true);
            }
        }
    }

    public void disableAllData(){
        ((CheckBox) viewContainer.findViewById(R.id.checkBoxFilterTypeSingleSelect)).setChecked(false);
    }

    public FilterValueSingleSelect getResultFilter(){
        filter.setFilterValueSingleSelectResult(
                new FilterValueSingleSelectResult(
                        filter.getSpec_id(),
                        filter.getType(),
                        filter.getCode(),
                        ((CheckBox) viewContainer.findViewById(R.id.checkBoxFilterTypeSingleSelect)).isChecked() ? "yes" : "no")
        );

        return filter;
    }
}