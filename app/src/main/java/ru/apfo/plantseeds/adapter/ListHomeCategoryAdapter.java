package ru.apfo.plantseeds.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.HomeCategory;

public class ListHomeCategoryAdapter extends BaseAdapter {
    private Context context;
    private List<HomeCategory> categories;

    private LayoutInflater mInflater;

    public ListHomeCategoryAdapter(Context context, List<HomeCategory> categories){
        this.context = context;
        this.categories = categories;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public HomeCategory getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return categories.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.card_home_category, viewGroup, false);
        }

        ((TextView) view.findViewById(R.id.textViewCardHomeCategoryName)).setText(categories.get(i).getTitle());

        Picasso.with(context)
                .load(categories.get(i).getImg_url())
                .into(((ImageView) view.findViewById(R.id.imageViewCardHomeCategory)));
        Log.d(getClass().getSimpleName(), categories.get(i).getImg_url());

        return view;
    }

    public void setItems(List<HomeCategory> categories){
        this.categories = categories;
        notifyDataSetChanged();
    }

    public void clean(){
        categories = new ArrayList<>();
        notifyDataSetChanged();
    }
}