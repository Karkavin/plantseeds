package ru.apfo.plantseeds.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.ProductListItem;
import ru.apfo.plantseeds.view.HomeProductView;

public class ListHomeSortAdapter extends BaseAdapter {
    private Context context;
    private List<ProductListItem> products;
    private HomeProductView homeProductView;

    public ListHomeSortAdapter(Context context, List<ProductListItem> products, HomeProductView view) {
        this.context = context;
        this.products = products;
        this.homeProductView = view;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public ProductListItem getItem(int i) {
        return products.get(i);
    }

    @Override
    public long getItemId(int i) {
        return products.get(i).getProduct().getId();
    }

    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.card_home_sub_subcategory, null);
            viewHolder.imageViewFavourite = (ImageView) convertView.findViewById(R.id.imageViewCardHomeSubSubcategoryLikeHomeIsFavourite);
            viewHolder.imageViewImage = (ImageView) convertView.findViewById(R.id.imageViewCardHomeSubSubcategory);
            viewHolder.textViewIsInGosReestr = (TextView) convertView.findViewById(R.id.textViewCardHomeSubSubcategoryGos);
            viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textViewCardHomeSubSubcategoryName);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Picasso.with(context)
                .load(products.get(i).getProduct().getImage())
                .into(viewHolder.imageViewImage);

        if (products.get(i).getProduct() != null) {
            if (products.get(i).getProduct().getGosreestr().equals("yes")) {
                viewHolder.textViewIsInGosReestr.setVisibility(View.VISIBLE);
            } else {
                viewHolder.textViewIsInGosReestr.setVisibility(View.GONE);
            }
        }

        viewHolder.imageViewFavourite.setVisibility(View.VISIBLE);
        if (products.get(i).getProduct() != null) {
            if (products.get(i).getProduct().getFavorite().equals("yes")) {
                viewHolder.imageViewFavourite.setImageResource(R.drawable.favourite_active);
            } else {
                viewHolder.imageViewFavourite.setImageResource(R.drawable.icon_favourite);
            }

            viewHolder.imageViewFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (products.get(i).getProduct().getFavorite().equals("yes")) {
                        products.get(i).getProduct().setFavorite("no");
                        viewHolder.imageViewFavourite.setImageResource(R.drawable.icon_favourite);
                        homeProductView.deleteFromFavourite((int)products.get(i).getProduct().getId());
                    } else {
                        products.get(i).getProduct().setFavorite("yes");
                        viewHolder.imageViewFavourite.setImageResource(R.drawable.favourite_active);
                        homeProductView.addToFavourite((int)products.get(i).getProduct().getId());
                    }
                }
            });
        }

        viewHolder.textViewName.setText(products.get(i).getProduct().getTitle());

        return convertView;
    }

    public void setItems(List<ProductListItem> products) {
        this.products.addAll(products);
        notifyDataSetChanged();
    }

    public void clean() {
        products = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void sortFromAToZ(){
        Comparator<ProductListItem> ORDER_FROM_A_TO_Z = new Comparator<ProductListItem>() {
            public int compare(ProductListItem item1, ProductListItem item2) {
                return String.CASE_INSENSITIVE_ORDER.compare(item1.getProduct().getTitle(), item2.getProduct().getTitle());
            }
        };
        Collections.sort(products, ORDER_FROM_A_TO_Z);
        notifyDataSetChanged();
    }

    public void sortFromZToA(){
        Comparator<ProductListItem> ORDER_FROM_Z_TO_A = new Comparator<ProductListItem>() {
            public int compare(ProductListItem item1, ProductListItem item2) {
                return String.CASE_INSENSITIVE_ORDER.compare(item2.getProduct().getTitle(), item1.getProduct().getTitle());
            }
        };
        Collections.sort(products, ORDER_FROM_Z_TO_A);
        notifyDataSetChanged();
    }

    public void updateListItem(ProductListItem productListItem){
        for (int i = 0; i < products.size(); i++){
            if (products.get(i).getProduct().getId() == productListItem.getProduct().getId()){
                products.set(i, productListItem);
                break;
            }
        }
        notifyDataSetChanged();
    }

    private class ViewHolder{
        ImageView imageViewImage;
        TextView textViewIsInGosReestr;
        ImageView imageViewFavourite;
        TextView textViewName;
    }
}