package ru.apfo.plantseeds.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;

import ru.apfo.plantseeds.R;
import ru.apfo.plantseeds.data.api.model.ProductListItem;

public class ListHomeProductCompaniesSortAdapter extends BaseAdapter {
    private SortType sortType;
    private LayoutInflater mInflater;

    private String[] content = new String[]{"Названию: А-Я", "Названию: Я-А", "Рейтингу"};

    public ListHomeProductCompaniesSortAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public SortType getItem(int i) {
        switch (i){
            case 0:
                return SortType.SORT_TYPE_FROM_A_TO_Z;
            case 1:
                return SortType.SORT_TYPE_FROM_Z_TO_A;
            case 2:
                return SortType.RATING;
            default:
                return SortType.SORT_TYPE_FROM_A_TO_Z;
        }
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(R.layout.card_sort_bottom_sheet, viewGroup, false);
        }

        TextView textViewContent = (TextView) view.findViewById(R.id.textViewHomeProductCardSortBottomSheet);
        textViewContent.setText(content[i]);

        ImageView imageViewCheck = (ImageView) view.findViewById(R.id.imageViewHomeProductCardSortBottomSheet);
        if (i == sortType.ordinal()){
            imageViewCheck.setVisibility(View.VISIBLE);
        } else{
            imageViewCheck.setVisibility(View.GONE);
        }

        return view;
    }

    public void setSortType(int i) {
        switch (i){
            case 0:
                this.sortType = SortType.SORT_TYPE_FROM_A_TO_Z;
                break;
            case 1:
                this.sortType = SortType.SORT_TYPE_FROM_Z_TO_A;
                break;
            case 2:
                this.sortType = SortType.RATING;
                break;
            default:
                this.sortType = SortType.SORT_TYPE_FROM_A_TO_Z;
                break;
        }
    }

    public void setSortType(SortType sortType) {
        this.sortType = sortType;
    }

    public SortType getSortType() {
        return sortType;
    }

    public enum SortType{
        SORT_TYPE_FROM_A_TO_Z,
        SORT_TYPE_FROM_Z_TO_A,
        RATING
    }
}
