package ru.apfo.plantseeds;

public class MyPreferences {
    public static final String APP_PREFERENCES_FIRST_START = "isFirstStart";
    public static final String APP_PREFERENCES_ACCESS_TOKEN = "access_token";
    public static final String APP_PREFERENCES_SEARCH_LAST_SUGGESTIONS = "searchLastSuggestions";
}