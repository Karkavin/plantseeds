package ru.apfo.plantseeds.view;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.HomeCategory;

public interface HomeCategoryView {
    void startLoading();

    void stopLoading();

    void addCategories(List<HomeCategory> categories);

    void showSnackbarMessage(String msg);

    void openNextCategories(HomeCategory category);
}