package ru.apfo.plantseeds.view;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.filter.base.BaseFilter;

public interface FilterView {
    void addFilters(List<BaseFilter> filters);

    void startLoading();

    void stopLoading();

    void showSnackbarMessage(String msg);
}