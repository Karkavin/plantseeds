package ru.apfo.plantseeds.view;

import ru.apfo.plantseeds.data.api.model.About;

public interface AboutView {
    void startLoading();

    void stopLoading();

    void showSnackbarMessage(String msg);

    void aboutLoaded(About about);
}