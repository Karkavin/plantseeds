package ru.apfo.plantseeds.view;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.ProductListItem;

public interface HomeProductView {
    void startLoading();

    void stopLoading();

    void addProducts(List<ProductListItem> products);

    void showSnackbarMessage(String msg);

    void openDetail(ProductListItem productListItem);

    void addToFavourite(int id);

    void deleteFromFavourite(int id);
}