package ru.apfo.plantseeds.view;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.DetailCompany;

public interface CompaniesView {
    void startLoading();

    void stopLoading();

    void showSnackbarMessage(String msg);

    void companiesLoaded(List<DetailCompany> listCompanies);
}