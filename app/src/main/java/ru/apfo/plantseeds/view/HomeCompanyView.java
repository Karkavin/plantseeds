package ru.apfo.plantseeds.view;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.CompanyReview;
import ru.apfo.plantseeds.data.api.model.DetailCompany;

public interface HomeCompanyView {
    void startLoadingContent();

    void stopLoadingContent();

    void startLoadingReviews();

    void stopLoadingReviews();

    void addContent(DetailCompany detailCompany);

    void addReviews(List<CompanyReview> companyReviews);

    void showSnackbarMessage(String msg);

    void removeAllReview();

    void fullReviewAdded();
}
