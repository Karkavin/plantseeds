package ru.apfo.plantseeds.view;

import java.util.List;

import ru.apfo.plantseeds.data.api.model.HomeCategory;

public interface HomeSearchView {
    void addVegetablesSearchData(List<HomeCategory> vegetablesSearchData);

    void addFlowersSearchData(List<HomeCategory> flowersSearchData);

    void showSnackbarMessage(String msg);
}