package ru.apfo.plantseeds.view;

public interface HomeProductDetailView {
    void showSnackbarMessage(String msg);

    void addToFavourite(int id);

    void deleteFromFavourite(int id);
}