package ru.apfo.plantseeds;

public class MyIntentKeys {
    //ALL
    public static final String APP_INTENT_BOTTOM_BAR_POSITION = "bottomBarPosition";

    //HOME
    public static final String APP_INTENT_HOME_FROM_CATEGORY = "homeCategory";
    public static final String APP_INTENT_HOME_FROM_SUBCATEGORY = "homeSubcategory";
    public static final String APP_INTENT_HOME_FROM_SUBSUBCATEGORY = "homeSubSubcategory";
    public static final String APP_INTENT_HOME_FROM_SORT = "homeSort";

    public static final String APP_INTENT_HOME_CATEGORY = "homeHomeCategory";

    public static final String APP_INTENT_HOME_PRODUCT_RESULT = "homeProductResult";

    public static final String APP_INTENT_HOME_COMPANY_BASE_INFO = "homeCompanyBaseInfo";
    public static final String APP_INTENT_HOME_COMPANY_PRODUCT = "homeCompanyProduct";

    public static final String APP_INTENT_FULL_REVIEW_RATING = "fullReviewRating";
    public static final String APP_INTENT_FULL_REVIEW_COMPANY = "fullReviewCompany";
    public static final String APP_INTENT_FULL_REVIEW_PRODUCT = "fullReviewProduct";

    public static final String APP_INTENT_FULL_REVIEW_IS_REVIEW_ADDED = "fullReviewIsAdded";

    public static final String APP_INTENT_DETAIL_COMPANY_CATEGORY = "detailCompanyCategory";

    public static final String APP_INTENT_FILTER_TITLE = "filterTitle";
    public static final String APP_INTENT_FILTER_CATEGORY_TYPE = "categoryType";
    public static final String APP_INTENT_FILTERS = "filter";

    public static final String APP_INTENT_MULTIPLY_FILTER_VALUES = "multiplyFilterValues";
    public static final String APP_INTENT_MULTIPLY_FILTER_VALUES_ID = "multiplyFilterValuesId";
    public static final String APP_INTENT_MULTIPLY_FILTER_VALUES_SEND = "multiplyFilterValuesSend";
}