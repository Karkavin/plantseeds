package ru.apfo.plantseeds;

public enum FragmentType {
//    BOTTOM_BAR
    MAIN,
    SEARCH,
    FAVOURITE,
//    NAVIGATION_BAR
    FLOWERS,
    VEGETABLES,
    COMPANIES,
    ABOUT
}